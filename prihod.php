<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
//include_once $_SERVER['DOCUMENT_ROOT'].'application/handler.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>ПРИХОД</title>
	<?php include_once $_SERVER['DOCUMENT_ROOT'].'/head.php'; ?>
</head>
<body>
	<div align="center">
			<div class="messager"></div>
	<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
			<section class="head">
				<div class="container">
					<div class="time" id="time"></div>
					<script>      
					        $(document).ready(function(){  
					            show();  
					            setInterval('show()',10000);
					        });  
					</script> 
					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/login.php';?>
				</div>
			</section>
	<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
			<section class="menu">
				<div class="container">
					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menu.php';?>
				</div>
			</section>
	<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
			<section class="menu">
				<div class="container">
					<div>
						<?php // WHERE `ZanosToScl` = 0
						$qery = "SELECT TOP 100 * FROM `in` ORDER BY `datepo` DESC";
						if ($result = $AccConn->query($qery))
						{ ?>
						<div><h3>Приходные накладные</h3>
							<table id = 'pnt' align = 'center'>
								<tr class = 'tabheader'>
									<td>№</td>
									<td>Номер накл.</td>
									<td>Дата накл.</td>
									<td>Дата прихода</td>
									<td>От кого</td>
									<td>Статус</td>
								</tr>
									<?php
									while ($row = $result->fetch(PDO::FETCH_LAZY))
									{ ?>
								<tr>
									<td><a class = "edit" id = <?php echo '"'.$row['kodttn'].'_prihod"'; ?>><?php echo $row['kodttn']; ?></a></td>
									<td><?php echo $row['nom']; ?></td>
									<td><?php echo substr($row['date'], 0, -9); ?></td>
									<td><?php echo substr($row['datepo'], 0, -9); ?></td>
									<td><?php echo iconv("Windows-1251", "UTF-8", $row['klnaim']); ?></td>
									<td><?php if($row['ZanosToScl'] == 1) { echo  "+";} else echo ""; ?></td>
								</tr>
								<?php } ?>
							</table>
						</div>
						<?php } else {echo "<h3>Ошибка выполнения запроса!</h3>";} ?>
					</div>
				</div>
			</section>
	<?php//------------------------------------------------------------------------------------------------------------------------------------------------------------------ ?>
	</div> 
</body>
</html>