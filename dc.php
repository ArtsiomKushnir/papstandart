<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>ГРУППЫ ТОВАРА</title>

	<link rel="stylesheet" href="css/style.css">
	
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/scripts.js" type="text/javascript"></script>

	</head>
</head>
<body>
	<div align="center">
<?php//----------------всплывающие окна-------------------------------------------------------------------------------------------------------------------------------------- ?>
		<div id = "messagebox">
			<div class="b-popup" id="popup">
				<div class="b-popup-content">
					<img src="pbar.gif" alt="clock">
				</div>
			</div>
		</div>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="head">
			<div class="container">
				<div class="time" id="time"></div>
				<?php include_once $_SERVER['DOCUMENT_ROOT'].'/login.php';?>
			</div>
		</section>
<?php//---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="menu">
			<div class="container">
				<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menu.php';?>
			</div>
		</section>
<?php//---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section>
			<div class="container">
				<div style="display: block;">
					<style type="text/css">
						td.FIO, td.email {
							text-align: left;
						}
					</style>

					<?php if($str = file_get_contents('http://dc.paperki.by/JSONgetDC.php?key=DcPap11111'))
							{$str = json_decode($str, true);}
						else $str = 0;
							//print_r($str); ?>

					<table>
						<tr class="tabheader">
							<td>ID</td>
							<td>activation</td>
							<td>FIO</td>
							<td>email</td>
							<td>phone</td>
							<td>SUM</td>
							<td>DISCOUNT, %</td>
						</tr>

			<?php
			if($str != 0) 
			{
			foreach ($str as $key => $value)
					{
						echo '<tr><td>'.$key.'</td>';
						if (is_array($value))
						{
							foreach($value as $k => $vl)
							{
								if ($k == 'FIO') 
								{
									echo '<td class = "edit FIO '.$key.' id cards">'.$vl.'</td>';
								}
								else if ($k == 'activation') 
								{
									if ($vl == 0) 
									{
										echo '<td class = "edit activation '.$key.' id cards"><input type="checkbox" /></td>';
									}
									else
									{
										echo '<td class = "edit activation '.$key.' id cards"><input type="checkbox" checked/></td>';
									}
								}
								else if ($k == 'email') 
								{
									echo '<td class = "edit email '.$key.' id cards">'.$vl.'</td>';
								}
								else if ($k == 'sum') 
								{
									echo '<td><a class= "edit" id = '.$key.'>'.$vl.'</a></td>';
								}
								else if ($k == 'phone')
								{
									echo '<td class = "edit phone '.$key.' id cards">'.$vl.'</td>';
								}
								else echo '<td>'.$vl.'</td>';
							}
						}
						echo '</tr>';
					} 
				} ?>
					</table>
				</div>
			</div>
		</section>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
	</div>
	<script>      
		$(document).ready(function(){  
			show();  
			setInterval('show()',10000);
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			PopUpHide();
		});
		function PopUpShow(){
			$("#popup").show();
		}
		function PopUpHide(){
			$("#popup").hide();
		}
	</script>
	<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function(){ // двойной клик
			$('.ajax').html($('.ajax input').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<form action="http://dc.paperki.by/update.php" method="post" name="form" onsubmit="return false;"><input id="editbox" size="'+ $(this).text().length+'" value="' + $(this).text() + '" type="text"></form>');
			$('#editbox').focus();
		});

		$(document).on('keydown', 'td.edit', function(event){
		arr = $(this).attr('class').split( " " );
		   if(event.which == 13)
		   {
				var value = $('.ajax input').val();
				$.ajax({ 
					type: "POST",
					url: "http://dc.paperki.by/update.php",
					data: {"value": value, "key": arr[2], "KeyName": arr[3], "field": arr[1], "table": arr[4], "pass": "Pap11111"},
					cache: false,
					success: function(data){
						 $('.ajax').html($('.ajax input').val());
						 $('.ajax').removeClass('ajax');
				 	}
				});
				return false;
		 	}
		});

		$("input").change(function(){
			var checkbox = $(this), value, arr = $(this).parent('td').attr('class').split(" ");
			if (checkbox.is(':checked')) {value = '1';}
			else{value = '0';}

			$.ajax({ 
				type: "POST",
				url: "http://dc.paperki.by/update.php",
				data: {"value": value, "key": arr[2], "KeyName": arr[3], "field": arr[1], "table": arr[4], "pass": "Pap11111"},
				cache: false,
				success: function(data){
					//alert(value);
			 	}
			});
			return false;
		});

	</script>
	<script type="text/javascript">
	$(document).on('click', 'a.edit', function(){
		var id = $(this).attr('id'),
			key = 'DcPap11111';
			$(this).parent('td').addClass('editable');
			$('a.edit').addClass('no_edit');
			$('a.edit').removeClass('edit');
			$('td.editable').parent('tr').children('td').attr('bgcolor', '#FFFF00');
			PopUpShow();
			$.ajax({ 
				type: "GET",
				url: "http://dc.paperki.by/gettransaction.php",
				data: {"id": id, "key": key},
				cache: false,
				success: function(response)
				{					
					$('#popup').html(response);
				}
			});
			return false;
	})

	$(document).on('click', 'a.button_cancel', function(){
			$('td.editable').parent('tr').children('td').removeAttr('bgcolor');
			$('a.no_edit').addClass('edit');
			$('a.edit').removeClass('no_edit');
			$('.editable').removeAttr('class');
			PopUpHide();
			$('#popup').html('<div class="b-popup-content"><img src="pbar.gif" alt="clock"></div>');
	});

	$(document).on('mouseover', 'tr', function(){
		$(this).attr('bgcolor', '#FFFF00');
	});
	$(document).on('mouseout', 'tr', function(){
		$(this).removeAttr('bgcolor');
	});
	</script>
</body>
</html>