<?php include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php'; ?>

			<form action="index.php?key=1" method="post" name="table">
			<input name="key" type="hidden" value="1">
				<table cellpadding="0";	cellspacing="0";>
					<tr>
						<td>Наименование БТ</td>
						<td>Наименование СКЛАД</td>
						<td>Подсказка для наименования</td>
						<td>Cвойства</td>
						<td>ГРУППА</td>
						<td class="td_no_border"></td>
					</tr>
					<tr>
						<td class="td_no_border">
							<input style="width: 197px; height: 19px; font-size: 14px; text-align: center;" name="BT" type="text">
						</td>
						<td class="td_no_border">
							<input style="width: 197px; height: 19px; font-size: 14px; text-align: center;" name="BT_skl" type="text">
						</td>
						<td class="td_no_border">
							<input style="width: 197px; height: 19px; font-size: 14px; text-align: center;" name="BT_placeholder" type="text" placeholder="Пример имени для сайта">
						</td>
						<td class="td_no_border">
							<div>
								<select name ='svs' data-placeholder="Выберите свойства..." class="chosen-select chosen" multiple>
									<option value =""></option>
									<?php
										$query = "SELECT * FROM PNTS2";
										$result = $AccConn->query($query);
										while ($row = $result->fetch(PDO::FETCH_LAZY))
										{
											echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
										}
									?>
								</select>
								<select name="sv[]" id="ch2" multiple style="display: none"></select>
							</div>
						</td>
						<td class="td_no_border" style="width: 450px;">
							<div style="width: 100%; display: inline-block; text-align: left;">
								<select name = 'gr' data-placeholder="Выберите группу..." class="chosen-select" style="width: 100%;">
									<option value =""></option>
									<?php
										$query = "SELECT grG.kod, grG.naim AS grGnaim, grG.grk, grK.naim AS grKnaim
										FROM grG
										INNER JOIN grK
										ON grG.grk = grK.kod 
										ORDER BY grK.posl";

										if($result = $AccConn->query($query))
										{
											$stek = 0;
											while ($row = $result->fetch(PDO::FETCH_LAZY))
											{
												$GrK = $row['grk'];
												if ($stek !== $GrK)
												{
													if ($stek != 0)
													{
														echo "</optgroup>";
													}												
														echo "<optgroup label = '".iconv("Windows-1251", "UTF-8", $row['grKnaim'])."'>";
												}
												echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
												$stek = $GrK;
											}
											echo "</optgroup>";
										}
										else echo "<option>Error</option>";
									?>
								</select>
								<script type="text/javascript">
							    	var config = {
							      	'.chosen-select'           : {},
							      	'.chosen-select-deselect'  : {allow_single_deselect:true},
							      	'.chosen-select-no-single' : {disable_search_threshold:10},
							     	'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
							      	'.chosen-select-width'     : {width:"95%"}
							    	}
								    for (var selector in config) {$(selector).chosen(config[selector]);}
								</script>
							</div>
						</td>
						<td class="td_no_border"><input id="submit" type="submit" value="ДОБАВИТЬ"></td>
					</tr>
				</table>
			</form>

			<form action="index.php?key=1" method="post">
				<table cellpadding="0";	cellspacing="0";>
					<tr>
						<td>Имя нового свойства</td>
						<td>Пример значения для нового свойства</td>
						<td class="td_no_border"></td>
					</tr>
					<tr>
						<td class="td_no_border"><input name="addSV" type="text"  placeholder="Имя нового свойства" style="width: 100%;"></td>
						<td class="td_no_border"><input name="addSVplaceholder" type="text" placeholder="подсказка для ввода значения" style="width: 100%;"></td>
						<td class="td_no_border"><input type="submit" value="+"></td>
					</tr>
				</table>
			</form>
				<?php//---------выпадпющее меню chosen-------------------------------------------------------------------------------------- ?>
			<script type="text/javascript">
		    	var config = {
		      	'.chosen-select'           : {},
		      	'.chosen-select-deselect'  : {allow_single_deselect:true},
		      	'.chosen-select-no-single' : {disable_search_threshold:10},
		     	'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
		      	'.chosen-select-width'     : {width:"95%"}
		    	}
			    for (var selector in config)
			    {
			    	$(selector).chosen(config[selector]);
			    }
			</script>
			<?php//----------собираем свойства (характер)-------------------------------------------------------------------------------- ?>
		  	<script type="text/javascript">
				$('select[multiple].chosen').chosen();
		        var s55_SELECT = $($('select[multiple][name="sv55"].chosen').get(0));
		        var sv_SELECT = $($('select[multiple][name="svs"].chosen').get(0));

		        $('#submit').click(function()
		        {
		            var selection = s55_SELECT.getSelectionOrder();
		            var selection2 = sv_SELECT.getSelectionOrder();

		            $('#ch').empty();

		            $(selection).each(function(i)
		            {
		                $('#ch').append("<option selected value = "+selection[i]+">"+selection[i]+"</option>");
		            });

		            $('#ch2').empty();

		            $(selection2).each(function(i)
		            {
		                $('#ch2').append("<option selected value = "+selection2[i]+">"+selection2[i]+"</option>");
		            });
		        });
		  	</script>