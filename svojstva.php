<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СПРАВОЧНИК СВОЙСТВ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<link rel="stylesheet" href="css/bootstrap.chosen.min.css">

</head>
<body>
	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="Modal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="title"><strong><i id="nameTitle"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body" id="modalBody">
										
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="save" onclick="">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</div>

	<!-- ГЛАВНЫЙ КОНТЕЙНЕР ======================================================= -->
	<div class="container">

		<div class="masthead">
			<!-- МЕНЮ ======================================================= -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>

							<li>
								<a>СПРАВОЧНИК СВОЙСТВ</a>
							</li>
						</ul>
			    	</div>
				</div>
			</nav>
		</div>

		<!-- КОНТЕНТ ======================================================= -->
		<div class="row">
			<table class="table table-condensed table-hover table-bordered">
				<tr class='tabheader'>
					<td>kod</td>
					<td>Наименование Свойства</td>
					<td>Подсказка для ввода значения</td>
				</tr>
<?php
					printSvojstva($AccConn);
?>
			</table>
		</div>

	</div>

	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script src="js/chosen.jquery.min.js"></script>
	<script src="js/chosen.order.jquery.min.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function(){
			$('.ajax').html($('.ajax input').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<input id="editbox" value="' + $(this).text() + '" type="text">');
			$('#editbox').focus();
		});

		$(document).on('keydown', 'td.edit', function(){
			arr = $(this).attr('class').split(" ");
			if(event.which == 13) {
				$.ajax({ 
					type: "POST",
					url: "MVC/update.php",
					data: {"value": $('.ajax input').val()
							, "key": arr[2]
							, "KeyName": arr[3]
							, "field": arr[1]
							, "table": arr[4]
						},
					cache: false,
					success: function(data){
						$('.ajax').html($('.ajax input').val());
						$('.ajax').removeClass('ajax');
					},
					error: function() {
						alert("ERROR request->response");
					}
				});
				return false;
			}
		});
	</script>
</body>
</html>

<?php

function printSvojstva ($AccConn) {
	if($arr = getSvojstva($AccConn)) {
		foreach ($arr as $value) { ?>
			<tr>
				<td><?php echo $value['kod']; ?></td>
				<td class= <?php echo '"edit naim '.$value['kod'].' kod PNTS2"'; ?>><strong><?php echo $value['naim']; ?></strong></td>
				<td class= <?php echo '"edit placeholder '.$value['kod'].' kod PNTS2"'; ?>><?php echo $value['placeholder']; ?></td>
			</tr>
<?php 	}
	} else echo "ERROR printSvojstva()";
}

function getSvojstva($AccConn) {
	$objArr = new ArrayObject();

	if($result = $AccConn->prepare("SELECT * FROM PNTS2")){
		if ($result->execute()) {
			while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
				$objArr->append(array('kod' => $row['kod'], 'naim' => iconv("Windows-1251", "UTF-8", $row['naim']), 'placeholder' => iconv("Windows-1251", "UTF-8", $row['placeholder'])));
			}
		} else echo "ERROR execute query getSvojstva()";
	} else echo "ERROR prepare query getSvojstva()";

	return $objArr;
}



?>