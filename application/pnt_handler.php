<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	echo $_POST['data']

if (isset($_POST['data']))
{
	$barCode = ''; // Штрих - код
	$ARTIKUL = ''; // Артикул
	$ED_IZM = ''; // Ед. измерения
	$STRANA = ''; // Страна
	$fasovka = ''; // Фасовка
	$brend = ''; // код Бренд
	$ves = 0.0; // Вес
	$size = ''; // Габариты размер
	$IDgr = 0; // код Группы
	$pnto = ''; // набор свойств и значений
												/* по строке PNTO   _БББББ_ССЗЗЗЗЗ_ССЗЗЗЗЗ_ССЗЗЗЗЗ_.........
											                       5    2    5  2    5  2    5
												*/
	foreach($_POST['data'] as $key => $value)
	{
		if ($key == 'barCode')
		{
			$barCode = $value;
		}
		else if ($key == 'ARTIKUL')
		{
			$ARTIKUL = iconv("UTF-8", "Windows-1251", $value);
		}
		else if ($key == 'ED_IZM')
		{
			$ED_IZM = iconv("UTF-8", "Windows-1251", $value);
		}
		else if ($key == 'STRANA')
		{
			$STRANA = iconv("UTF-8", "Windows-1251", $value);
		}
		else if ($key == 'fasovka')
		{
			$fasovka = iconv("UTF-8", "Windows-1251", $value);
		}
		else if ($key == 'brend')
		{
			$brend = $value;
		}
		else if ($key == 'ves')
		{
			$ves = (double)$value; 
		}
		else if ($key == 'size')
		{
			$size = iconv("UTF-8", "Windows-1251", $value);
		}
		else if ($key == 'IDgr')
		{
			$IDgr = (int)$value;
		}
		else
		{
			if ($value != '')
			{
				if ($kodpntsz = ifpntszExist($key, $value, $AccConn)) // возвращяет код значения свойства если оно уже присутствует в справочнике pntsz, или false если нет значения
				{
					$pnto .= SetSV($key, $kodpntsz);
				}
				else if($kodpntsz = addpntsz($key, $value, $AccConn)) // создаем новое значение свойства в справочнике pntsz
				{
					$pnto .= SetSV($key, $kodpntsz);
				}
				else echo "Не удалось создать и присвоить новое свойство pnt_handler_addpntsz";
			}
		}
	}

	if ($IDgr != 0 && $barCode != '' && $ARTIKUL != '' && $ED_IZM != '' && $STRANA != '' && $fasovka != '' && $pnto != '')
	{
		if ($nextNumber = GetNextNumber($AccConn, 'pnt' ,'pnt'))
		{
			$values = array($nextNumber, $IDgr, $barCode, $ARTIKUL, $ED_IZM, $STRANA, $pnto, $fasovka);
			$result = $AccConn->prepare("INSERT INTO pnt (pnt, grG, barCode, ARTIKUL, ED_IZM, STRANA, PNTO2, fasovka) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			if ($result->execute($values))
			{
				echo "Товар добавлен. Код ".$nextNumber;
			}
			else
			{
				echo "Ошибка записи в pnt. Код: ".$nextNumber.' + '.$IDgr.' + '.$barCode.' + '.$ARTIKUL.' + '.$ED_IZM.' + '.$STRANA.' + '.$pnto.' + '.$fasovka;
			}
			//header('Location: spravochnik.php', true, 303);
		}
		else
		{
			echo "Error pnt_handler_GetNextNumber";
			//header('Location: spravochnik.php', true, 303);
		}
	}
	else
	{
		unset($_POST);
	}
}
else echo "no data";
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
function ifpntszExist($kods, $value, $AccConn)
	{
		$result = $AccConn->prepare("SELECT kodz, naim FROM pntsz WHERE kods = $kods");
		if ($result->execute())
		{
			$tr = 0;
			while ($row = $result->fetch(PDO::FETCH_LAZY))
			{
				$naimval = iconv("UTF-8", "Windows-1251", $value);
				//echo $kods.'.'.$row['naim'].' === '.$naimval.'  ';
				if ($row['naim'] === $naimval) // проверка на совпадение наименований
				{
					$tr = $row['kodz'];
				}
			}
			if ($tr == 0)
			{
				return false;
			}
			else
			{
				return $tr;
			}
		}
		else
		{
			AddLog("Ошибка pnt_handler_ifpntszExist");
			return false;
		}
	}

function addpntsz($kods, $naim, $AccConn) // добавляем значение свойства в таблицу pntsz
	{
		if ($nextNumber = GetNextNumberpntsz($kods))
		{
			$naimconv = iconv("UTF-8", "Windows-1251", $naim);
			try
			{
				$qery = "INSERT INTO pntsz (kods, kodz, naim) VALUES (:kods, :nextNumber, :naimconv)";
				$result = $AccConn->prepare($qery);
				if($result->execute(array(':kods'=>$kods,':nextNumber'=>$nextNumber,':naimconv'=>$naimconv)))
				{
					AddLog("Новое значение свойства ".$naim.", добавлено. Код ".$nextNumber.", Код свойства: ".$kods);
					return $nextNumber;
				}
				else
				{
					AddLog("Error pnt_handler_addpntsz, Код ".$nextNumber.", значение свойства ".$naim.", Код свойства: ".$kods);
					return false;
				}
			}
			catch (Exception $e)
			{
				AddLog($e." - Error pnt_handler_addpntsz, Код ".$nextNumber.", значение свойства ".$naim.", Код свойства: ".$kods.' Qery: '.$qery);
			}
		}
		else return false;
	}

function SetSV($key, $value) // собираем свойства и значение для товара
    {
    	$sv = '00';
    		 if (strlen($key) == '1') {$sv = '0'.$key;}
    	else if (strlen($key) >= '2') {$sv =  $key;}

    	$sval = '00000';
    		 if (strlen($value) == '1') {$sval = '0000'.$value;}
    	else if (strlen($value) == '2') {$sval = '000'.$value;}
    	else if (strlen($value) == '3') {$sval = '00'.$value;}
    	else if (strlen($value) == '4') {$sval = '0'.$value;}
    	else if (strlen($value) == '5') {$sval = $value;}

        return "_".$sv.$sval;
    }

function GetNextNumberpntsz($kods) // возвращает следующий код значения свойства по коду свойства
    {
        $AccConn = DBAccessConnect();
        $result = $AccConn->prepare("SELECT MAX(kodz) FROM pntsz WHERE kods = $kods");
        if ($result->execute())
        {
        	$name = $result->fetchColumn();
        	return (int)$name + 1;
    	}
    	else
		{
			AddLog("Ошибка pnt_handler_GetNextNumberpntsz");
			return false;
		}
    }
?>