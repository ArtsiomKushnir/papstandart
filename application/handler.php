<?php

if (isset($_POST['BT']) && $_POST['BT'] !== '' &&
	isset($_POST['BT_skl']) && $_POST['BT_skl'] !== '' &&
	isset($_POST['BT_placeholder']) &&
	isset($_POST['sv']) && $_POST['sv'] !== '' &&
	isset($_POST['gr']) && $_POST['gr'] !== '')
	{
		$naim = iconv("UTF-8", "Windows-1251", $_POST['BT']); // наименование для Салона
		$naimscl = iconv("UTF-8", "Windows-1251", $_POST['BT_skl']); // складское наименование
		$BT_placeholder = iconv("UTF-8", "Windows-1251", $_POST['BT_placeholder']); // подсказка для ввода короткого имени для сайта
		$xarakter = GetString($_POST['sv']); // набор свойств с учетом порядка _ССССС_ССССС_ССССС_...
		$xarakterZN = ''; // значение свойств по умолчанию _00000_00000_00000_...
		$GrG = $_POST['gr']; // код группы
		//--------------------------------------------------------------------------------------------------------
		$nextNumber = GetNextNumber($AccConn, 'kod' ,'BT');
		$values = array($nextNumber, $naim, $naimscl, $xarakter, $xarakterZN, $GrG, $BT_placeholder);
		$result = $AccConn->prepare("INSERT INTO BT (kod, naim, naimscl, xarakter, xarakterZN, GrG, placeholder) VALUES (?, ?, ?, ?, ?, ?, ?)");
		if ($result->execute($values))
		{
			AddLog("Данные в BT добавлены kod: ".$nextNumber.', naim: '.$_POST['BT'].', xarakter: '.$xarakter);
		}
		else
		{
			AddLog("Ошибка добавления данных в BT. kod - ".$nextNumber);
		}
		header('Location: index.php?key=1', true, 303);
	}
else if (isset($_POST['addSV']) && $_POST['addSV'] !== '' && isset($_POST['addSVplaceholder'])) // добавляем свойство
	{
		if(ifPNTS2Exist($AccConn, $_POST['addSV']))
		{

		}
		else addPNTS2($AccConn, $_POST['addSV'], $_POST['addSVplaceholder']);

		header('Location: index.php?key=1', true, 303);
	}
else if(isset($_POST['xarakter'])) // обновляем характер BT
	{
		$id = $_POST['xarakter'];
		$descr = $_POST['descr'];

		if ($_POST['gr'] != '') {$gr = $_POST['gr'];} else {$gr = 0;}
		unset($_POST['xarakter']);
		unset($_POST['descr']);
		unset($_POST['gr']);
		unset($_POST['chose_svs']);

		$string = '';
		foreach ($_POST as $key => $value)
		{
			$string .= "_".SetSV($key);
		}

		if(UpdateBT($AccConn, 'xarakter', $string, $id))
		{
			if ($gr != 0)
			{
				if(UpdateBT($AccConn, 'GrG', $gr, $id))
				{
					UpdateBT($AccConn, 'descr', $descr, $id);
				}
			}
		}
		header('Location: index.php?key=1', true, 303);
	}
else 
	{}
?>