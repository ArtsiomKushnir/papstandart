﻿<?php

function DBAccessConnect() // подключение к БД Access
    {
        //$dbAcess = $_SERVER['DOCUMENT_ROOT'].'/application\setd.mdb';
        $dbAcess = 'D:\SALON\setd.mdb';
        //if(!file_exists($dbAcess)){return 'Error finding access database';}
        try { $conn = new PDO("odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=".$dbAcess.";Uid=; Pwd=;"); return $conn; } catch (Exception $e) { return $e->getMessage();}
    }

function DBSQLConnect() // подключение к БД Access
    {
        include_once $_SERVER['DOCUMENT_ROOT'].'/application/config.php';
        $mysqli = new mysqli($dbhost, $dbusername, $dbpass, $dbname); // объект SQLi, БД SQL
        if ($mysqli->connect_errno) 
            {
                return $mysqli->connect_error;
            }
        else return $mysqli;
    }

function GetNextNumber($AccConn, $fieldname ,$table) // подключение к БД
    {
        $stmt = $AccConn->prepare("SELECT MAX($fieldname) FROM $table");
        if ($stmt->execute())
        {
            $name = $stmt->fetchColumn();
            return (int)$name + 1;
        }
        else return false;
    }

function AddLog($info) // Запись лога в файл
    {
        $curDate = date("Y-m-d G:i:s"); // получаем дату
        $LogDir = $_SERVER['DOCUMENT_ROOT'].'/log.txt';
        $fp = fopen($LogDir, 'a+'); // открываем файл для записи
        $test = fwrite($fp, $curDate." - ".$info.PHP_EOL); // записываем строку с переводом курсора на следующую строку
        fclose($fp); // закрываем файл
    }

function GetString($array) // собираем строку из значений массива с разделителем "_"
    {
        $string = "";
        foreach ($array as $id)
        {
            if ($id !== '')
            {
               $string .= "_".SetSV($id);
            }
        }
        return $string;
    }

function addPNTS2($AccConn, $value, $plchohld)
	{
		if ($nextNumber = GetNextNumber($AccConn, 'kod' ,'PNTS2'))
		{
			$naim = iconv("UTF-8", "Windows-1251", $value);
            $plchohld2 = iconv("UTF-8", "Windows-1251", $plchohld);
			$values = array($nextNumber, $naim, $plchohld2);
			$result = $AccConn->prepare("INSERT INTO PNTS2 (kod, naim, placeholder) VALUES (?, ?, ?)");
			if ($result->execute($values))
			{
				AddLog("Данные в PNTS2 добавлены kod: ".$nextNumber.', naim: '.$value.', placeholder: '.$plchohld);
				return $nextNumber;
			}
			else
			{
				AddLog("Ошибка добавления данных в PNTS2. kod - ".$nextNumber);
				return false;
			}
		}
		else return false;		
	}

function ifPNTS2Exist($AccConn, $value) // проверка существования свойства
    {
        $result = $AccConn->prepare("SELECT * FROM PNTS2");
        if ($result->execute())
        {
            $tr = 0;
            $naimval = iconv("UTF-8", "Windows-1251", $value);
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            {
                if ($row['naim'] == $naimval) // проверка на совпадение наименований
                {
                    $tr = $row['kod'];
                }
            }
            if ($tr == 0)
            {
                return false;
            }
            else
            {
                return $tr;
            }
        }
        else return false;
    }

function addBTplus($AccConn, $val) // добавляем значение свойства в таблицу BTplus
    {
        if ($nextNumber = GetNextNumber($AccConn,'kod' ,'BTplus'))
        {
            $naim = iconv("UTF-8", "Windows-1251", $val);

            $qery = "INSERT INTO BTplus (kod, naim) VALUES (:kod, :naim)";
            $result = $AccConn->prepare($qery);
            if($result->execute(array(':kod'=>$nextNumber, ':naim'=>$naim)))
            {
                AddLog("Новое значение в таблице BTplus: ".$val.", добавлено. Код ".$nextNumber);
                return $nextNumber;
            }
            else
            {
                AddLog("Error addBTplus, Код ".$nextNumber.", значение свойства ".$naim);
                return false;
            }
        }
        else return false;
    }

function addBrend($AccConn, $val) // добавляем значение свойства в таблицу BTplus
    {
        if ($nextNumber = GetNextNumber($AccConn,'kod' ,'brends'))
        {
            $naim = iconv("UTF-8", "Windows-1251", $val);

            $qery = "INSERT INTO brends (kod, naim) VALUES (:kod, :naim)";
            $result = $AccConn->prepare($qery);
            if($result->execute(array(':kod'=>$nextNumber, ':naim'=>$naim)))
            {
                AddLog("Новое значение в таблице brends: ".$val.", добавлено. Код ".$nextNumber);
                return $nextNumber;
            }
            else
            {
                AddLog("Error addBrend, Код ".$nextNumber.", значение свойства ".$naim);
                return false;
            }
        }
        else return false;
    }

function ifBTplusExist($AccConn, $value) // проверка существования наименования
    {
        $result = $AccConn->prepare("SELECT * FROM BTplus");
        if ($result->execute())
        {
            $tr = 0;
            $naimval = iconv("UTF-8", "Windows-1251", $value);
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            {
                if ($row['naim'] == $naimval) // проверка на совпадение наименований
                {
                    $tr = $row['kod'];
                }
            }
            if ($tr == 0)
            {
                return false;
            }
            else
            {
                return $tr;
            }
        }
        else return false;
    }

function ifBrendExist($AccConn, $value) // проверка существования наименования
    {
        $result = $AccConn->prepare("SELECT * FROM brends");
        if ($result->execute())
        {
            $tr = 0;
            $naimval = iconv("UTF-8", "Windows-1251", $value);
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            {
                if ($row['naim'] == $naimval) // проверка на совпадение наименований
                {
                    $tr = $row['kod'];
                }
            }
            if ($tr == 0)
            {
                return false;
            }
            else
            {
                return $tr;
            }
        }
        else return false;
    }

function getBTplus($AccConn, $kod)
    {
        $kod = (int)$kod;
        $stmt = $AccConn->prepare("SELECT naim FROM BTplus WHERE kod = $kod");
        if ($stmt->execute())
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }

function GetBTname($AccConn, $kod)
	{
		$kod = (int)$kod;
        $result = $AccConn->prepare("SELECT naim FROM BT WHERE kod = $kod");
        if ($result->execute())
        {
        	$val = $result->fetchColumn();
        	return $val;
        }
        else return false;
	}

function GetBTxarakter($AccConn, $kod)
    {
        $kod = (int)$kod;
        $result = $AccConn->prepare("SELECT xarakter FROM BT WHERE kod = $kod");
        if ($result->execute())
        {
            $val = $result->fetchColumn();
            return $val;
        }
        else return false;
    }

function SetSV($PNTS2) // создает код для свойства в поле характер
    {
		$sv = '00000';
             if (strlen($PNTS2) == '1') {$sv = '0000'.$PNTS2;}
        else if (strlen($PNTS2) == '2') {$sv = '000'.$PNTS2;}
        else if (strlen($PNTS2) == '3') {$sv = '00'.$PNTS2;}
        else if (strlen($PNTS2) == '4') {$sv = '0'.$PNTS2;}
        else if (strlen($PNTS2) == '5') {$sv = $PNTS2;}

        return $sv;
    }
    
function GetGRGName($AccConn, $ID)
    {
        $stmt = $AccConn->prepare("SELECT naim FROM grG WHERE kod = $ID");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }
    
function GetpntsNameOld($ID, $AccConn) // получаем имя свойства из таблицы PNTS2 по идентификатору
    {
        $stmt = $AccConn->prepare("SELECT naim FROM pnts WHERE kods = $ID");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return iconv("Windows-1251", "UTF-8", $name);
        }
        else return false;
    }

function GetpntsValueOld($ID, $kodz, $AccConn) // получаем имя свойства из таблицы PNTS2 по идентификатору
    {
        $stmt = $AccConn->prepare("SELECT naim FROM pntsz WHERE kods = $ID AND kodz = $kodz");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return iconv("Windows-1251", "UTF-8", $name);
        }
        else return false;
    }

function GetpntsPlaceholder($ID, $AccConn) // получаем имя свойства из таблицы PNTS2 по идентификатору
    {
        $stmt = $AccConn->prepare("SELECT placeholder FROM PNTS2 WHERE kod = $ID");
        if ($stmt->execute()) 
        {
            $placeholder = $stmt->fetchColumn();
            return $placeholder;
        }
        else return false;
    }

function GetpntsValue($ID, $AccConn) // получаем имя свойства из таблицы PNTS2 по идентификатору
    {
        $stmt = $AccConn->prepare("SELECT naim FROM PNTZ WHERE kod = $ID");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }

function GetArraypntsName($IDarray, $AccConn) // получаем массив имен свойств из таблицы PNTS2 по массиву идентификаторов
    {
        $array = explode("_", $IDarray); // разбиваем строку на массив. по разделителю "_"
        $string = "";
        for ($i = 1; $i < count($array); $i++)
        {
            if ($name = GetpntsName($array[$i], $AccConn))
            {
                $string .= $name.", ";
            }
            else return false;
        }
        return substr($string, 0, -2); // обрезаем 2 символа в конце строки
    }

function GetArraypntsNameDiv($IDarray, $AccConn) // получаем массив имен свойств из таблицы PNTS2 по массиву идентификаторов
    {
        $array = explode("_", $IDarray);
        $string = "";
        for ($i = 1; $i < count($array); $i++)
        {
            if ($name = GetpntsName($array[$i], $AccConn))
            {
                $string .= "<div class='sv'>".iconv("Windows-1251", "UTF-8", $name)." </div>";
            }
        }
        return $string;
    }

function GetpntsName($ID, $AccConn) // получаем имя свойства из таблицы PNTS2 по идентификатору
    {
        $stmt = $AccConn->prepare("SELECT naim FROM PNTS2 WHERE kod = $ID");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }

function GetArrayName($IDString, $table) // получаем массив имен свойств из таблицы по массиву идентификаторов
    {
        $array = explode("_", $IDString); // разбиваем строку на массив. по разделителю "_"
        $string = "";
        for ($i = 1; $i < count($array); $i++)
        {
            $string .= GetName($array[$i], $table).", ";
        }
        return substr($string, 0, -2); // обрезаем 2 символа в конце строки
    }
//--------------------------------------------------------------------------------------------------------------------------------------------

function SetValueSV($AccConn, $val)
	{
		if ($nextNumber = GetNextNumber($AccConn, 'kod' ,'PNTZ'))
		{
			$naim = iconv("UTF-8", "Windows-1251", $val);
			$qery = "INSERT INTO PNTZ (kod, naim) VALUES ($nextNumber, '$naim')";
            $result = $AccConn->prepare($qery);
            if($result->execute())
            {
            	AddLog("Новое значение в таблице PNTZ: ".$val.", добавлено. Код ".$nextNumber);
            	return $nextNumber;
            }
            else 
            {
            	AddLog("Error SetValueSV, Код ".$nextNumber.", значение свойства ".$val);
           		return false;
           	}
		}
		else return false;
	}

function ifPNTSExist($AccConn, $value) // Если такое свойство есть в справочнике свойств вернем его код
    {
        $result = $AccConn->prepare("SELECT * FROM PNTS2");
        if ($result->execute())
        {
            $tr = 0;
            $naimval = iconv("UTF-8", "Windows-1251", $value);
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            {
                if ($row['naim'] == $naimval) // проверка на совпадение наименований
                {
                    $tr = $row['kod'];
                }
            }
            if ($tr == 0)
            {
                return false;
            }
            else
            {
                return $tr;
            }
        }
        else return false;
    }

function IfExistValueSV($AccConn, $value) // если такое значение присутствует в справочнике значений
	{
		$result = $AccConn->prepare("SELECT kod, naim FROM PNTZ");
        if ($result->execute())
        {
            $tr = 0;
            $naimval = iconv("UTF-8", "Windows-1251", $value);

            while ($row = $result->fetch(PDO::FETCH_LAZY))
            {
                if ($row['naim'] == $naimval) // проверка на совпадение наименований
                {
                    $tr = $row['kod'];
                }
            }
            if ($tr == 0)
            {
                return false;
            }
            else
            {
                return $tr;
            }
        }
        else return false;
	}

function UpdatePNT($AccConn, $field, $value, $pnt)
	{
		$val = iconv("UTF-8", "Windows-1251", $value);
		$qery = "UPDATE pnt SET $field = '$value' WHERE pnt = $pnt";
		$result = $AccConn->prepare($qery);
		if ($result->execute())
		{
			AddLog('Данные обновлены. Таблица: pnt, поле: '.$field.', ID строки: '.$pnt.', Новое значение: '.iconv("Windows-1251", "UTF-8", $value));
			return true;
		}
		else 
		{
			AddLog('Ошибка обновления данных. Таблица: pnt, поле: '.$field.', ID строки: '.$pnt.', Новое значение: '.$value);
			return false;
		}
	}

function UpdatePntlongnaim($AccConn, $field, $value, $pnt)
    {
        $val = iconv("UTF-8", "Windows-1251", $value);
        $qery = "UPDATE pntlongnaim SET $field = '$val' WHERE pnt = $pnt";
        $result = $AccConn->prepare($qery);
        if ($result->execute())
        {
            AddLog('Данные обновлены. Таблица: pntlongnaim, поле: '.$field.', ID строки: '.$pnt.', Новое значение: '.iconv("Windows-1251", "UTF-8", $val));
            return true;
        }
        else 
        {
            AddLog('Ошибка обновления данных. Таблица: pntlongnaim, поле: '.$field.', ID строки: '.$pnt.', Новое значение: '.$val);
            return false;
        }
    }

function UpdateBT($AccConn, $field, $value, $id)
    {
        $val = iconv("UTF-8", "Windows-1251", $value);
        $qery = "UPDATE BT SET $field = '$value' WHERE kod = $id";
        $result = $AccConn->prepare($qery);
        if ($result->execute())
        {
            AddLog('Данные обновлены. Таблица: BT, поле: '.$field.', ID строки: '.$id.', Новое значение: '.iconv("Windows-1251", "UTF-8", $value));
            return true;
        }
        else 
        {
            AddLog('Ошибка обновления данных. Таблица: BT, поле: '.$field.', ID строки: '.$id.', Новое значение: '.$value);
            return false;
        }
    }

function GetNAIM($AccConn, $pnt) // склеиваем и обновляем поле наименование (NAIM2)
	{
		$string = "";
		if($BT = getBTnameFromPNT($AccConn, $pnt)) // + Базовое наименование
		{
			$string .= $BT;

            if($BTplus = getBTplusNameFromPNT($AccConn, $pnt)) // если BTplus заполнено присваиваем к имени
            {
                $string .= " ".$BTplus;
            }

			if ($PNTO = getStringPNTOvalues($AccConn, $pnt)) // если PMTO заполнено присваиваем к имени
			{
				$string .= ", ".$PNTO;
			}

			return $string;
		}
		else return false;
	}

function getBTnameFromPNT($AccConn, $pnt)
	{
		$pnt = (int)$pnt;
        $res = $AccConn->prepare("SELECT BT2 FROM pnt WHERE pnt = $pnt");
        if ($res->execute())
        {
            $id = $res->fetchColumn();
            if($BTname = GetBTname($AccConn, $id))
            {
            	return $BTname;
            }
            else return false;
        }
        else return false;
	}

function getBTplusNameFromPNT($AccConn, $pnt)
	{
		$pnt = (int)$pnt;
        $res = $AccConn->prepare("SELECT BT2plus FROM pnt WHERE pnt = $pnt");
        if ($res->execute())
        {
            $id = $res->fetchColumn();
            if($BTplus = getBTplus($AccConn, $id))
            {
            	return $BTplus;
            }
            else return false;
        }
        else return false;
	}

function getBrandNameFromPNT($AccConn, $pnt)
    {
        $pnt = (int)$pnt;
        $res = $AccConn->prepare("SELECT brend FROM pnt WHERE pnt = $pnt");
        if ($res->execute())
        {
            $id = $res->fetchColumn();
            if($BTplus = getBrand($AccConn, $id))
            {
                return $BTplus;
            }
            else return false;
        }
        else return false;
    }

function getBrand($AccConn, $kod)
{
    $kod = (int)$kod;
     $stmt = $AccConn->prepare("SELECT naim FROM brends WHERE kod = $kod");
     if ($stmt->execute())
    {
        $name = $stmt->fetchColumn();
        return $name;
    }
    else return false;
}

function getStringPNTOvalues($AccConn, $pnt)
	{
		$res = $AccConn->prepare("SELECT PNTO2 FROM pnt WHERE pnt = $pnt");
        if ($res->execute())
        {
			$str = $res->fetchColumn();
        	$string = "";
        	$array = explode("_", $str);
        	for ($i=1; $i < count($array); $i++)
        	{
        		$SValue = (int)substr($array[$i], -5); // значение свойства
        		if($PntsValue = GetpntsValue($SValue, $AccConn))
        		{  		
        			$string .= $PntsValue.", ";
        		}
        	}
        	return substr($string, 0, -2);
        }
        else return false;
	}

//--------------------------------------------------------------------------------------------------------------------------------------------

function TreePrintGRK_old($AccConn)
    { ?>
        <ul class='tree'>
            <?php
            $result = $AccConn->query("SELECT * FROM grK ORDER BY posl");
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            { ?>
                <li>
                    <span class = "toggler"></span>
                    <a>
                        <span><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></span>
                    </a>

                    <?php TreePrintParent('grG', $row['kod'], $AccConn); ?>

                </li>
            <?php } ?>
        </ul>
    <?php }

function TreePrintParent($table, $IDParentCat, $AccConn)
    { ?>
        <ul>
            <?php
            $result = $AccConn->query("SELECT * FROM $table WHERE grk = $IDParentCat ORDER BY poslSite");
            while ($row = $result->fetch(PDO::FETCH_LAZY))
            { ?>
                <li>
                    <span class = "toggler"></span>
                    <a href='#'>                    
                        <strong style = "color: black;"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></strong>
                    </a>

                    <?php StandartPrint_old($row['kod'], $AccConn); ?>

                </li>
            <?php } ?>
        </ul>
    <?php }

function StandartPrint_old($grG, $AccConn)
    { ?>
        <ul>
            <li>
                <div class="layer_tree">
                    <table>
                <?php
                    if($result = $AccConn->query("SELECT * FROM BT WHERE GrG = $grG"))
                    { ?>
                        <tr class='tabheader'>
				            <td style= "width: 60px;">kod</td>
				            <td style= "width: 300px;">Наименование БТ</td>
				            <td style= "width: 500px;">Характер</td>
                            <td>edit</td>
                            <td style= "width: 60px;">Кол-во</td>
                        </tr>
                <?php
                    while ($row = $result->fetch(PDO::FETCH_LAZY))
                    { ?>
                        <tr>
                            <td><?php echo $row['kod']; ?></td>
                            <td class = "left"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></td>
                            <td class = "left"><?php echo GetArraypntsNameDiv($row['xarakter'], $AccConn); ?></td>
                            <td><a class = "edit" id = <?php echo '"'.$row['kod'].'_std'.'"'; ?>>edit</a></td>
                            <td class= "td_no_border"><a style="cursor: pointer;" id = <?php echo '"'.$row['kod'].'"'; ?> class = "show">показать</a></td>
                        </tr>
                <?php }
                    } else { ?>
                        <tr>
                            <td colspan="5">Ошибка выборки данных! StandartPrint</td>
                        </tr>
                <?php } ?>
                    </table>
                </div>
            </li>
        </ul>
<?php }

function GroupPrint($AccConn)
    {
        ?>
        <table id="grK">
            <tr class = "tabheader">
                <td>Код группы</td>
                <td>Наименование генеральной группы</td>
                <td>Последовательность</td>
            </tr>
        <?php
        $query = "SELECT * FROM grK ORDER BY posl";
        $result = $AccConn->query($query);
        while ($row = $result->fetch(PDO::FETCH_LAZY))
        { ?>
            <tr>
                <td><?php echo $row['kod']; ?></td>
                <?php 
                    if ($row['kod'] > 24)
                    { ?>
                <td class = <?php echo '"'."edit "."naim ".$row['kod'].'"'; ?> style = "text-align: left; color: red; font-weight: 600;"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></td>
                   <?php }
                    else {
                ?>
                <td class = <?php echo '"'."edit "."naim ".$row['kod'].'"'; ?> style = "text-align: left"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></td>
                <?php } ?>

                <td class = <?php echo '"'."edit "."posl ".$row['kod'].'"'; ?> style = text-align: left"><?php echo $row['posl']; ?></td>
            </tr>
        <?php } ?>
        </table>
   <?php }

function ParentGroupPrint($AccConn)
    { ?>
        <table>
            <tr class = "tabheader" id = "grG">
                <td>Код группы</td>
                <td>Наименование группы</td>
                <td>ID родительской группы</td>
                <td>Последовательность САЙТ</td>
            </tr>
    <?php
        $query = "SELECT * FROM grG ORDER BY naim";
        $result = $AccConn->query($query);
        while ($row = $result->fetch(PDO::FETCH_LAZY))
        { ?>
            <tr>
                <td><? echo $row['kod']; ?></td>
                <?php if ($row['kod'] > 635) 
                { ?>
                   <td class = <?php echo '"'."edit "."naim ".$row['kod'].'"'; ?> style = "text-align: left; color: red; font-weight: 600;"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></td>
                <?php } else { ?>
                    <td class = <?php echo '"'."edit "."naim ".$row['kod'].'"'; ?> style = "text-align: left"><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?></td>
                <?php } ?>
                <td><?php echo $row['grk']; ?></td>
                <td><?php echo $row['poslSite']; ?></td>
            </tr>
       <?php } ?>
        </table>
    <?php }

function BrendsPrint($AccConn)
    {
        $query = "SELECT * FROM brends";
        $result = $AccConn->query($query);
        echo "<table id="."'brends'"."><tr class="."'tabheader'"."><td>Код</td><td>Наименование бренда</td><td>Наименование латинское бренда</td><td class="."td_no_border"."></td></tr>";
        while ($row = $result->fetch(PDO::FETCH_LAZY))
        {
            echo "<tr><td>".$row['kod']."</td>
            <td class = '"."edit "."naim ".$row['kod']."' style = 'text-align: left'>".iconv("Windows-1251", "UTF-8", $row['naim'])."</td>
            <td class = '"."edit "."NaimLatin ".$row['kod']."' style = 'text-align: left'>".iconv("Windows-1251", "UTF-8", $row['NaimLatin'])."</td>
            <td class="."td_no_border"."><form action="."brands.php"." method="."post"."><input type = "."hidden"." name = "."DELbrands"." value=".$row['kod']."><input type="."submit"." value='УДАЛИТЬ' style='display: none;'></form></td></tr>";
        }
        echo "</table>";
    }
?>