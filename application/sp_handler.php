<?php

if (isset($_POST['BT']))
	{
		if ($_POST['BT'] !== '') 
		{
			$arr = explode("_", $_POST['BT']); // разбиваем полученные данные на 2 переменные

			$value = $arr[0]; // значение BT
			$pnt = $arr[1]; // значение кода товара
			
			$qery = "UPDATE pnt SET BT2 = $value WHERE pnt = $pnt";
			$result = $AccConn->prepare($qery);

			if ($result->execute())
			{
				AddLog("Данные в pnt обновлены, код товара: ".$pnt.", BT2: ".$value);
				if($naim = GetNAIM($AccConn, $pnt))
				{
					//AddLog("Присвоено NAIM2 = ".iconv("Windows-1251", "UTF-8", $naim).", код товара: ".$pnt);
					UpdatePNT($AccConn, 'NAIM2', $naim, $pnt);
				}
				else AddLog("Не удалось записать NAIM2, код товара: ".$pnt);
			}
			else AddLog("Ошибка в обновлении pnt: ".$pnt.", поле BT2: ".$value);

			header('Location: spravochnik.php', true, 303);
		}
		else header('Location: spravochnik.php', true, 303);
	}

else if (isset($_POST['PNTO2']))
	{
		$pnt = $_POST['PNTO2'];
                unset($_POST['PNTO2']);
                unset($_POST['chose_svs']);
                $NaimSite = $_POST['NaimSite'];
                unset($_POST['NaimSite']);

                UpdatePntlongnaim($AccConn, 'NaimSite', $NaimSite, $pnt);

                $opisanie = $_POST['opisanie'];
                unset($_POST['opisanie']);

                UpdatePntlongnaim($AccConn, 'opisanie', $opisanie, $pnt);

		$string = '';
		foreach ($_POST as $key => $value)
		{
                        if ($OldVal =  IfExistValueSV($AccConn, $value)) 
                        {
                                $string .= "_".SetSV($key).SetSV($OldVal);
                        }
                        else if ($NewVal = SetValueSV($AccConn, $value)) 
                        {
                                $string .= "_".SetSV($key).SetSV($NewVal);
                        }
		}

		if ($EDIZMcode = GetEDIZMcode($AccConn, $pnt))
		{
			if ($STRANcode = GetSTRANAcode($AccConn, $pnt))
			{
				$string .= "_".$EDIZMcode."_".$STRANcode; // дописываем в конце Еди изм (_КККККЗЗЗЗЗ) + Страна (_ЗЗЗЗЗССССС)
			}
		}

		// На выходе строка имеет вид: СССССЗЗЗЗЗ_СССССЗЗЗЗЗ_ ... + Единица измерения + Страна
		// AddLog("Данные PNTO2 в sp_handler ".$string);

		if(UpdatePNT($AccConn, 'PNTO2', $string, $pnt)) // обновляем поле PNTO2 получившимся набором
		{
			if($naim = GetNAIM($AccConn, $pnt)) // одновременно обновляем поле NAIM2 складываем (BT + BTplus + PNTO)
			{
				//AddLog("Присвоено NAIM2 = ".iconv("Windows-1251", "UTF-8", $naim).", код товара: ".$pnt);
				UpdatePNT($AccConn, 'NAIM2', $naim, $pnt);
			}
			else AddLog("Не удалось записать NAIM2, код товара: ".$pnt);
		}
		else AddLog("Не удалось обновить PNTO2, код товара: ".$pnt.", Значение: ".$string);
		header('Location: spravochnik.php', true, 303);
	}
else if (isset($_POST['oldPnt']))
	{
                $pnt = $_POST['oldPnt'];
                $NaimSite = $_POST['NaimSite'];
                $opisanie = $_POST['opisanie'];
                UpdatePntlongnaim($AccConn, 'NaimSite', $NaimSite, $pnt);
                UpdatePntlongnaim($AccConn, 'opisanie', $opisanie, $pnt);
	}
// ---------------------------------------------------------------------------------------
function GetEDIZMcode($AccConn, $pnt)
	{
		$string = '';
        $result = $AccConn->prepare("SELECT ED_IZM FROM pnt WHERE pnt = $pnt");
        if ($result->execute())
        {
        	$val = $result->fetchColumn(); // получили наименование единицы измерения
        	$val = iconv("Windows-1251", "UTF-8", $val);

        	if($EdizmCode = ifPNTSExist($AccConn, 'Единица измерения'))
        	{
        		$string .= SetSV($EdizmCode);

        		if ($EdizmValueOld = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValueOld);
        		}
        		else if ($EdizmValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValue);
        		}
        		else return false;
        	}


        	else if ($EdizmCode = setPNTO2($AccConn, 'Единица измерения'))
        	{
        		$string .= SetSV($EdizmCode);

        		if ($EdizmValueOld = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValueOld);
        		}
        		else if ($EdizmValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValue);
        		}
        		else return false;        		
        	}
        	else return false;
        }
        else return false;
	}

function GetSTRANAcode($AccConn, $pnt)
	{
		$string = '';
        $result = $AccConn->prepare("SELECT STRANA FROM pnt WHERE pnt = $pnt");
        if ($result->execute())
        {
        	$val = $result->fetchColumn(); // получили наименование Страны
        	$val = iconv("Windows-1251", "UTF-8", $val);

        	if($STRANACode = ifPNTSExist($AccConn, 'Страна'))
        	{
        		$string .= SetSV($STRANACode);
        		if ($STRANAValue = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else if ($STRANAValue = SetValueSV($AccConn, $val)) 
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else return false;
        	}


        	else if ($STRANACode = setPNTO2($AccConn, 'Страна'))
        	{
        		$string .= SetSV($STRANACode);
        		if ($STRANAValue = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else if ($STRANAValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else return false;        		
        	}
        	else return false;
        }
        else return false;
	}
?>