<?php

if (isset($_POST['addbrend']))
{
	if ($_POST['addbrend'] !== '')
	{
		$nextNumber = GetNextNumber($AccConn, 'kod' ,'brends');
		$naim = iconv("UTF-8", "Windows-1251", $_POST['addbrend']);
		$values = array($nextNumber, $naim);
		$result = $AccConn->prepare("INSERT INTO brends (kod, naim) VALUES (?, ?)");
		if ($result->execute($values))
		{
			AddLog('Данные в brends добавлены. kod: '.$nextNumber.', naim: '.$_POST['addbrend']);
		}
		else
		{
			AddLog("Ошибка добавления данных в brends. kod - ".$nextNumber.', naim: '.$_POST['addbrend']);
		}
		header('Location: brands.php', true, 303);
	}
}

?>