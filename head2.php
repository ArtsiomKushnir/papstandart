<link rel="icon" type="image/png" href="favicon.ico">	
<link rel="stylesheet" href="css/chosen.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/jquery.treemenu.css">
<link rel="stylesheet" href="css/jqueryui.custom.css" type="text/css">

<script src="http://code.jquery.com/jquery-1.8.3.js" type="text/javascript"></script>

<script src="js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="js/chosen.order.jquery.min.js" type="text/javascript"></script>
<script src="js/jqueryui.custom.js" type="text/javascript"></script>

<script src="js/scripts.js" type="text/javascript"></script>