<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СТАНДАРТ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="myModal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Описание категории <strong><i id="catId"></i> - <i id="catName"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body">
					<textarea class="form-control" rows="6" id="catDesc"></textarea>					
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="saveDesc">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container">

		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>
						
						<li>
							<a>
								СОРТИРОВКА ГРУПП mypaper
							</a>
						</li>
					</ul>
		    	</div>
			</div>
		</nav>

		<div class="row">
	  		<h3>СОРТИРОВКА ГРУПП НА САЙТЕ</h3>

	  			<style type="text/css">
	  				a.cursor {
	  					cursor: pointer;
	  				}
	  				ul {
					  list-style-type: none;
					}
	  			</style>
				<?php
					if($str = file_get_contents('http://dc.paperki.by/REST_mypaper.php?key=DcPap11111')) {
						$str = json_decode($str, true);
						treePrint ($str);
					} else echo "ERROR file_get_contents";
					//debug($str);
				?>

	  	</div>
	</div>

	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript">		
		$(document).ready(function(){ // схлопнуть дерево категорий
				$('a.closed').parent('div').parent('li').children('ul').hide();
			});

			$(document).on('click', 'a.open', function(){
				$(this).parent('div').parent('li').children('ul').hide();
				$(this).addClass('closed');
				$(this).removeClass('open');
			})

			$(document).on('click', 'a.closed', function(){
				$(this).parent('div').parent('li').children('ul').show();
				$(this).addClass('open');
				$(this).removeClass('closed');
			})
		// удаление категории
		$(document).on('click', 'button.button-delete', function(){
			$.ajax({ 
				type: "DELETE",
				url: "http://dc.paperki.by/REST_mypaper.php?"+ $.param({"Id": $(this).attr('id')}),
				async: false,
				cache: false,
				success: function(response)
				{
					location.reload();
				}
			});
		});

	</script>
	<script type="text/javascript">
		$(document).on('click', '.btn-link', function(){
			$.ajax({ 
				type: "GET",
				url: "http://dc.paperki.by/REST_mypaper.php?"+ $.param({"Id": $(this).attr('id')}),
				async: false,
				cache: false,
				success: function(json)
				{
					$('#catId').html(json.id);
					$('#catName').html(json.name);
					$('#catDesc').val(json.desc);
					//console.log(json);
					$("#myModal").modal('show');
				},
				error: function() {
					alert('Ошибка сервера');
				}
			});
		});

		$(document).on('click', '#saveDesc', function(){
			$.ajax({
				type: "POST",
				url: "http://dc.paperki.by/REST_mypaper.php",
				async: false,
				cache: false,
				data: {'id': $('#catId').html(), 'desc': $('#catDesc').val()},
				success: function(json)
				{
					//console.log(json);
					$('#myModal').modal('hide');
				},
				error: function() {
					alert('Ошибка сервера');
				}
			});
		});

	</script>
	<script type="text/javascript">
		$(document).on('dblclick', 'span.edit', function(){ // двойной клик
			$('.ajax').html($('#editbox').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<input id="editbox" size="'+ $(this).text().length+'" value="' + $(this).text() + '" class = "'+ $(this).attr('id') +'" type="text">');
			$('#editbox').focus();
		});

		$(document).on('keydown', '#editbox', function(){
		   if(event.which == 13)
		   {
				$.ajax({ 
					type: "PUT",
					url: "http://dc.paperki.by/REST_mypaper.php?"+ $.param({"order": $(this).val()}) + "&" + $.param({"id": $(this).attr('class')}),
					async: false,
					cache: false,
					success: function(json)
					{
						location.reload();
				 	},
					error: function() {
						alert('Ошибка сервера');
					}
				});
		 	}
		});
	</script>
</body>
</html>

<?php

function treePrint ($arr) {
	echo "<ul>";
	foreach ($arr as $value) {
		echo "<li>";
		if (count($value['childrens']) == '0') {
			echo '<div class = "closed">
					<span class="edit" id="'.$value['id'].'">'.$value['ordering'].'</span>
					<span> - '.$value['name'].'</span>
					<!--<button class="button-delete" id = "'.$value['id'].'">Delete</button>-->
						<button type="button" class="btn btn-link" id = "'.$value['id'].'" data-toggle="modal">
							описание
						</button>
				  </div>';
		} else {
			echo '<div><span class="edit" id="'.$value['id'].'">'.$value['ordering'].'</span><a class = "closed cursor"><span><strong> - '.$value['name'].'</strong></span></a></div>';
		}
			treePrint($value['childrens']);
		echo "</li>";
	}
	echo "</ul>";
}

function debug($arr) { ?>
	<pre>
		<?php
			print_r($arr);
		?>
	</pre>
<?php }

?>