﻿<div style="text-align:center; width: 100%; height: auto;">
<h3>Поиск по справочнику товара</h3>
	<table align = "center">
		<tr class="tabheader">
			<td>Код товара</td>
			<td>Штрих-код</td>
		</tr>
		<tr>
			<td><input type="text" id="pnt"></td>
			<td><input type="text" id="barCode" autofocus></td>
		</tr>
	</table>
</div>
<div id="resSearch" class="top5 bottomline">
	<?php// Сюда загружается результат search ?>
</div>

	<?php

	//$h3 = "Выборка по всем штрих - кодам в наличии";
	$h3 = "Выборка по в наличии на складе";

	/*$qery = 'SELECT TOP 100 pnt.pnt, pnt.NAIM, pnt.NAIM2, pnt.ARTIKUL, pnt.barCode, pnt.BT2, pnt.BT2plus, pnt.STRANA, pnt.ves, pnt.gabarits 
	FROM pnt 
	INNER JOIN scl 
	ON pnt.pnt = scl.pnt 
	WHERE ((pnt.barCode IS NOT NULL) AND (scl.ost>0)) ORDER BY pnt.barCode'; */

	$qery = "SELECT TOP 100 *
	FROM pnt
	INNER JOIN scl
	ON pnt.pnt = scl.pnt
	WHERE ((pnt.gabarits IS NULL) AND (scl.ost > 0) AND (pnt.pnt > 27737)) ORDER BY pnt.pnt";

	if ($result = $AccConn->query($qery)){
	?>

<div><h3><?php echo $h3; ?></h3>
	<table id = 'pnt' align = 'center'>
		<tr class = 'tabheader'>
			<td>Код</td>
			<td>Изображение</td>
			<td>Наименование старое</td>
			<td>Наименование новое</td>
			<td>Ед. Изм</td>
			<td>АРТИКУЛ</td>
			<td>Штрих-Код</td>
			<td>СВОЙСТВА</td>
			<td>BT2</td>
			<td>BT2plus</td>
			<td>Страна</td>
			<td>ВЕС, кг</td>
			<td>Габариты</td></tr>
			<?php
				while ($row = $result->fetch(PDO::FETCH_LAZY))
				{ ?>
					<tr>
					<td><?php echo $row['pnt']; ?></td>
					<td><a style = "cursor: pointer;" id = <?php echo '"'.$row['pnt'].'_setimage"'; ?> class = "edit">show</a></td>
					<td style = "text-align: left;"><a style= "cursor: pointer;" class = "edit" id = <?php echo '"'.$row['pnt']."_OldSv".'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row['NAIM']); ?></a></td>
					<td style = "text-align: left;"><?php echo iconv("Windows-1251", "UTF-8", $row['NAIM2']);?></td>
					<td><?php echo iconv("Windows-1251", "UTF-8", $row['ED_IZM']);?></td>
					<td class = <?php echo '"'."edit "."ARTIKUL ".$row['pnt']." pnt pnt".'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row['ARTIKUL']); ?></td>
					<td class = <?php echo '"'."edit "."barCode ".$row['pnt']." pnt pnt".'"'; ?>><?php echo $row['barCode']; ?></td>
					<?php
					if ($row['BT2'] == '' || $row['BT2'] == 0) // если не присвоен стандарт (новое Базовое наименование)
					{
						echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_sv'></a></td>";
						echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_bt'>add</a></td>";
					}
					else
					{
						echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_sv'>edit</a></td>";
						echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_bt'>".$row['BT2']."</a></td>";
					}
					if ($row['BT2plus'] == '')
					{
						$BTpluString = '';
					}
					else
					{
						$BTpluString = getBTplus($AccConn, $row['BT2plus']);
					}

					echo "<td class = '"."edit "."BT2plus ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $BTpluString)."</td>
					<td>".iconv("Windows-1251", "UTF-8", $row['STRANA'])."</td>
					<td class = '"."edit "."ves ".$row['pnt']." pnt pnt'>".$row['ves']."</td>
					<td class = '"."edit "."gabarits ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $row['gabarits'])."</td>
					</tr>";
				}
			?>
	</table>
</div>
<?php } else {echo "<div>Ошибка выполнения запроса</div>";} ?>