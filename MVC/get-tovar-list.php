<?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	if ($_POST['query'] !== '') {

		PntTablePrint($AccConn, $_POST['query'], $pnt);

	} else echo "Переданы пустые значения параметров";

?>
<?php
//----------------------------------------------------------------------------
function PntTablePrint ($AccConn, $query, $pnt) {

	if($result = $AccConn->query($query)) {
		?>
		<div class="table-responsive">
			<style type="text/css">
				th {
					cursor: pointer;
				}
			</style>
			<table class="table table-condensed table-bordered tablesorter" id="myTable" style="text-align: left;">
				<thead>
					<th>Код</th>
					<th>Наименование старое</th>
					<th>Стандарт</th>
					<th>Артикул</th>
					<th>Штрих-код</th>
					<th>Габариты</th>
					<th>Вес</th>
				</thead>

				<tbody>

			<?php
			while ($row = $result->fetch(PDO::FETCH_NUM)) { ?>
					<tr>
						<td id="updPnt"><?php echo $row[0]; ?></td>
						<td><a class="getOldPnt" style="cursor: pointer;" id=<?php echo '"'.$row[0].'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row[1]); ?></a></td>
						<td><?php echo $row[2]; ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row[3]); ?></td>
						<td class= <?php echo '"edit barCode '.$row[0].' pnt pnt"' ?>><?php echo $row[4]; ?></td>
						<td class= <?php echo '"edit gabarits '.$row[0].' pnt pnt"' ?>><?php echo $row[5]; ?></td>
						<td class= <?php echo '"edit ves '.$row[0].' pnt pnt"' ?>><?php echo $row[6]; ?></td>
					</tr>
			<?php
			} ?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript">
			$(document).ready(function() 
			{
				$("#myTable").tablesorter(); 
			});
		</script>
<?php
	} else echo "ERROR PntTablePrint query".$AccConn->errorInfo()[2];
}

?>