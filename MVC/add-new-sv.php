<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
	if (isset($_POST['name']) && $_POST['name'] != '') {
		if($oldSV = ifPNTS2ExistMew($AccConn, $_POST['name'])) {
			echo SetSV($oldSV);
		} else if ($newSv = addPNTS2New($AccConn, $_POST['name'])) {
			echo SetSV($newSv);
		} else echo 'ERROR';
	} else echo 'EMPTY VALUE';

// ================================================================================

function ifPNTS2ExistMew($AccConn, $value) // проверка существования свойства
{
	$name = iconv("UTF-8", "Windows-1251", $value);

	$query = "SELECT kod FROM PNTS2 WHERE naim = :name";

	if($result = $AccConn->prepare($query)) {
		if ($result->execute(array(':name' => $name))) {
			if($kod = $result->fetchColumn()) {
				return $kod;
			} else return false;
		} else return false;
	} else return false;
}

function addPNTS2New($AccConn, $value, $placeholder = '') {
	//$name = iconv("UTF-8", "Windows-1251", $value);
	return addPNTS2($AccConn, $value, $placeholder);
	// $query = "INSERT INTO PNTS2 (kod, naim, placeholder) VALUES (:kod, :name, :placeholder)";
}

?>