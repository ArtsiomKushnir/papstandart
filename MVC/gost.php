<style type="text/css">
	.gost{
		width: 500px;
		max-height: 550px;
		overflow-y: scroll;
		position: absolute; 
		display: none; 
		background: #fff; 
		background-color: #fff; 
		border: 1px solid #c2c2c2;
		z-index: 1000; 
		padding: 10px; 
		text-align: center;
		/*right: 50px;*/
		top: -30px;
	}
	td {
		text-align: left;
	}
</style>
<div class="gost">
	<div>
		<span>Указания по заполнению значений свойств</span><br>
		<table class="table table-condensed table-bordered">
			<tr>
				<td>Бизнесс-блокноты заводить как "Книжка записная"</td>
			</tr>
			<tr>
				<td>Штрих-коды отличаются в зависимости от фасовки одного товара единица/пачка/упаковка/</td>
			</tr>
		</table>
		<span><a href="https://orfogrammka.ru/OGL03/70091445.html" target="_blank">Более подробно о сокращениях</a></span><br>
		<span><a href="http://unicode-table.com/ru/" target="_blank">Коды специальных символов</a></span>
		<table class="table table-condensed table-bordered">
			<tr class="tabheader"><td>Знак ед. изм.</td><td>Спец символ для вставки</td></tr>
			<tr>
				<td>Корень квадратный: &#178;</td>
				<td><?php echo htmlspecialchars("&#178;", ENT_NOQUOTES); ?></td>
			</tr>
			<tr>
				<td>Корень кубический: &#179;</td>
				<td><?php echo htmlspecialchars("&#179;", ENT_NOQUOTES); ?></td>
			</tr>
			<tr>
				<td>Градус: &#176;</td>
				<td><?php echo htmlspecialchars("&#176;", ENT_NOQUOTES); ?></td>
			</tr>
		</table>
	</div>
	<div>
		<span>Общегосударственный классификатор единиц измерения</span>
		<div float="right">
			<table class="table table-condensed table-bordered">
				<tr class="tabheader"><td colspan="2">ЭКОНОМИЧЕСКИЕ ЕДИНИЦЫ</td></tr>
				<tr><td>Российский рубль</td><td>росс. руб</td></tr>
				<tr><td>Белорусский рубль</td><td>бел. руб</td></tr>
				<tr><td>Человеко-час</td><td>чел. ч</td></tr>
				<tr><td>Минимальная заработная плата</td><td>мин. заработн. плат</td></tr>
				<tr><td>Лист</td><td>л.</td></tr>
				<tr><td>100 листов</td><td>100 л.</td></tr>
				<tr><td>Единица</td><td>ед.</td></tr>
				<tr><td>Набор</td><td>набор</td></tr>
				<tr><td>Рулон</td><td>рул</td></tr>
				<tr><td>Упаковка</td><td>упак</td></tr>
				<tr><td>Штука</td><td>шт</td></tr>
				<tr><td>Комплект</td><td>компл</td></tr>
				<tr><td>Бутылка</td><td>бут</td></tr>
				<tr><td>Флакон</td><td>флак</td></tr>

				<tr class="tabheader"><td colspan="2">ЕДИНИЦЫ ДЛИНЫ</td></tr>
				<tr><td>Нанометр</td><td>нм</td></tr>
				<tr><td>Микрометр</td><td>мкм</td></tr>
				<tr><td>Миллиметр</td><td>мм</td></tr>
				<tr><td>Сантиметр</td><td>см</td></tr>
				<tr><td>Дециметр</td><td>дм</td></tr>
				<tr><td>Метр</td><td>м</td></tr>
				<tr><td>Погонный метр</td><td>пог.м</td></tr>
				<tr><td>Условный метр</td><td>усл.м</td></tr>

				<tr class="tabheader"><td colspan="2">ЕДИНИЦЫ ПЛОЩАДИ</td></tr>
				<tr><td>Квадратный миллиметр</td><td>мм&#178;</td></tr>
				<tr><td>Квадратный сантиметр</td><td>см&#178;</td></tr>
				<tr><td>Квадратный дециметр</td><td>дм&#178;</td></tr>
				<tr><td>Квадратный метр</td><td>м&#178;</td></tr>

				<tr class="tabheader"><td colspan="2">ЕДИНИЦЫ МАССЫ</td></tr>
				<tr><td>Миллиграмм</td><td>мг</td></tr>
				<tr><td>Метрический карат</td><td>кар</td></tr>
				<tr><td>Грамм</td><td>гр</td></tr>
				<tr><td>Килограмм</td><td>кг</td></tr>

				<tr class="tabheader"><td colspan="2">ТЕХНИЧЕСКИЕ ЕДИНИЦЫ</td></tr>
				<tr><td>Ватт</td><td>Вт</td></tr>
				<tr><td>Киловатт</td><td>кВт</td></tr>
				<tr><td>Вольт</td><td>В</td></tr>
				<tr><td>Киловольт</td><td>кВ</td></tr>
				<tr><td>Килокалория</td><td>ккал</td></tr>
				<tr><td>Калория в час</td><td>кал/час</td></tr>
				<tr><td>Ватт-час</td><td>Вт.ч</td></tr>
				<tr><td>Лошадиная сила</td><td>л.с.</td></tr>
				<tr><td>Бит</td><td>бит</td></tr>
				<tr><td>Байт</td><td>байт</td></tr>
				<tr><td>Килобайт</td><td>кбайт</td></tr>
				<tr><td>Мегабайт</td><td>Мбайт</td></tr>
				<tr><td>Градус Цельсия</td><td>&#176;С</td></tr>
				<tr><td>Люмен</td><td>лм</td></tr>
				<tr><td>Герц</td><td>Гц</td></tr>
				<tr><td>Мегагерц</td><td>МГц</td></tr>

				<tr class="tabheader"><td colspan="2">ЕДИНИЦЫ ВРЕМЕНИ</td></tr>
				<tr><td>Миллисекунда</td><td>мс</td></tr>
				<tr><td>Секунда</td><td>с</td></tr>
				<tr><td>Минута</td><td>мин</td></tr>
				<tr><td>Час</td><td>ч</td></tr>
				<tr><td>Квартал</td><td>кварт</td></tr>
				<tr><td>Год</td><td>г; лет</td></tr>

			</table>
		</div>
	</div>
</div>
