<?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	$pnt = $_POST['id'];
	$id ='';

	$query = "SELECT BT2 FROM pnt WHERE pnt = $pnt";
	if ($result = $AccConn->prepare($query)) {
		if ($result->execute()) {
			while ($row = $result->fetch(PDO::FETCH_LAZY)){
				$id = $row['BT2'];
			}
		}
	}

?>
<select name ='BT' data-placeholder="Выберите СТАНДАРТ..." class="chosen-select" id="Standart" >
	<option value=""></option>
	<?php
		$query = "SELECT kod, naim, GrG FROM BT ORDER BY GrG";
		$result = $AccConn->prepare($query);
		if ($result->execute())
		{
			$stek = 0;
			while ($row = $result->fetch(PDO::FETCH_LAZY))
			{
				$GrG = $row['GrG'];
				if ($stek !== $GrG) 
				{
					if ($stek != 0) 
					{
						echo "</optgroup>";
					}
					if ($GRGName = GetGRGName($AccConn, $GrG))
					{
						echo "<optgroup label = '".iconv("Windows-1251", "UTF-8", $GRGName)."'>";
					} else echo "<optgroup label = 'Error_GetGRGName'>";
				}

				if ($id == $row['kod']) {
					echo "<option value = '".$row['kod']."' selected>".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
				} else echo "<option value = '".$row['kod']."'>".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";

				$stek = $GrG;
			}
			echo "</optgroup>";
		} else echo "<option>Error</option>";
	?>
</select>

<div class="alert alert-danger">Сбросить свойства и значения <input id="resetPNT" type="checkbox" aria-label="Одновременно сбросить свойства и значения для товара"></div>

<script type="text/javascript">
	$('.chosen-select').chosen({width: "100%"});
</script>