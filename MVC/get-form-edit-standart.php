<?php
	$id = $_GET['id'];

	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	$query = "SELECT naim, GrG, descr, xarakter FROM BT WHERE kod = $id";
	if($result = $AccConn->query($query))
	{
		while ($row = $result->fetch(PDO::FETCH_LAZY))
		{
			$naim = iconv("Windows-1251", "UTF-8", $row['naim']);
			$OldGrg = $row['GrG'];
			$Descr = $row['descr'];
			$xarakter = $row['xarakter'];
		}
	}

?>
<!-- Наименование стандарта -->
<div class="input-group">
	<span class="input-group-addon">Наименование стандарта (BT):</span>
	<input class="form-control" type="text" id="BTname" value=<?php echo '"'.htmlspecialchars($naim, ENT_NOQUOTES).'"'; ?>>
</div>
<!-- Набор свойств -->
<div class="panel panel-default">
	<div class="panel-heading">
		Набор свойств:
	</div>
	<div class="panel-body">
	<style type="text/css">
		span.label {
			font-size: 100%;
			padding: .4em .8em .6em;
			line-height: 2.4;
			cursor: default;
		}
		span.glyphicon-remove {
			cursor: pointer;
		}
		span.label-success {
			cursor: pointer;
		}
		span.glyphicon-plus {
			cursor: pointer;
		}
		span.green {
			background-image: linear-gradient(to bottom,#5cb85c 0,#419641 100%);
			background-repeat: repeat-x;
    		border-color: #3e8f3e;
			font-weight: 700;
			color: #fff;
    		background-color: #5cb85c;
    		text-shadow: 0 -1px 0 rgba(0,0,0,.2);
		}
		span.red {
			font-weight: 700;
			background-image: linear-gradient(to bottom,#d9534f 0,#c12e2a 100%);
			background-repeat: repeat-x;
			border-color: #b92c28;
			text-shadow: 0 -1px 0 rgba(0,0,0,.2);
			color: #fff;
    		background-color: #d9534f;
		}
	</style>
		<form id="xarakter">
			<ul id = 'sortable'>
			<?php
				GetArraypntsNameUI2($xarakter, $AccConn);
				addPnts();
			?>
			</ul>
			<script>
				$("#sortable").sortable().disableSelection();
			</script>
		</form>
	</div>
</div>
<!-- Группа стандарта -->
<select data-placeholder="Выберите группу..." class="chosen-select" id="gr">
	<option value =""></option>
<?php
		$query = "SELECT grG.kod, grG.naim AS grGnaim, grG.grk, grK.naim AS grKnaim
					FROM grG
					INNER JOIN grK
					ON grG.grk = grK.kod 
					ORDER BY grK.posl";

		if($result = $AccConn->query($query))
		{
			$stek = 0;
			while ($row = $result->fetch(PDO::FETCH_LAZY))
			{
				$GrK = $row['grk'];
				if ($stek !== $GrK)
				{
					if ($stek != 0)
					{
						echo "</optgroup>";
					}												
					echo "<optgroup label = '".iconv("Windows-1251", "UTF-8", $row['grKnaim'])."'>";
				}

				if (isset($OldGrg))
				{
					if ($OldGrg == $row['kod'])
					{
						echo "<option value = ".$row['kod']." selected>".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
					}
					else
					{
						echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
					}
				}
				else
				{
					echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
				}
				$stek = $GrK;
			}
			echo "</optgroup>";
		}
		else echo "<option>Error</option>";
?>
</select>
<script type="text/javascript">
	$('.chosen-select').chosen({width: "100%"});
</script>


<?php
function GetArraypntsNameUI2($xarakter, $AccConn)
    {
        $array = explode("_", $xarakter);
        for ($i = 1; $i < count($array); $i++)
        {
?>
				<li>
					<span class="label label-primary">
						<?php
							$namePnts = iconv("Windows-1251", "UTF-8", GetpntsName($array[$i], $AccConn));
							echo $namePnts; ?>
						<input class="form-control" type='hidden' name = <?php echo '"'.$array[$i].'"'; ?> value=<?php echo '"'.htmlspecialchars($namePnts, ENT_NOQUOTES).'"'; ?> >
						<span style="top: 3px;" class="glyphicon glyphicon-remove" aria-hidden="true" onclick="javascript: $(this).parents('li').remove();"></span>
					</span>
				</li>
<?php
        }
	}

function addPnts() {
?>

	<li id="addSvLi">
		<span class="label label-success" onclick="addSvLi()">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>					
			Свойство
		</span>
	</li>

<?php
}

?>