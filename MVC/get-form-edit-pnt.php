	<style type="text/css">
		
		ul {
			list-style-type: none;
		}
		li {
			margin-bottom: 3px;
		}
		div.input-group {
			cursor: pointer;
		}
		span.label {
			font-size: 100%;
			padding: .4em .8em .6em;
			line-height: 2.4;
			cursor: default;
		}
		span.glyphicon-remove {
			cursor: pointer;
		}
		span.label-success {
			cursor: pointer;
		}
		span.glyphicon-plus {
			cursor: pointer;
		}
		span.green {
			background-image: linear-gradient(to bottom,#5cb85c 0,#419641 100%);
			background-repeat: repeat-x;
    		border-color: #3e8f3e;
			font-weight: 700;
			color: #fff;
    		background-color: #5cb85c;
    		text-shadow: 0 -1px 0 rgba(0,0,0,.2);
		}
		span.red {
			font-weight: 700;
			background-image: linear-gradient(to bottom,#d9534f 0,#c12e2a 100%);
			background-repeat: repeat-x;
			border-color: #b92c28;
			text-shadow: 0 -1px 0 rgba(0,0,0,.2);
			color: #fff;
    		background-color: #d9534f;
		}
	</style>
	<span class="glyphicon glyphicon-info-sign" aria-hidden="true" onmouseover="$(this).children('div').css('display', 'block');" onmouseout="$(this).children('div').css('display', 'none');">
		<?php include 'gost.php'; ?>
	</span>
	<form method="post" id="PNT2">

<?php
		include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
		$id = $_POST['id'];

		$query = "SELECT NAIM, PNTO2, BT2 FROM pnt WHERE pnt = $id";

		if ($result = $AccConn->prepare($query)) 
		{
			if ($result->execute())
			{
				while ($row = $result->fetch(PDO::FETCH_LAZY))
				{
					if ($row['PNTO2'] == '')
					{ ?>
							<h3><?php echo iconv("Windows-1251", "UTF-8", $row['NAIM']); ?></h3>
							<div class="panel panel-default">
								<div class="panel-heading">
									Свойства не заполнены, заполнить по шаблону:
								</div>
								<div class="panel-body">
									<ul id = 'sortable'>
										<?php printXarakterToAdd2($AccConn, $row['BT2']); ?>
									</ul>
								</div>
							</div>															
				<?php 
					}

					else
					{ ?>
							<h3><?php echo iconv("Windows-1251", "UTF-8", $row['NAIM']); ?></h3>
							<div class="panel panel-default">
								<div class="panel-heading">
									Редактор значений свойств:
								</div>
								<div class="panel-body">
									<ul id = 'sortable'>
										<?php printPNTONameAndValue2($AccConn, $row['PNTO2']); ?>
									</ul>
								</div>
							</div>
				<?php									
					}
				}
			}
		}
?>
	</form>
	<script type="text/javascript">
		$('#sortable').sortable().disableSelection();
	</script>

<?php

function printPNTONameAndValue2($AccConn, $idString)
{
	$arrSV = explode("_", $idString);
	for ($i=1; $i < count($arrSV); $i++)
	{ 
		$SV = (int)substr($arrSV[$i], 0, -5); // код свойства
		$SVname = GetpntsName($SV, $AccConn);
		$SVnameConv = iconv("Windows-1251", "UTF-8", $SVname);

		$SValue = (int)substr($arrSV[$i], -5); // код значения свойства
		$SValueName = GetpntsValue($SValue, $AccConn);
		$SValueNameConv = iconv("Windows-1251", "UTF-8", $SValueName);

		if($SVnameConv !== "Единица измерения" && $SVnameConv !== "Страна")
		{
?>
			<li>
				<div class="input-group">
					<span class="input-group-addon"><?php echo $SVnameConv; ?></span>
					<input class="form-control" type='text' name = <?php echo '"'.$SV.'"'; ?> placeholder = <?php echo '"'.$placeholder.'"'  ?> value = <?php echo '"'.htmlspecialchars($SValueNameConv, ENT_NOQUOTES).'"'; ?>>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-remove" aria-hidden="true" onclick="javascript: $(this).parents('li').remove();"></span>
					</span>
				</div>
			</li>
<?php
		}
	}
}


 ?>

 <?php
function printXarakterToAdd2($AccConn, $BT)
{
	$stmt = $AccConn->prepare("SELECT xarakter FROM BT WHERE kod = $BT");
	if ($stmt->execute()) 
	{
		$xarakter = $stmt->fetchColumn();
		$arrSV = explode("_", $xarakter);
		for ($i=1; $i < count($arrSV); $i++)
		{ 
			$SV = $arrSV[$i]; // код свойства
			$SVal = GetpntsName($SV, $AccConn);
			$placeholder = iconv("Windows-1251", "UTF-8", GetpntsPlaceholder($SV, $AccConn));
?>
			<li>
				<div class="input-group">
					<span class="input-group-addon"><?php echo iconv("Windows-1251", "UTF-8", $SVal); ?></span>
					<input class="form-control" type='text' name = <?php echo '"'.$SV.'"'; ?> placeholder = <?php echo '"'.$placeholder.'"'; ?>>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-remove" aria-hidden="true" onclick="javascript: $(this).parents('li').remove();"></span>
					</span>
				</div>
			</li>
<?php
		}
	}
	else echo 'error';
}

?>