<?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	if ($_GET['pnt'] !== '') {
		$pnt = $_GET['pnt'];

		$query = "SELECT pnt.pnt AS Pnt
						, pnt.NAIM AS NaimOld
						, pnt.NAIM2 AS NaimNew
						, pnt.BT2 AS BT2
						, pnt.BT2plus AS BTplus
						, pnt.brend AS Brend
						, pnt.STRANA AS Strana 
						, pnt.ED_IZM AS EdIzm
						, pnt.ARTIKUL AS Articul
						, pnt.barCode AS BarCode
						, pnt.ves AS Ves
						, pnt.gabarits AS Gabarits
					FROM pnt
					WHERE pnt.pnt = $pnt";

		PntTablePrint($AccConn, $query, $pnt);

	} else echo "Переданы пустые значения параметров";

?>
<?php
//----------------------------------------------------------------------------
function PntTablePrint ($AccConn, $query, $pnt) {

	if($result = $AccConn->query($query)) {
		?>
		<div class="table-responsive">
		<style type="text/css">
			input.form-control {
				margin-bottom: 5px;
			}
		</style>
			<table class="table table-condensed table-bordered" style="font-size: 10px; text-align: left;">
				<thead>
					<th>Код</th>
					<th>Наименование старое</th>
					<th>Страна</th>
					<th>Ед изм.</th>
					<th>АРТИКУЛ</th>
					<th>Штрих-Код</th>
					<th>Габариты</th>
					<th>Вес</th>
				</thead>

				<tbody>

			<?php
			while ($row = $result->fetch(PDO::FETCH_ASSOC)) { ?>
					<tr>
						<td id="updPnt"><?php echo $row['Pnt']; ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['NaimOld']); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['Strana']); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['EdIzm']); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['Articul']); ?></td>
						<td class= <?php echo '"edit barCode '.$row['Pnt'].' pnt pnt"' ?>><?php echo $row['BarCode']; ?></td>
						<td class= <?php echo '"edit gabarits '.$row['Pnt'].' pnt pnt"' ?>><?php echo $row['Gabarits']; ?></td>
						<td class= <?php echo '"edit ves '.$row['Pnt'].' pnt pnt"' ?>><?php echo $row['Ves']; ?></td>
					</tr>
			<?php
			} ?>
				</tbody>
			</table>
		</div>
			<?php
				$result = $AccConn->prepare("SELECT opisanie, NaimSite FROM pntlongnaim WHERE pnt = $pnt");
				if($result->execute()) {
					$row = $result->fetch(PDO::FETCH_ASSOC);

					$opisanie = iconv("Windows-1251", "UTF-8", $row['opisanie']);
					$NaimSite = iconv("Windows-1251", "UTF-8", $row['NaimSite']);
				}
			?>
	
			<span class="glyphicon glyphicon-info-sign" aria-hidden="true" onmouseover="$(this).children('div').css('display', 'block');" onmouseout="$(this).children('div').css('display', 'none');">
				<?php include 'gost.php'; ?>
			</span>
			<input id="pntShortNaim" type="text" class="form-control" placeholder="СОКРАЩЕННОЕ НАИМЕНОВАНИЕ САЙТ" value=<?php echo '"'.htmlspecialchars($NaimSite, ENT_NOQUOTES).'"'; ?>>
			<textarea class="form-control" rows="8" id="pntDesc" placeholder="Описание отсутствует"><?php echo htmlspecialchars($opisanie, ENT_NOQUOTES); ?></textarea>

			<?php
		
	} else echo "Ошибка выполнения запроса".$AccConn->errorInfo()[2];
}

?>