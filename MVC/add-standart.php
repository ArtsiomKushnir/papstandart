<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

if (isset($_POST['BT']) && $_POST['BT'] != '' 
	&& isset($_POST['svs'])
	&& isset($_POST['grg']) && $_POST['grg'] != '') {


	// mysql_real_escape_string - экранирует спецсимволы
	$naim = iconv("UTF-8", "Windows-1251", $_POST['BT']);
	$svs = GetString($_POST['svs']);
	$grg = $_POST['grg'];

	$nextNumber = GetNextNumber($AccConn, 'kod' ,'BT');
	$values = array($nextNumber, $naim, $svs, $grg);
	$query = "INSERT INTO BT (kod, naim, xarakter, GrG) VALUES (?, ?, ?, ?)";
	if($result = $AccConn->prepare($query)) {
		if ($result->execute($values)) {
			AddLog("Данные в BT добавлены kod: ".$nextNumber.', naim: '.$_POST['BT'].', xarakter: '.$svs.', код группы: '.$grg);
			echo "OK";
		} else echo "ERROR execute query";
	} else echo "ERROR prepare query";
} else echo "ERROR empty parameters";

?>