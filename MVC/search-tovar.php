<?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	if( (isset($_GET['pnt']) && $_GET['pnt'] != '') || 
		(isset($_GET['barCode']) && $_GET['barCode'] != '') || 
		(isset($_GET['naim']) && $_GET['naim'] != '') || 
		(isset($_GET['articul']) && $_GET['articul'] != '')) {

		$where = "";
		if (isset($_GET['pnt']) && $_GET['pnt'] != '') 					$where = addWhere($where, "pnt.pnt = ".htmlspecialchars($_GET['pnt']));
		if (isset($_GET['barCode']) && $_GET['barCode'] != '') {
			$barCode = htmlspecialchars($_GET['barCode']); $barCode = iconv("UTF-8", "Windows-1251", $barCode);
			$where = addWhere($where, "pnt.barCode Like '".$barCode)."'";
		}
		if (isset($_GET['naim']) && $_GET['naim'] != '') { 
			$naim = htmlspecialchars($_GET['naim']); $naim = iconv("UTF-8", "Windows-1251", $naim);
			$where = addWhere($where, "pnt.NAIM Like '%".$naim)."%'"; 
		}
		if (isset($_GET['articul']) && $_GET['articul'] != '') {
			$articul = htmlspecialchars($_GET['articul']); $articul = iconv("UTF-8", "Windows-1251", $articul);
			$where = addWhere($where, "pnt.ARTIKUL Like '%".$articul)."%'";
		}

		$query = "SELECT		pnt.pnt AS Pnt,
								pnt.NAIM AS NaimOld,
								pnt.NAIM2 AS NaimNew,
								pnt.PNTO2 AS PNTO2,
								pnt.BT2 AS BT2,
								BT.naim AS BT2name,
								pnt.BT2plus AS BTplus,
								pnt.brend AS Brend,
								pnt.STRANA AS Strana ,
								pnt.ED_IZM AS EdIzm,
								pnt.ARTIKUL AS Articul,
								pnt.barCode AS BarCode,
								pnt.ves AS Ves,
								pnt.gabarits AS Gabarits
						FROM pnt
						LEFT JOIN BT ON BT.kod = pnt.BT2";
		if ($where) $query .= " WHERE ".$where;
		
		// echo $query;

		PntTablePrint($AccConn, $query);

	} else echo "Переданы пустые значения параметров";


?>
<?php
//----------------------------------------------------------------------------

function addWhere($where, $add, $and = true) {
	if ($where) {
		if ($and) $where .= " AND $add";
		else $where .= " OR $add";
	}
	else $where = $add;
	return $where;
}

function PntTablePrint ($AccConn, $query) {

	if($result = $AccConn->query($query)) {
		?>
		<div class="table-responsive">
			<style type="text/css">
				a {
					cursor: pointer;
				}
				th {
					cursor: pointer;
				}
			</style>
			<table class="table table-condensed table-bordered tablesorter" id="myTable" style="text-align: left;">
				<thead>
					<th>Код</th>
					<th>Изображение</th>
					<th>Наименование</th>
					<th>Свойства</th>
					<th>СТАНДАРТ</th>
					<th>BT2plus</th>
					<th>Бренд</th>
					<th>Страна</th>
					<th>Ед изм.</th>
					<th>АРТИКУЛ</th>
					<th>Штрих-Код</th>
					<th>Вес, кг</th>
					<th>Габариты</th>
				</thead>

				<tbody>

			<?php
			while ($row = $result->fetch(PDO::FETCH_ASSOC)) { ?>
					<tr>
						<td><?php echo $row['Pnt']; ?></td>
						<td><button type="button" class="btn btn-sm btn-info" onclick="showImageForm(<?php echo $row['Pnt']; ?>)"><strong>EDIT</strong></button></td>
						<td><a class="getOldPnt" id=<?php echo '"'.$row['Pnt'].'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row['NaimOld']); ?></a></td>
					<?php

							if ($row['BT2'] == '' OR $row['BT2'] == '0') {
								if ($row['PNTO2'] == '') { ?>
									<td></td>
					<?php 		} else { ?>
									<td><button type="button" class="btn btn-sm btn-info" onclick="pnt(<?php echo $row['Pnt']; ?>)"><strong>редактировать</strong></button></td>
					<?php		}

							} else {

								if ($row['PNTO2'] == '') { ?>
									<td><button type="button" class="btn btn-sm btn-default" onclick="pnt(<?php echo $row['Pnt']; ?>)"><strong>заполнить</strong></button></td>
					<?php 		} else { ?>
									<td><button type="button" class="btn btn-sm btn-info" onclick="pnt(<?php echo $row['Pnt']; ?>)"><strong>редактировать</strong></button></td>
					<?php		}
							}
					?>

						<!-- <td></td>
						<td><?php //echo $row['BTplus']; ?></td> -->
							<?php if ($row['BT2'] == '' OR $row['BT2'] == '0') { ?>
								<td><button type="button" class="btn btn-sm btn-default" onclick="Standart(<?php echo $row['Pnt']; ?>)"><strong>ПРИСВОИТЬ</strong></button></td>
							<?php
							} else { ?>
								<td><button type="button" class="btn btn-sm btn-info" onclick="Standart(<?php echo $row['Pnt']; ?>)"><strong><?php echo iconv("Windows-1251", "UTF-8", $row['BT2name']); ?></strong></button></td>
							<?php
							} ?>
						<td class= <?php echo '"edit BT2plus '.$row['Pnt'].' pnt pnt"' ?>><?php echo iconv("Windows-1251", "UTF-8", getBTplus($AccConn, $row['BTplus'])); ?></td>
						<td class= <?php echo '"edit brend '.$row['Pnt'].' pnt pnt"' ?>><?php echo iconv("Windows-1251", "UTF-8", getBrand($AccConn, $row['Brend'])); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['Strana']); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['EdIzm']); ?></td>
						<td><?php echo iconv("Windows-1251", "UTF-8", $row['Articul']); ?></td>
						<td class= <?php echo '"edit barCode '.$row['Pnt'].' pnt pnt"' ?>><?php echo $row['BarCode']; ?></td>
						<td class= <?php echo '"edit ves '.$row['Pnt'].' pnt pnt"' ?>><?php echo $row['Ves']; ?></td>
						<td class= <?php echo '"edit gabarits '.$row['Pnt'].' pnt pnt"' ?>><?php echo iconv("Windows-1251", "UTF-8", $row['Gabarits']); ?></td>
					</tr>
			<?php
			} ?>
				</tbody>
			</table>
			<script type="text/javascript">
				$(document).ready(function() 
				{
					$("#myTable").tablesorter(); 
				});
			</script>
		</div>

			<?php
		
	} else echo "Ошибка выполнения запроса".$AccConn->errorInfo()[2];
}

?>