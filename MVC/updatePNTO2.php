<?php

	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

	$id = $_POST['id'];
	unset($_POST['id']);

	foreach ($_POST['data'] as $value)
	{
		if ($OldVal =  IfExistValueSV($AccConn, $value['value'])) 
		{
			$string .= "_".SetSV($value['name']).SetSV($OldVal);
		}
		else if ($NewVal = SetValueSV($AccConn, $value['value'])) 
		{
			$string .= "_".SetSV($value['name']).SetSV($NewVal);
		}
	}

	if ($EDIZMcode = GetEDIZMcode($AccConn, $id))
	{
		if ($STRANcode = GetSTRANAcode($AccConn, $id))
		{
			$string .= "_".$EDIZMcode."_".$STRANcode; // дописываем в конце Еди изм (_КККККЗЗЗЗЗ) + Страна (_ЗЗЗЗЗССССС)
		}
	}

	// На выходе строка имеет вид: СССССЗЗЗЗЗ_СССССЗЗЗЗЗ_ ... + Единица измерения + Страна

	if(UpdatePNT($AccConn, 'PNTO2', $string, $id)) // обновляем поле PNTO2 получившимся набором
	{
		if($naim = GetNAIM($AccConn, $id)) // одновременно обновляем поле NAIM2 складываем (BT + BTplus + PNTO)
		{
			UpdatePNT($AccConn, 'NAIM2', $naim, $id);
		} else AddLog("Не удалось записать NAIM2, код товара: ".$id);
	} else AddLog("Не удалось обновить PNTO2, код товара: ".$id.", Значение: ".$string);


// ====================================================================

function GetEDIZMcode($AccConn, $pnt)
	{
		$string = '';
        $result = $AccConn->prepare("SELECT ED_IZM FROM pnt WHERE pnt = $pnt");
        if ($result->execute())
        {
        	$val = $result->fetchColumn(); // получили наименование единицы измерения
        	$val = iconv("Windows-1251", "UTF-8", $val);

        	if($EdizmCode = ifPNTSExist($AccConn, 'Единица измерения'))
        	{
        		$string .= SetSV($EdizmCode);

        		if ($EdizmValueOld = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValueOld);
        		}
        		else if ($EdizmValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValue);
        		}
        		else return false;
        	}

        	else if ($EdizmCode = setPNTO2($AccConn, 'Единица измерения'))
        	{
        		$string .= SetSV($EdizmCode);

        		if ($EdizmValueOld = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValueOld);
        		}
        		else if ($EdizmValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($EdizmValue);
        		}
        		else return false;        		
        	}
        	else return false;
        }
        else return false;
	}

function GetSTRANAcode($AccConn, $pnt)
	{
		$string = '';
        $result = $AccConn->prepare("SELECT STRANA FROM pnt WHERE pnt = $pnt");
        if ($result->execute())
        {
        	$val = $result->fetchColumn(); // получили наименование Страны
        	$val = iconv("Windows-1251", "UTF-8", $val);

        	if($STRANACode = ifPNTSExist($AccConn, 'Страна'))
        	{
        		$string .= SetSV($STRANACode);
        		if ($STRANAValue = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else if ($STRANAValue = SetValueSV($AccConn, $val)) 
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else return false;
        	}

        	else if ($STRANACode = setPNTO2($AccConn, 'Страна'))
        	{
        		$string .= SetSV($STRANACode);
        		if ($STRANAValue = IfExistValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else if ($STRANAValue = SetValueSV($AccConn, $val))
        		{
        			return $string .= SetSV($STRANAValue);
        		}
        		else return false;        		
        	}
        	else return false;
        }
        else return false;
	}

?>