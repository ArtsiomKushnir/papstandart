<?php

header('Cache-Control: no-cache, must-revalidate');
header('Content-Type: application/json;charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');

include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

class Standart {

	var $id;
	var $name;
	var $desc;

	function __construct ($id, $name, $desc) {
		$this->id = $id;
		$this->name = $name;
		$this->desc = $desc;
	}

	function __destruct() {}
}

class Category {

	var $kod;
	var $naim;
	var $posl;

	function __construct ($kod, $naim, $posl) {
		
		$this->kod = $kod;
		$this->naim = $naim;
		$this->posl = $posl;
	}

	function __destruct() {}
}

	$objArr = new ArrayObject();

	$result = $AccConn->query("SELECT kod, naim, posl FROM grK ORDER BY posl");
	while ($row = $result->fetch(PDO::FETCH_LAZY)) {
		$category = new Category($row['kod'], iconv("Windows-1251", "UTF-8", $row['naim']), $row['posl']);
		$objArr->append($category);
	}

	echo json_encode($objArr, JSON_UNESCAPED_UNICODE);

	unset($objArr);

?>