<?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>

	<form>
	<style type="text/css">
		div.form-horizontal {
			margin-bottom: 3px;
		}
		span.input-group-addon {
			cursor: pointer;
		}
	</style>
		<div class="form-horizontal">
			<label for="BT">Наименование Базового товара</label>
			<input name="BT" type="text" class="form-control" placeholder="БТ" id="BT">
		</div>
		<div class="form-horizontal" id="getsvs">
			<?php include_once 'svs.php'; ?>
		</div>
		<div class="form-horizontal">
			<div class="input-group" style="width: 45%;">
				<input class="form-control" placeholder="Название нового свойства" id="appendedAddSv" type="text">
				<span class="input-group-addon" id="basic-addon2" onclick="addSV2($('#appendedAddSv').val())">ДОБАВИТЬ СВОЙСТВО</span>
			</div>
		</div>
		<div class="form-horizontal">
			<label for="grG">Группа, к которой отнести стандарт:</label>
				<select name = "gr" data-placeholder="Выберите группу..." class="chosen-select" id="grG">
					<option value =""></option>
					<?php
						$query = "SELECT grG.kod
										, grG.naim AS grGnaim
										, grG.grk
										, grK.naim AS grKnaim
									FROM grG
									INNER JOIN grK ON grG.grk = grK.kod 
									ORDER BY grK.posl";

						if($result = $AccConn->query($query)) {
							$stek = 0;
							while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
								// stripslashes() - убирает экранирование
								$GrK = $row['grk'];
								if ($stek !== $GrK) {
									if ($stek != 0) { ?>
										</optgroup> <?php
									} ?>
									<optgroup label = <? echo '"'.iconv("Windows-1251", "UTF-8", $row['grKnaim']).'"'; ?>> <?php
								} ?>
								<option value = <?php echo '"'.$row['kod'].'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row['grGnaim']); ?></option> <?php
								$stek = $GrK;
							} ?>
							</optgroup> <?php
						} else echo "<option>Error</option>";
					?>
				</select>
		</div>
	</form>

	<script type="text/javascript">
		$('.chosen-select').chosen({width: "100%"});
	</script>