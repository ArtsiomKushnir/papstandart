<?php

	include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

if (isset($_POST['BT']) && $_POST['BT'] != '' 
	&& isset($_POST['xarakter'])
	&& isset($_POST['gr']) && $_POST['gr'] != '') {

	$id = $_POST['id'];
	// mysql_real_escape_string - экранирует спецсимволы
	$BT = iconv("UTF-8", "Windows-1251", $_POST['BT']);
	$xarakter = GetString2($_POST['xarakter']);
	$gr = $_POST['gr'];

	$values = array($BT, $xarakter, $gr, $id);

	$query = "UPDATE BT SET naim = ?, xarakter = ?, GrG = ? WHERE kod = ?";
	if($result = $AccConn->prepare($query)) {
		if ($result->execute($values)) {
			AddLog("Данные в BT обновлены kod: ".$id.', naim: '.$_POST['BT'].', xarakter: '.$xarakter.', код группы: '.$gr);
			echo "OK";
		} else {
			echo 'ERROR execute ('.$query.')';
			print_r($values);
		}
	} else {
		echo 'ERROR prepare ('.$query.')';
		print_r($values);
	}
} else echo 'ERROR parameters';


function GetString2($array) // собираем строку из значений массива с разделителем "_"
{
	$string = "";
	foreach ($array as $id)
	{
		if ($id['name'] !== '')
		{
			$string .= "_".$id['name'];
		}
	}
	return $string;
}

?>