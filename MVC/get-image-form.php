<?php $id = $_GET['id']; ?>

<style type="text/css">
	div.dropZone, div.dropZone1, div.dropZone2 {
	    width: 200px; 
	    height: 200px; 
	    margin: 3px; 
	    float: left;
	    overflow: hidden;
	    display: block;

	    color: #555;
	    font-size: 12px;
	    text-align: center;
	    
	    background: #fff;

	    border: 1px solid #c2c2c2; 
	    -webkit-border-radius: 5px;
	    -moz-border-radius: 5px;
	    border-radius: 5px;
	}

	#dropZone, #dropZone1, #dropZone2 {
	    display: inline-block;
	    position: relative;
	    width: 200px;
	    height: 200px;
	    -webkit-border-radius: 5px;
	    -moz-border-radius: 5px;
	    border-radius: 5px;
	}

	#dropZone.hover, #dropZone1.hover, #dropZone2.hover {
	    background-color: rgba(0,0,255,0.1);
	}

	#dropZone.error, #dropZone1.error, #dropZone2.error {
	    background-color: rgba(255,0,0,0.1);
	}

	#dropZone.drop, #dropZone1.drop, #dropZone2.drop {
	    background-color: rgba(0,255,0,0.1);
	}
</style>
<div class="form-horizontal text-center">
	<span>
		Размер изображения ограничен: <strong>300 Kb</strong><br>
		Только файлы с расширением <strong style="color: red;">.jpg</strong><br>
		Перетащите файл изображения на соответствующее поле
	</span>
</div>
<div class="form-horizontal text-center">
	<?php
		include $_SERVER['DOCUMENT_ROOT'].'/application/config.php';

		$ftp_conn = ftp_connect($ftp_server) or die ('Не удалось установить соединение с: '.$ftp_server);
		$login_result = ftp_login($ftp_conn, $ftp_user_name, $ftp_user_pass) or die ('Не удалось авторизироваться: '.$ftp_conn);
		$buff = ftp_nlist($ftp_conn, '/paperki.by/www/images/catalog/');
		ftp_close($ftp_conn);

		$icode = '00000000';
		$len = strlen($id);
		if ($len == '1') {$icode = '0000000'.$id;}
		else if ($len == '2') {$icode = '000000'.$id;}
		else if ($len == '3') {$icode = '00000'.$id;}
		else if ($len == '4') {$icode = '0000'.$id;}
		else if ($len == '5') {$icode = '000'.$id;}
		else if ($len == '6') {$icode = '00'.$id;}
		else if ($len == '7') {$icode = '0'.$id;}
		else if ($len == '8') {$icode =  $id;}

		$full_image = $icode.'.jpg';
		$full_image1 = $icode.'-1.jpg';
		$full_image2 = $icode.'-2.jpg';

		if (FileExist($full_image, $buff)){$full_image = $URLimagefolder.$icode.'.jpg';}else{$full_image = $URLimagefolder.'noimage.jpg';}
		if (FileExist($full_image1, $buff)){$full_image1 = $URLimagefolder.$icode.'-1.jpg';}else{$full_image1 = $URLimagefolder.'noimage.jpg';}
		if (FileExist($full_image2, $buff)){$full_image2 = $URLimagefolder.$icode.'-2.jpg';}else{$full_image2 = $URLimagefolder.'noimage.jpg';}
	?>

	<div class="row">
		<div class="col-md-12 text-center">
			<div style="background-image: url(<?php echo $full_image; ?>); background-size: 100%; background-repeat: no-repeat;" class = "dropZone">
				<form action="/imageLoader.php">
					<div id="dropZone">

					</div>
				</form>
			</div>
			<div style="background-image: url(<?php echo $full_image1; ?>); background-size: 100%; background-repeat: no-repeat;" class = "dropZone1">
				<form action="/imageLoader.php">
					<div id="dropZone1">

					</div>
				</form>
			</div>
			<div style="background-image: url(<?php echo $full_image2; ?>); background-size: 100%; background-repeat: no-repeat;" class = "dropZone2">
				<form action="/imageLoader.php">
					<div id="dropZone2">

					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
				$(document).ready(function() {
				    var dropZone = $('#dropZone'),
				    	dropZone1= $('#dropZone1'),
				    	dropZone2 = $('#dropZone2'),
				        maxFileSize = 300000; // максимальный размер файла - 300 кб.
				        
				        // анимация эффекта при навелении на поле
						dropZone[0].ondragover = function() {
							dropZone.removeClass();
						    dropZone.addClass('hover');
						    return false;
						};
						dropZone1[0].ondragover = function() {
							dropZone1.removeClass();
						    dropZone1.addClass('hover');
						    return false;
						};
						dropZone2[0].ondragover = function() {
							dropZone2.removeClass();
						    dropZone2.addClass('hover');
						    return false;
						};

						// анимация эффекта ухода с поля
						dropZone[0].ondragleave = function() {
						    dropZone.removeClass('hover');
						    return false;
						};
						dropZone1[0].ondragleave = function() {
						    dropZone1.removeClass('hover');
						    return false;
						};
						dropZone2[0].ondragleave = function() {
						    dropZone2.removeClass('hover');
						    return false;
						};
						// событие при вбросе файла в зону
						dropZone[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в браузере
						    dropZone.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone.text('Файл слишком большой!');
							    dropZone.addClass('error');
							    return false;
							}

							// ajax запрос обработчику
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress, false);
							xhr.onreadystatechange = stateChange; // обработка результата
							xhr.open('POST', '/MVC/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};
						dropZone1[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в брацзере
						    dropZone1.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone1.text('Файл слишком большой!');
							    dropZone1.addClass('error');
							    return false;
							}

							// ajax запрос обработчику 1
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress1, false);
							xhr.onreadystatechange = stateChange1; // обработка результата
							xhr.open('POST', '/MVC/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'-1.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};
						dropZone2[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в брацзере
						    dropZone2.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone2.text('Файл слишком большой!');
							    dropZone2.addClass('error');
							    return false;
							}

							// ajax запрос обработчику 2
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress2, false);
							xhr.onreadystatechange = stateChange2; // обработка результата
							xhr.open('POST', '/MVC/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'-2.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};

						// функция прогресса загрузки
						function uploadProgress(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone.text('Загрузка: ' + percent + '%');
						}
						function uploadProgress1(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone1.text('Загрузка: ' + percent + '%');
						}
						function uploadProgress2(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone2.text('Загрузка: ' + percent + '%');
						}
						// Проверка завершен ли процесс загрузки, и если да, то необходимо проверить не возникла ли какая-либо ошибка
						function stateChange(event) {							
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText = this.responseText;
						            if (resbText == "ERROR") {dropZone.addClass('error'); dropZone.text('Ошибка загрузки файла!');}
						            else {dropZone.addClass('drop'); dropZone.text('Файл загружен!'); $('div.dropZone').css("background-image", 'url(' + resbText + ')');}
						        } else {
						            dropZone.text('Произошла ошибка!');
						            dropZone.addClass('error');
						        }
						    }
						}
						function stateChange1(event) {
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText1 = this.responseText;
						            if (resbText1 == "ERROR") {dropZone1.addClass('error'); dropZone1.text('Ошибка загрузки файла!');}
						            else {dropZone1.addClass('drop'); dropZone1.text('Файл загружен!'); $('div.dropZone1').css("background-image", 'url(' + resbText1 + ')');}
						        } else {
						            dropZone1.text('Произошла ошибка!');
						            dropZone1.addClass('error');
						        }
						    }
						}
						function stateChange2(event) {
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText2 = this.responseText;
						            if (resbText2 == "ERROR") {dropZone2.addClass('error'); dropZone2.text('Ошибка загрузки файла!');}
						            else {dropZone2.addClass('drop'); dropZone2.text('Файл загружен!'); $('div.dropZone2').css("background-image", 'url(' + resbText2 + ')');}
						        } else {
						            dropZone2.text('Произошла ошибка!');
						            dropZone2.addClass('error');
						        }
						    }
						}			        
				});
</script>


<?php

function FileExist($name, $array)
	{
		$is = false;
		foreach ($array as $value) 
			{
				$string = substr($value, 31);
				if ($name == $string) $is = true;
			}
		return $is;
	}

?>