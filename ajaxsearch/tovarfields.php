<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php'; //подключаем файл ядра

if (isset($_POST['BTvalue']))
{
	$array = explode(" ", $_POST['BTvalue']); // разбиваем строку на массив. по разделителю " "

	$BTvalue = $array[0];
	$xarakter = $array[1];
	$grG = $array[2];
	$barCode = $_POST['barCode'];

//------------------------------------------------------ПОЛУЧЕНИЕ СЛЕДУЮЩЕГО pnt----------------------------------------------------------------------------------
	//$auto = mysql_query("SHOW TABLE STATUS LIKE 'pnt'");
	//$ar  = mysql_fetch_array($auto);
	//$ai = $ar['Auto_increment'];

	$nextpnt = GetNextNumber($AccConn, 'pnt' ,'pnt');
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	echo '<form action="/" method="post" id="pntForm">';
	echo '<div style = "color: #015DCB; text-align: left;"><h3>'.GetPNTOName($BTvalue).'</h3><span id="tovarName"></span></div>';

	echo '<div style="margin-bottom: 5px;"><div style="margin-bottom: 5px;"><input name="NaimSite" type="text" size="99" placeholder="Наименование для отображения на сайте"></div>';
	echo '<div><textarea name="opisanie" rows="3" cols="76" placeholder="Описание товара" wrap="hard"></textarea></div></div>';

	echo '<div class ="spec">
	<table class = "tabspec">';
	echo '<tr class ="tabheader"><td colspan="2">ОБЯЗАТЕЛЬНЫЕ ПОЛЯ</td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Группа товара</td><td class ="grG"><input type = "hidden" name="IDgr" value = "'.$grG.'">'.GetGRGName($grG).'</td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Код товара:</td><td class ="grG">'.$nextpnt.'</td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Штрих-код:</td><td><input style="text-align: center;" class="valuefield" value="'.$barCode.'" type="text" id="Skode" name="barCode"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Артикул</td><td><input name="ARTIKUL" class="valuefield" type="text"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Ед. измерения</td><td><input name="ED_IZM" class="valuefield" type="text"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Страна</td><td><input name="STRANA" class="valuefield" type="text"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Бренд</td><td><input name="brend" class="valuefield" type="text"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Фасовка</td><td><input name="fasovka" class="valuefield" type="text"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Вес, кг</td><td><input name="ves" class="valuefield" type="text" id="ves" placeholder="1.000"></td></tr>';
	echo '<tr><td class = "left" style="font-weight: bold;">Габариты, мм</td><td><input name="size" class="valuefield" type="text" id="size" placeholder="Ширина х Высота х Глубина, мм"></td></tr>';
	echo '<tr class ="tabheader"><td colspan="2">СВОЙСТВА:</td></tr>';

	$string = '<tr class ="tabheader"><td style = "padding: 5px;">Название свойства</td><td style = "padding: 5px;">Значение свойства</td></tr>';

		$array = explode("_", $xarakter);
		for ($i = 1; $i < count($array); $i++)
		{
			$string .= '<tr><td class = "left" style="font-weight: bold;"><div>'.GetpntsName($array[$i], $AccConn).'</div></td><td><input name="'.$array[$i].'" class="valuefield" type="text"></td></tr>';
		}
	$string .= '</table></div></div>';
	echo $string;
	echo '<div style="text-align: right; width: 100%;"><input id="submit" type="button" value="ДОБАВИТЬ" class="button button-green"></div></form>';
	echo "<script>$(document).on('click', '#submit', function(){var data = $('#pntForm').serialize();$.ajax({type: 'POST', url: 'ajaxsearch/pnt_handler.php', data: {'data': data},cache: false,success: function(response){".'$("#s55result").html(response);'."}});return false;});</script>";
	echo "<script>function showValues(){var str = $('#pntForm').serialize(); $('#tovarName').text(str);}$(".'"'."input[type='text']".'"'.").on( 'change', showValues );$('select').on('change', showValues );showValues();</script>";
	echo "<script>$('#Skode').keypress(function(key){if ((key.charCode < 48 || key.charCode > 57) && key.charCode != 8) return false;});</script>";
	echo "<script>$('#ves').keypress(function(key){if ((key.charCode < 48 || key.charCode > 57) && key.charCode != 46) return false;});</script>";
	echo "<script>$('#size').keypress(function(key){if ((key.charCode < 48 || key.charCode > 57) && key.charCode != 42) return false;});</script>";
	echo "<script src='js/jquery.maskedinput.min.js'></script>";
	echo '<script>$(function(){$("#size").mask("9999 х 9999 х 9999");});</script>';
}
else
{
	echo 'no data';
}
?>