<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if ($_POST['barCode'] !== '' && $_POST['pnt'] == '') // поиск по полю Штрих-кода
{
  $barCode = $_POST['barCode'];
  $query = "SELECT * FROM pnt WHERE barCode = '$barCode'";
  try {$count = $AccConn->query("SELECT COUNT(*) AS CountRows FROM pnt WHERE barCode LIKE '%".$barCode ."%'");}
  catch (Exception $e) {echo '<div class="width100 bottom5">Ошибка выполнения запроса количества строк: '.$e.'</div>';}
  try {$count = $count->fetchColumn();}
  catch (Exception $e) {echo '<div class="width100 bottom5">Ошибка обработки результата запроса количества строк: '.$e.'</div>';}
  if ($count > 0)
  {
    PntTablePrint($AccConn, $query);
  }
  else { ?> <div class= "width100 bottom5" ><span>По Штрих-коду: <a><?php echo $barCode; ?> </a>, ничего не найдено</span></div> <?php }
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
else if ($_POST['pnt'] !== '' && $_POST['barCode'] == '') // поиск по полю код
{
  $pnt = $_POST['pnt'];
  $query = "SELECT * FROM pnt WHERE pnt = $pnt";
  try {$count = $AccConn->query("SELECT COUNT(*) AS CountRows FROM pnt WHERE pnt LIKE '".$pnt ."'");}
  catch (Exception $e) {echo '<div class="width100 bottom5">Ошибка выполнения запроса количества строк: '.$e.'</div>';}
  try {$count = $count->fetchColumn();}
  catch (Exception $e) {echo '<div class="width100 bottom5">Ошибка обработки результата запроса количества строк: '.$e.'</div>';}
  if ($count > 0)
  {
    PntTablePrint($AccConn, $query);
  }
  else {echo '<div class= "width100 bottom5" ><span>По Коду: <a>'.$pnt.'</a>, ничего не найдено</span></div>';}
}
// -------------------------------------------------------------------------------------------------------------------------------------------------
else if ($_POST['barCode'] !== '' && $_POST['pnt'] !== '') // поиск по полю Штрих-кода и Кода
{
  $barCode = $_POST['barCode'];
  $pnt = $_POST['pnt'];

  $query = "SELECT * FROM pnt WHERE barCode LIKE '".$barCode ."' AND pnt LIKE '".$pnt."'";
  try {$count = $AccConn->query("SELECT COUNT(*) AS CountRows FROM pnt WHERE barCode LIKE '%".$barCode ."%' AND pnt LIKE '".$pnt."'");}
  catch (Exception $e) {echo '<div class="width100 bottom5">Ошибка выполнения запроса количества строк: '.$e.'</div>';}
  try {$count = $count->fetchColumn();}
  catch (Exception $e) { ?> <div class="width100 bottom5">Ошибка обработки результата запроса количества строк: <?php echo $e; ?> </div> <?php } ?>
  <?php if ($count > 0)  {PntTablePrint($AccConn, $query);}
 else {?><div class= "width100 bottom5" ><span>По Штрих-коду: <a><?php echo $barCode; ?></a> и Коду: <a><?php echo $pnt; ?></a>, ничего не найдено</span></div> <?php } }

else { ?> <div class="width100 bottom5">Пустое значение полей</div> <?php } ?>

<?php
function PntTablePrint ($AccConn, $query) //gabarits
{
  $result = $AccConn->prepare($query);
  try {$result->execute();}
  catch (Exception $e) {?><div class="width100 bottom5">Ошибка выполнения запроса выборки: <?php echo $e; ?></div> <?php } ?>

  <div>
    <table id = 'pnt' align = 'center'>
      <tr class = 'tabheader'>
        <td>Код</td>
        <td>Изображение</td>
        <td>Наименование старое</td>
        <td>Наименование новое</td>
        <td>СВОЙСТВА</td>
        <td>BT2</td>
        <td>BT2plus</td>
        <td>Бренд</td>
        <td>Страна</td>
        <td>Ед изм.</td>
        <td>АРТИКУЛ</td>
        <td>Штрих-Код</td>        
        <td>Вес, кг</td>
        <td>Габариты</td>
      </tr>
  <?php
  while ($row = $result->fetch(PDO::FETCH_LAZY))
  { ?>
    <tr>
    <td><?php echo $row['pnt']; ?></td>
    <td><a style = "cursor: pointer;" id = <?php echo '"'.$row['pnt'].'_setimage"'; ?> class = "edit">show</a></td>
    <td style = "text-align: left;"><a style= "cursor: pointer;" class = "edit" id = <?php echo '"'.$row['pnt'].'_OldSv"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $row['NAIM']);?></a></td>
    <?php
    echo "<td style = 'text-align: left;'>".iconv("Windows-1251", "UTF-8", $row['NAIM2'])."</td>";
      if ($row['BT2'] == '' || $row['BT2'] == ' ') // если не присвоен стандарт (новое Базовое наименование)
        {
          echo "<td></td>";
          echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_bt'>add</a></td>";
        }
        else
        {
          echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_sv'>edit</a></td>";
          echo "<td><a style= 'cursor: pointer;' class = 'edit' id = '".$row['pnt']."_bt'>".iconv("Windows-1251", "UTF-8", GetBTname($AccConn, $row['BT2']))." (".$row['BT2'].")"."</a></td>";
        }

        if ($row['BT2plus'] == '')
        {
          $BTpluString = '';
        }
        else
        {
          $BTpluString = getBTplus($AccConn, $row['BT2plus']);
        }
    echo "<td class = '"."edit "."BT2plus ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $BTpluString)."</td>";
    echo "<td>".iconv("Windows-1251", "UTF-8", getBrand($AccConn, $row['brend']))." (".$row['brend'].")"."</td>";
    echo "<td>".iconv("Windows-1251", "UTF-8", $row['STRANA'])."</td>";
    echo "<td style = 'font-weight: 600; color: red;'>".iconv("Windows-1251", "UTF-8", $row['ED_IZM'])."</td>";
    echo "<td class = '"."edit "."ARTIKUL ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $row['ARTIKUL'])."</td>";
    echo "<td class = '"."edit "."barCode ".$row['pnt']." pnt pnt' id = 'barCode'>".$row['barCode']."</td>";
    echo "<td class = '"."edit "."ves ".$row['pnt']." pnt pnt'>".$row['ves']."</td>";
    echo "<td class = '"."edit "."gabarits ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $row['gabarits'])."</td>";
    echo "</tr>";
  }
  echo "</table></div>";
}

?>