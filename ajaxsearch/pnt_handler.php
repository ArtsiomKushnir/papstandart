<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

$array = explode("&", $_POST['data']);
$newarray = [];
foreach($array as $value)
	{
		$x = explode("=", $value);
		$newarray[$x[0]] = $x[1];
	}

if (isset($_POST['data']))
{
	$barCode = ''; // Штрих - код
	$ARTIKUL = ''; // Артикул
	$ED_IZM = ''; // Ед. измерения
	$STRANA = ''; // Страна
	$fasovka = ''; // Фасовка
	$brend = ''; // код Бренд
	$ves = 0.0; // Вес
	$size = ''; // Габариты размер
	$IDgr = 0; // код Группы
	$pnto = ''; // набор свойств и значений
	$NaimSite = ''; // наименование сайт
	$opisanie = ''; // описание товара
												/* по строке PNTO   _БББББ_ССЗЗЗЗЗ_ССЗЗЗЗЗ_ССЗЗЗЗЗ_.........
											                       5    2    5  2    5  2    5
												*/
	echo '</br>';
	foreach($newarray as $key => $value)
	{

		if ($key == 'barCode')
		{
			if ($value !== '')
			{
				$barCode = $value;
			}
			else echo '<span>Пустое значение поля: Штрих - код</span></br>';
		}
		else if ($key == 'ARTIKUL')
		{
			if ($value !== '')
			{
				$ARTIKUL = iconv("UTF-8", "Windows-1251", $value);
				echo '<span>'.$value.' - '.$ARTIKUL.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Артикул</span></br>';
		}
		else if ($key == 'ED_IZM')
		{
			if ($value !== '')
			{
				$ED_IZM = iconv("UTF-8", "Windows-1251", $value);
				echo '<span>'.$value.' - '.$ED_IZM.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Ед. измерения</span></br>';
		}
		else if ($key == 'STRANA')
		{
			if ($value !== '')
			{
				$STRANA = iconv("UTF-8", "Windows-1251", $value);
				echo '<span>'.$value.' - '.$STRANA.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Страна</span></br>';
		}
		else if ($key == 'fasovka')
		{
			if ($value !== '')
			{
				$fasovka = iconv("UTF-8", "Windows-1251", $value);
				echo '<span>'.$value.' - '.$fasovka.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Фасовка</span></br>';
		}
		else if ($key == 'brend')
		{
			if ($value !== '')
			{
				$brend = $value;
			}
			else echo '<span>Пустое значение поля: Бренд</span></br>';
		}
		else if ($key == 'ves')
		{
			if ($value !== '')
			{
				$ves = (double)$value;
				echo '<span>'.$value.' - '.$ves.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Вес</span></br>';
		}
		else if ($key == 'size')
		{
			if ($value !== '')
			{
				$size = iconv("UTF-8", "Windows-1251", $value);
				echo '<span>'.$value.' - '.$size.'</span></br>';
			}
			else echo '<span>Пустое значение поля: Габариты</span></br>';
		}
		else if ($key == 'IDgr')
		{
			if ($value !== '')
			{
				$IDgr = (int)$value;
			}
			else echo '<span>Пустое значение поля: '.$key.'</span></br>';
		}
		else if ($key == 'NaimSite')
		{
			if ($value !== '')
			{
				$NaimSite = $value;
			}
			else echo '<span>Пустое значение поля: <strong>Наименование сайт</strong>,</span><span style = "color: red;"> значение создано автоматически!</span></br>';
		}
		else if ($key == 'opisanie')
		{
			if ($value !== '')
			{
				$opisanie = $value;
			}
			else echo '<span>Пустое значение поля: <strong>Описание товара</strong></span></br>';
		}
		else
		{
			if ($value != '')
			{
				if ($kodpntsz = ifpntszExist($key, $value, $AccConn)) // возвращяет код значения свойства если оно уже присутствует в справочнике pntsz, или false если нет значения
				{
					$pnto .= SetSV($key, $kodpntsz);
				}
				else if($kodpntsz = addpntsz($key, $value, $AccConn)) // создаем новое значение свойства в справочнике pntsz
				{
					$pnto .= SetSV($key, $kodpntsz);
					echo '<span>'.$key.' - '.$value.'</span></br>';
				}
				else echo "<span>Не удалось присвоить значение (".$value."), свойства - ".$key."<span></br>";
			}
			else echo '<span>Пустое значение свойства: '.GetpntsName($key, $AccConn).'</span></br>';
		}
	}

	echo '</br>';

	if ($IDgr != 0 && $barCode != '' && $ARTIKUL != '' && $ED_IZM != '' && $STRANA != '' && $fasovka != '' && $pnto != '')
	{
		if ($nextNumber = GetNextNumber($AccConn, 'pnt' ,'pnt'))
		{
			$values = array($nextNumber, $IDgr, $barCode, $ARTIKUL, $ED_IZM, $STRANA, $pnto, $fasovka);
			$result = $AccConn->prepare("INSERT INTO pnt (pnt, grG, barCode, ARTIKUL, ED_IZM, STRANA, pnto, fasovka) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			if ($result->execute($values))
			{
				echo "<span style = 'color: red;'>Товар добавлен. Код: ".$nextNumber."</span>";
			}
			else
			{
				echo "<span style = 'color: red;'>Ошибка записи в pnt. Код: ".$nextNumber.' + '.$IDgr.' + '.$barCode.' + '.$ARTIKUL.' + '.$ED_IZM.' + '.$STRANA.' + '.$pnto.' + '.$fasovka.'</span>';
			}
		}
		else
		{
			echo "<span style = 'color: red;'>Error pnt_handler_GetNextNumber</span>";
		}
	}
	else
	{
		echo '<span style = "color: red;">Новый товар не создан (не заполнены обязательные поля!)</span></br>';
	}
}
else echo "JS error, no data";
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
function ifpntszExist($kods, $value, $AccConn)
	{
		$result = $AccConn->prepare("SELECT kodz, naim FROM pntsz WHERE kods = $kods");
		if ($result->execute())
		{
			$tr = 0;
			while ($row = $result->fetch(PDO::FETCH_LAZY))
			{
				$naimval = iconv("UTF-8", "Windows-1251", $value);
				//echo $kods.'.'.$row['naim'].' === '.$naimval.'  ';
				if ($row['naim'] === $naimval) // проверка на совпадение наименований
				{
					$tr = $row['kodz'];
				}
			}
			if ($tr == 0)
			{
				return false;
			}
			else
			{
				return $tr;
			}
		}
		else
		{
			AddLog("Ошибка pnt_handler_ifpntszExist");
			return false;
		}
	}

function addpntsz($kods, $naim, $AccConn) // добавляем значение свойства в таблицу pntsz
	{
		if ($nextNumber = GetNextNumberpntsz($kods, $AccConn))
		{
			$naimconv = iconv("UTF-8", "Windows-1251", $naim);
			$qery = "INSERT INTO pntsz (kods, kodz, naim) VALUES (?, ?, ?)";
			$result = $AccConn->prepare($qery);
			if($result->execute(array($kods, $nextNumber, $naimconv)))
			{
				AddLog("Новое значение свойства ".$naim.", добавлено. Код ".$nextNumber.", Код свойства: ".$kods);
				return $nextNumber;
			}
			else
			{
				AddLog("Error pnt_handler_addpntsz, Код ".$nextNumber.", значение свойства ".$naim.", Код свойства: ".$kods);
				return false;
			}
		}
		else return false;
	}

function SetSV($key, $value) // собираем свойства и значение для товара
    {
    	$sv = '00';
    		 if (strlen($key) == '1') {$sv = '0'.$key;}
    	else if (strlen($key) >= '2') {$sv =  $key;}

    	$sval = '00000';
    		 if (strlen($value) == '1') {$sval = '0000'.$value;}
    	else if (strlen($value) == '2') {$sval = '000'.$value;}
    	else if (strlen($value) == '3') {$sval = '00'.$value;}
    	else if (strlen($value) == '4') {$sval = '0'.$value;}
    	else if (strlen($value) == '5') {$sval = $value;}

        return "_".$sv.$sval;
    }

function GetNextNumberpntsz($kods, $AccConn) // возвращает следующий код значения свойства по коду свойства
    {
        $result = $AccConn->prepare("SELECT MAX(kodz) FROM pntsz WHERE kods = $kods");
        if ($result->execute())
        {
        	$name = $result->fetchColumn();
        	return (int)$name + 1;
    	}
    	else
		{
			AddLog("Ошибка pnt_handler_GetNextNumberpntsz");
			return false;
		}
    }
?>