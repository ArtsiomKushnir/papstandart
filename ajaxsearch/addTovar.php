<div class="b-popup" id="popup1">
	<div class="b-popup-content">
		<div style="position: relative;">
			<div style="position: absolute; text-align: right; width: 100%;">
				<a class="button button-red" style="margin: 0px; font-size: 15px; font-weight: bold; padding: 3px 8px 5px 8px;" href="javascript:PopUpHide()">X</a>
			</div>
			<div style="width: 100%; text-align: left;">
				<span style="margin: 0px 0px 10px 3px;">Выберите наименование Базового товара:</span></br>
			    <select id="BTsearch" name = "BTsearch" data-placeholder="Выберите БТ..." class="chosen-select" style="min-width: 40%;">
			        <option value =""></option>
				        <?php
				        	$result = $AccConn->prepare("SELECT kod, naim, xarakter, GrG FROM PNTO2");
							if ($result->execute($values))
							{
								while ($row = $result->fetch(PDO::FETCH_LAZY)) 
								{echo "<option value = '".$row['kod']." ".$row['xarakter']." ".$row['GrG']."'>".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";}
							}
				        ?>
			    </select>
			</div>
		</div>
		<div style="width: 100%; text-align: left;" id="s55result">
			<?php// Сюда загружается результат s55search ?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function()
	{  
		PopUpHide();
	});
	//Функция отображения PopUp
	function PopUpShow(){
		$("#popup1").show();
	}
	//Функция скрытия PopUp
	function PopUpHide(){
		$("#popup1").hide();
	}
</script>