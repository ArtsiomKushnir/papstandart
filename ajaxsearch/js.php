	<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function(){ // двойной клик
			$('.ajax').html($('.ajax input').val());
			//удаляем все классы ajax
			$('.ajax').removeClass('ajax');
			//Нажатой ячейке присваиваем класс ajax
			$(this).addClass('ajax');
			//внутри ячейки создаём input и вставляем текст из ячейки в него
			$(this).html('<form action="ajaxsearch/update.php" method="post" name="form" onsubmit="return false;"><input id="editbox" size="'+ $(this).text().length+'" value="' + $(this).text() + '" type="text"></form>');
			//устанавливаем фокус на созданном элементе
			$('#editbox').focus();
		});

		//определяем нажатие кнопки на клавиатуре
		$(document).on('keydown', 'td.edit', function(event){
		//получаем значение класса и разбиваем на массив
		//в итоге получаем такой массив - arr[0] = edit, arr[1] = наименование столбца, arr[2] = id строки
		arr = $(this).attr('class').split( " " );
		//проверяем какая была нажата клавиша и если была нажата клавиша Enter (код 13)
		   if(event.which == 13)
		   {
				var value = $('.ajax input').val();
				$.ajax({ 
					type: "POST",
					url: "ajaxsearch/update.php",
					data: {"value": value, "key": arr[2], "KeyName": arr[3], "field": arr[1], "table": arr[4]},
					cache: false,
					success: function(data){
						 $('.ajax').html($('.ajax input').val());
						 $('.ajax').removeClass('ajax');
				 	}
				});
				return false;
		 	}
		});
	</script>

	<script type="text/javascript">
		$(function(){
			$("#barCode").on('keydown', function(event){

				if(event.which == 13)
				{
				  	$("#resSearch").html('<div style = "color: red;"><strong>Выполняется запрос, подождите </strong><img src="ajax_clock_small.gif" width="16" height="16" alt="ajax clock"><div>');

				    var barCode = $("#barCode").val();
				    var pnt = $("#pnt").val();
				    $.ajax({
				       type: "POST",
				       url: "ajaxsearch/search.php",
				       data: {"barCode": barCode, "pnt": pnt},
				       cache: false,
				       success: function(response)
				       {
				          $("#resSearch").html(response);
				       }
				    });
				    return false;
				}
		   	});
		});
		$(function(){
			$("#pnt").keyup(function(){

			  	$("#resSearch").html('<div style = "color: red;"><strong>Выполняется запрос, подождите </strong><img src="ajax_clock_small.gif" width="16" height="16" alt="ajax clock"><div>');
			    var pnt = $("#pnt").val();
			    var barCode = $("#barCode").val();
			    $.ajax({
			       type: "POST",
			       url: "ajaxsearch/search.php",
			       data: {"pnt": pnt, "barCode": barCode},
			       cache: false,
			       success: function(response)
			       {
			          $("#resSearch").html(response);
			       }
			    });
			    return false;
			});
		});
	</script>