<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

$arr = explode("_", $_POST['id']);
$id = $arr[0];
$key = $arr[1];

?>

<div class="addBT">
	<div style="z-index: 100; padding: 20px;">
<?php // -------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<?php if ($key == 'bt') { ?>
		<h3><?php echo 'Добавить БТ к коду: '.$id ?></h3>
		<div style="width: 100%;">
			<form action="spravochnik.php" method="post">
					<div style="width: 100%; display: inline-block; text-align: left;">
						<select name ='BT' data-placeholder="Выберите БТ..." class="chosen-select" style="width:100%;">
							<option value=""></option>
							<?php
								$query = "SELECT kod, naim, GrG FROM BT ORDER BY GrG";
								$result = $AccConn->prepare($query);
								if ($result->execute())
								{
									$stek = 0;
									while ($row = $result->fetch(PDO::FETCH_LAZY))
									{
										$GrG = $row['GrG'];
										if ($stek !== $GrG) 
										{
											if ($stek != 0) 
											{
												echo "</optgroup>";
											}
											if ($GRGName = GetGRGName($AccConn, $GrG))
											{
												echo "<optgroup label = '".iconv("Windows-1251", "UTF-8", $GRGName)."'>";
											}
											else echo "<optgroup label = 'Error_GetGRGName'>";
										}
										echo "<option value = '".$row['kod']."_".$id."'>".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
										$stek = $GrG;
									}
									echo "</optgroup>";
								}
								else echo "<option>Error</option>";
							?>
						</select>
					</div>
				<div style="width: 100%; display: inline-block; text-align: center;">
					<input class="button" type="submit" value="Добавить">					
					<a class="button_cancel">Закрыть</a>
				</div>
			</form>
			<script type="text/javascript">
		    	var config = {
		      	'.chosen-select'           : {},
		      	'.chosen-select-deselect'  : {allow_single_deselect:true},
		      	'.chosen-select-no-single' : {disable_search_threshold:10},
		     	'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
		      	'.chosen-select-width'     : {width:"95%"}
		    	}
			    for (var selector in config) {$(selector).chosen(config[selector]);}
			</script>
		</div>

<?php // -------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<?php } else if ($key == 'sv'){?>

			<h3><?php echo 'Редактор свойств и значений к коду: '.$id; ?></h3>

			<style>
    			#sortable{list-style-type:none; margin:0; padding:0;}
    			#sortable li{width: 96%; display: inline-block; margin:1px; padding:1px;}
    			div.sortSV {cursor: default; position: relative; display: inline-block; margin:1px; padding:3px; border:1px solid #888; border-radius: 3px; background-color: #eee; background-image: linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%); box-shadow: 0 0 2px #fff inset,0 1px 0 rgba(0,0,0,.05);}
  			</style>

			<div align="left" style="width: 100%; position: relative;">
				<div style="width: 100%; display: inline-block;">
					<?php 	
						$result = $AccConn->prepare("SELECT * FROM pnt WHERE pnt = $id");
						if($result->execute())
						{
							?> <table style="width: 100%;">
									<tr class = "tabheader">
										<td>Код</td>
										<td>Наименование</td>
										<td>АРТИКУЛ</td>
										<td>Бренд</td>
										<td>Страна</td>
										<td>Ед изм.</td>
										<td>Штрих-код</td>
										<td>Вес, кг</td>
										<td>Габариты</td></tr>
							<?php
							while ($row = $result->fetch(PDO::FETCH_LAZY))
							{
								echo "<tr><td>".$row['pnt']."</td>								
									<td>".iconv("Windows-1251", "UTF-8", $row['NAIM'])."</td>
									<td>".iconv("Windows-1251", "UTF-8", $row['ARTIKUL'])."</td>
									<td>".iconv("Windows-1251", "UTF-8", getBrand($AccConn, $row['brend']))." (".$row['brend'].")"."</td>
									<td>".iconv("Windows-1251", "UTF-8", $row['STRANA'])."</td>
									<td style = 'font-weight: 600; color: red;'>".iconv("Windows-1251", "UTF-8", $row['ED_IZM'])."</td>
									<td class = '"."edit "."barCode ".$row['pnt']." pnt pnt' id = 'barCode'>".$row['barCode']."</td>
									<td class = '"."edit "."ves ".$row['pnt']." pnt pnt'>".$row['ves']."</td>
									<td class = '"."edit "."gabarits ".$row['pnt']." pnt pnt'>".iconv("Windows-1251", "UTF-8", $row['gabarits'])."</td></tr>";
							}
							echo "</table>";
						}
					?>
				</div>
				<div style="width: 100%; display: inline-block;">
					<?php
						$result = $AccConn->prepare("SELECT NaimSite FROM pntlongnaim WHERE pnt = $id");
						if($result->execute())
						{
							$NaimSite = $result->fetchColumn();
							$NaimSite = iconv("Windows-1251", "UTF-8", $NaimSite);
						}

						$result = $AccConn->prepare("SELECT opisanie FROM pntlongnaim WHERE pnt = $id");
						if($result->execute())
						{
							$opisanie = $result->fetchColumn();
							$opisanie = iconv("Windows-1251", "UTF-8", $opisanie);
						}

					?>
					<p><strong>Наименование для сайта: </strong><input form="PNT2" type="text" name="NaimSite" value=<?php echo '"'.htmlspecialchars($NaimSite, ENT_NOQUOTES).'"'; ?> placeholder = "Обязательно заполните" style="width: 99.5%;"></p>
					<p><strong>Описание товара: </strong><textarea rows="6" cols="145" form="PNT2" name="opisanie" style="resize: none;"><?php echo $opisanie; ?></textarea></p>
					<form action="spravochnik.php" method="post" id="PNT2">
					<input type="hidden" name="PNTO2" value = <?php echo '"'.$id.'"'; ?>>
							<?php
								$query = "SELECT PNTO2, BT2 FROM pnt WHERE pnt = $id";
								$result = $AccConn->prepare($query);
								if ($result->execute())
								{
									while ($row = $result->fetch(PDO::FETCH_LAZY))
									{
							?>
							<!--<div style="width: 100%; display: inline-block;">
								<p align = "center">Шаблон свойств:</p>
								<?php// printXarakter($AccConn, $row['BT2']); ?>
							</div>-->
							<?php

										if ($row['PNTO2'] == '')
										{ ?>
							<span><strong>Свойства не заполнены, заполнить по шаблону:</strong>
								<div onmouseover="$(this).children('div').css('display', 'block');" onmouseout="$(this).children('div').css('display', 'none');" class="informer">
									<?php include 'gost.php'; ?>
								</div>
							</span>
							<div style='width: 100%; display: inline-block; border: 1px solid #c2c2c2; padding: 3px;  max-height: 270px; overflow: scroll; position: relative;'>
								<ul id = 'sortable'>
									<?php printXarakterToAdd($AccConn, $row['BT2']); ?>
								</ul>
							</div>										
									<?php }

										else
										{ ?>
							<span><strong>Редактор значений свойств: </strong>
								<div onmouseover="$(this).children('div').css('display', 'block');" onmouseout="$(this).children('div').css('display', 'none');" class="informer">
									<?php include 'gost.php'; ?>
								</div>
							</span>
							<div style='width: 100%; display: inline-block; border: 1px solid #c2c2c2; padding: 3px;  max-height: 270px; overflow: scroll; position: relative;'>
								<ul id = 'sortable'>
									<?php printPNTONameAndValue($AccConn, $row['PNTO2']); ?>
								</ul>
							</div>
							<?php									
										}
									}
								}
							?>
						<script>
	    					$( "#sortable" ).sortable().disableSelection();
	  					</script>
						<div style="width: 100%; display: inline-block; text-align: center;">
							<input class="button" type="submit" value="Добавить">
							<a class="button_cancel">Закрыть</a>
						</div>					
				</form>





				</div>
			</div>
			<script>
				var LIhtml = $('#LI_add_SV').html();
			</script>
			<script>
				$(document).on('click', 'a.close_ui', function(){
					$(this).parents('li').detach();
				})
			</script>
<?php // -------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<?php } else if ($key == 'OldSv'){?>
		<h3><?php echo 'Редактор свойств и значений к коду: '.$id; ?></h3>

			<style>
    			#sortable{list-style-type:none; margin:0; padding:0; width:50%;}
    			#sortable li{width: 96%; display: inline-block; margin:1px; padding:1px;}
    			div.sortSV {cursor: default; position: relative; display: inline-block; margin:1px; padding:3px; border:1px solid #888; border-radius: 3px; background-color: #eee; background-image: linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%); box-shadow: 0 0 2px #fff inset,0 1px 0 rgba(0,0,0,.05);}
  			</style>

		<div align="left" style="width: 100%; position: relative;">
			<div style="width: 100%; display: inline-block;">
				<?php 	
					$result = $AccConn->prepare("SELECT * FROM pnt WHERE pnt = $id");
					if($result->execute())
					{
						?> <table style="width: 100%;">
								<tr class = "tabheader">
									<td>Код</td>
									<td>Штрих-код</td>
									<td>Наименование</td>
									<td>Артикул</td>
									<td>Бренд</td>
									<td>Страна</td></tr>
						<?php
						while ($row = $result->fetch(PDO::FETCH_LAZY))
						{
							echo "<tr><td>".$row['pnt']."</td>
								<td>".$row['barCode']."</td>
								<td>".iconv("Windows-1251", "UTF-8", $row['NAIM'])."</td>
								<td>".iconv("Windows-1251", "UTF-8", $row['ARTIKUL'])."</td>
								<td>".$row['brend']."</td>
								<td>".iconv("Windows-1251", "UTF-8", $row['STRANA'])."</td></tr>";
						}
						echo "</table>";
					}
				?>
			</div>
			<form action="spravochnik.php" method="post" id="oldPnt">
			<input type="hidden" name="oldPnt" value = <?php echo '"'.$id.'"'; ?>>
				<div style="width: 100%; display: inline-block; padding: 3px;">
					<?php
							$result = $AccConn->prepare("SELECT NaimSite FROM pntlongnaim WHERE pnt = $id");
							if($result->execute())
							{
								$NaimSite = $result->fetchColumn();
								$NaimSite = iconv("Windows-1251", "UTF-8", $NaimSite);
							}

							$result = $AccConn->prepare("SELECT opisanie FROM pntlongnaim WHERE pnt = $id");
							if($result->execute())
							{
								$opisanie = $result->fetchColumn();
								$opisanie = iconv("Windows-1251", "UTF-8", $opisanie);
							}

						?>
					<p>
						<strong>Наименование для сайта: </strong>
						<input type="text" name="NaimSite" value=<?php echo '"'.htmlspecialchars($NaimSite, ENT_NOQUOTES).'"'; ?> placeholder = "Обязательно заполните" style="width: 99.5%;">
					</p>
					<p>
						<strong>Описание товара: </strong>
						<textarea rows="6" cols="145" name="opisanie" style="resize: none;"><?php echo $opisanie; ?></textarea>
					</p>
				</div>
			</form>


			<div style="width: 100%; display: inline-block; padding: 3px; max-height: 270px; overflow: scroll;">
				<ul id = "sortable">
					<?php  printPNTONameAndValueOld($AccConn, getpnto($AccConn, $id)); ?>
				</ul>	
				<script>
	    			$( "#sortable" ).sortable().disableSelection();
	  			</script>
			</div>
			<div style="width: 100%; display: inline-block; text-align: center;">
				<input class="button" type="submit" value="Подтвердить" form="oldPnt">
				<a class="button_cancel">Закрыть</a>
			</div>	
		</div>
		<script>
			var LIhtml = $('#LI_add_SV').html();
		</script>
		<script> // удаляем свойство
			$(document).on('click', 'a.close_ui', function(){
				$(this).parents('li').remove(); // удаляем родительский li
			})
		</script>
<?php // -------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<?php } else if ($key == 'std'){ ?>
			<h3><?php echo 'Редактор свойств к стандарту: '.$id.' ('.iconv("Windows-1251", "UTF-8", GetBTname($AccConn, $id)).')'; ?></h3>

			<style>
    			#sortable{list-style-type:none; margin:0; padding:0; width:100%;}
    			#sortable li{width: 96%; display: inline-block; margin:1px; padding:1px;}
    			div.sortSV {cursor: default; position: relative; display: inline-block; margin:1px; padding:3px; border:1px solid #888; border-radius: 3px; background-color: #eee; background-image: linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%); box-shadow: 0 0 2px #fff inset,0 1px 0 rgba(0,0,0,.05);}
  			</style>

			<form action="index.php" method="post">
	  			<div align="left" style="position: relative;">
				<input type="hidden" name="xarakter" value = <?php echo '"'.$id.'"'; ?>>
					<div style="display: inline-block;">
						<div style="width: 100%; display: inline-block; padding: 3px; padding: 3px;">
							<ul id = "sortable">
								<?php
								if ($xarakter = GetBTxarakter($AccConn, $id))
								{
									GetArraypntsNameUI($xarakter, $AccConn);
								}
								else {echo '<li><div class = "sortSV">Error</div></li>';}
								?>
							</ul>
							<script>
								$("#sortable").sortable().disableSelection();
							</script>
						</div>
					</div>
				</div>


				<?php
					$query = "SELECT GrG, descr FROM BT WHERE kod = $id";
					if($result = $AccConn->query($query))
					{
						while ($row = $result->fetch(PDO::FETCH_LAZY))
						{
							$OldGrg = $row['GrG'];
							$Descr = $row['descr'];
						}
					}
				?>


				<div style="width: 100%; display: inline-block; text-align: left;">
					<table style="width: 100%; text-align: left;">
						<tr>
							<td style="text-align: left;">Описание стандарта:</td>
							<td style="text-align: left;" class = <?php echo '"'."edit "."descr ".$id." kod BT".'"'; ?>><?php echo iconv("Windows-1251", "UTF-8", $Descr); ?></td>
						</tr>
						<tr>
							<td style="text-align: left;">Группа стандарта:</td>
							<td style="text-align: left;">
								<select name = 'gr' data-placeholder="Выберите группу..." class="chosen-select" style="width: 100%;">
									<option value =""></option>
										<?php
											$query = "SELECT grG.kod, grG.naim AS grGnaim, grG.grk, grK.naim AS grKnaim
											FROM grG
											INNER JOIN grK
											ON grG.grk = grK.kod 
											ORDER BY grK.posl";

											if($result = $AccConn->query($query))
											{
												$stek = 0;
												while ($row = $result->fetch(PDO::FETCH_LAZY))
												{
													$GrK = $row['grk'];
													if ($stek !== $GrK)
													{
														if ($stek != 0)
														{
															echo "</optgroup>";
														}												
															echo "<optgroup label = '".iconv("Windows-1251", "UTF-8", $row['grKnaim'])."'>";
													}

													if (isset($OldGrg))
													{
														if ($OldGrg == $row['kod'])
														{
															echo "<option value = ".$row['kod']." selected>".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
														}
														else
														{
															echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
														}
													}
													else
													{
														echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['grGnaim'])."</option>";
													}
													$stek = $GrK;
												}
												echo "</optgroup>";
											}
											else echo "<option>Error</option>";
										?>
								</select>
					<script type="text/javascript">
				    	var config = {
				      	'.chosen-select'           : {},
				      	'.chosen-select-deselect'  : {allow_single_deselect:true},
				      	'.chosen-select-no-single' : {disable_search_threshold:10},
				     	'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
				      	'.chosen-select-width'     : {width:"95%"}
				    	}
					    for (var selector in config) {$(selector).chosen(config[selector]);}
					</script>
							</td>
						</tr>
					</table>
				</div>
				<div style="width: 100%; display: inline-block; text-align: center;">
					<input class="button" type="submit" value="Добавить">
					<a class="button_cancel">Закрыть</a>
				</div>
			</form>
			<script>
				var LIhtml = $('#LI_add_SV').html();
			</script>
			<script> // удаляем свойство
				$(document).on('click', 'a.close_li', function(){
					$(this).parents('li').remove(); // удаляем родительский li
				})

				$(document).on('dblclick', 'td.edit', function(){
					$('.ajax').html($('.ajax input').val());
					$('.ajax').removeClass('ajax');
					$(this).addClass('ajax');
					var innertext = $(this).text();
					$(this).html('<form action="ajaxsearch/update.php" method="post" name="form" onsubmit="return false;"><input id="editbox" name="descr" value="' + innertext + '" type="text" style = "width: 100%;"></form>');
					$('#editbox').focus();
				});

				$(document).on('keydown', 'td.edit', function(){
				arr = $(this).attr('class').split( " " );
				   if(event.which == 13)
				   {
						var value = $('.ajax input').val();
						$.ajax({ 
							type: "POST",
							url: "ajaxsearch/update.php",
							data: {"value": value, "key": arr[2], "KeyName": arr[3], "field": arr[1], "table": arr[4]},
							cache: false,
							success: function(data){
								 $('.ajax').html($('.ajax input').val());
								 $('.ajax').removeClass('ajax');
						 	}
						});
						return false;
				 	}
				});
			</script>
<?php // -------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<?php } else if ($key == 'setimage') { ?>
			<h3>Загрузка изображений к коду: <?php echo $id; ?></h3>
			<p>Размер изображения ограничен: <strong>300 Kb</strong><br>Только файлы с расширением <strong style="color: red;">.jpg</strong><br>Перетащите файл изображения на соответствующее поле</p>
  			<div style="display: inline-block; text-align: center;">
  				<?php
  					include $_SERVER['DOCUMENT_ROOT'].'/application/config.php';

					$ftp_conn = ftp_connect($ftp_server) or die ('Не удалось установить соединение с: '.$ftp_server);
					$login_result = ftp_login($ftp_conn, $ftp_user_name, $ftp_user_pass) or die ('Не удалось авторизироваться: '.$ftp_conn);
					$buff = ftp_nlist($ftp_conn, '/htdocs/images/catalog/');
					ftp_close($ftp_conn);

  					$icode = '00000000';
					$len = strlen($id);
					if ($len == '1') {$icode = '0000000'.$id;}
					else if ($len == '2') {$icode = '000000'.$id;}
					else if ($len == '3') {$icode = '00000'.$id;}
					else if ($len == '4') {$icode = '0000'.$id;}
					else if ($len == '5') {$icode = '000'.$id;}
					else if ($len == '6') {$icode = '00'.$id;}
					else if ($len == '7') {$icode = '0'.$id;}
					else if ($len == '8') {$icode =  $id;}

					$full_image = $icode.'.jpg';
					$full_image1 = $icode.'-1.jpg';
					$full_image2 = $icode.'-2.jpg';

					if (FileExist($full_image, $buff)){$full_image = $URLimagefolder.$icode.'.jpg';}else{$full_image = $URLimagefolder.'noimage.jpg';}
					if (FileExist($full_image1, $buff)){$full_image1 = $URLimagefolder.$icode.'-1.jpg';}else{$full_image1 = $URLimagefolder.'noimage.jpg';}
					if (FileExist($full_image2, $buff)){$full_image2 = $URLimagefolder.$icode.'-2.jpg';}else{$full_image2 = $URLimagefolder.'noimage.jpg';}
  				?>
	  			<div style="background-image: url(<?php echo $full_image;?>); background-size: 100%; background-repeat: no-repeat" class = "dropZone">
	  				<form action="/imageLoader.php">
			  			<div id="dropZone">

			  			</div>
		  			</form>
		  		</div>
  				<div style="background-image: url(<?php echo $full_image1;?>); background-size: 100%; background-repeat: no-repeat" class = "dropZone1">
		  			<form action="/imageLoader.php">
			  			<div id="dropZone1">

			  			</div>
		  			</form>
		  		</div>
				<div style="background-image: url(<?php echo $full_image2;?>); background-size: 100%; background-repeat: no-repeat" class = "dropZone2">
		  			<form action="/imageLoader.php">
			  			<div id="dropZone2">

			  			</div>
		  			</form>
		  		</div>
  			</div>
  			<div style="width: 100%; display: inline-block; text-align: center;">
  				<a class="button_cancel">OK</a>
			</div>
			<script type="text/javascript">
				$(document).ready(function() {
				    var dropZone = $('#dropZone'),
				    	dropZone1= $('#dropZone1'),
				    	dropZone2 = $('#dropZone2'),
				        maxFileSize = 300000; // максимальный размер файла - 300 кб.
				        
				        // анимация эффекта при навелении на поле
						dropZone[0].ondragover = function() {
							dropZone.removeClass();
						    dropZone.addClass('hover');
						    return false;
						};
						dropZone1[0].ondragover = function() {
							dropZone1.removeClass();
						    dropZone1.addClass('hover');
						    return false;
						};
						dropZone2[0].ondragover = function() {
							dropZone2.removeClass();
						    dropZone2.addClass('hover');
						    return false;
						};

						// анимация эффекта ухода с поля
						dropZone[0].ondragleave = function() {
						    dropZone.removeClass('hover');
						    return false;
						};
						dropZone1[0].ondragleave = function() {
						    dropZone1.removeClass('hover');
						    return false;
						};
						dropZone2[0].ondragleave = function() {
						    dropZone2.removeClass('hover');
						    return false;
						};
						// событие при вбросе файла в зону
						dropZone[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в браузере
						    dropZone.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone.text('Файл слишком большой!');
							    dropZone.addClass('error');
							    return false;
							}

							// ajax запрос обработчику
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress, false);
							xhr.onreadystatechange = stateChange; // обработка результата
							xhr.open('POST', '/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};
						dropZone1[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в брацзере
						    dropZone1.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone1.text('Файл слишком большой!');
							    dropZone1.addClass('error');
							    return false;
							}

							// ajax запрос обработчику 1
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress1, false);
							xhr.onreadystatechange = stateChange1; // обработка результата
							xhr.open('POST', '/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'-1.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};
						dropZone2[0].ondrop = function(event) {
						    event.preventDefault(); // отмена открытия заброшенного файла в брацзере
						    dropZone2.removeClass('hover');
						    // проверка размера файла
						    var file = event.dataTransfer.files[0];
							if (file.size > maxFileSize) {
							    dropZone2.text('Файл слишком большой!');
							    dropZone2.addClass('error');
							    return false;
							}

							// ajax запрос обработчику 2
							var xhr = new XMLHttpRequest();
							xhr.upload.addEventListener('progress', uploadProgress2, false);
							xhr.onreadystatechange = stateChange2; // обработка результата
							xhr.open('POST', '/imageLoader.php'); // открываем соединение
							xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded"); // заголовок запроса
							xhr.setRequestHeader('Cache-Control', 'no-cache');
					        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					        xhr.setRequestHeader("X-File-Name", <?php echo '"'.$icode.'-2.jpg'.'"';?>);
					        xhr.setRequestHeader("X-File-Size", file.size);
							xhr.send(file); // отправляем данные
						};

						// функция прогресса загрузки
						function uploadProgress(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone.text('Загрузка: ' + percent + '%');
						}
						function uploadProgress1(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone1.text('Загрузка: ' + percent + '%');
						}
						function uploadProgress2(event) {
						    var percent = parseInt(event.loaded / event.total * 100);
						    dropZone2.text('Загрузка: ' + percent + '%');
						}
						// Проверка завершен ли процесс загрузки, и если да, то необходимо проверить не возникла ли какая-либо ошибка
						function stateChange(event) {							
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText = this.responseText;
						            if (resbText == "ERROR") {dropZone.addClass('error'); dropZone.text('Ошибка загрузки файла!');}
						            else {dropZone.addClass('drop'); dropZone.text('Файл загружен!'); $('div.dropZone').css("background-image", 'url(' + resbText + ')');}
						        } else {
						            dropZone.text('Произошла ошибка!');
						            dropZone.addClass('error');
						        }
						    }
						}
						function stateChange1(event) {
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText1 = this.responseText;
						            if (resbText1 == "ERROR") {dropZone1.addClass('error'); dropZone1.text('Ошибка загрузки файла!');}
						            else {dropZone1.addClass('drop'); dropZone1.text('Файл загружен!'); $('div.dropZone1').css("background-image", 'url(' + resbText1 + ')');}
						        } else {
						            dropZone1.text('Произошла ошибка!');
						            dropZone1.addClass('error');
						        }
						    }
						}
						function stateChange2(event) {
						    if (event.target.readyState == 4) {
						        if (event.target.status == 200) {
						        	var resbText2 = this.responseText;
						            if (resbText2 == "ERROR") {dropZone2.addClass('error'); dropZone2.text('Ошибка загрузки файла!');}
						            else {dropZone2.addClass('drop'); dropZone2.text('Файл загружен!'); $('div.dropZone2').css("background-image", 'url(' + resbText2 + ')');}
						        } else {
						            dropZone2.text('Произошла ошибка!');
						            dropZone2.addClass('error');
						        }
						    }
						}			        
				});
			</script>
		<?php } else if ($key == 'grk') { ?>
		<?php 
			$result = $AccConn->query("SELECT naim FROM grK WHERE kod = $id");
			$naimgrk = iconv("Windows-1251", "UTF-8", $result->fetchColumn());
		?>
			<h3>Описание к группе: <?php echo $id." - ".$naimgrk; ?></h3>
			<?php
					if($result = $AccConn->query("SELECT opisanie FROM grK WHERE kod = $id")) {
						$opisanie = iconv("Windows-1251", "UTF-8", $result->fetchColumn());
				?>
				<form action="sortgroup.php" method="POST">
					<div style="width: 100%; display: inline-block; text-align: center;">

						<input type="text" name="naim_grK" value = <?php echo '"'.$naimgrk.'"'; ?> style = "margin-bottom: 5px; width: 99%;">
						<textarea rows="6" cols="145" name="opisanie_grK" style="resize: none;"><?php echo $opisanie; ?></textarea>
						<input type="hidden" name="id_grK" value = <?php echo '"'.$id.'"'; ?>>

					</div>
					<div style="width: 100%; display: inline-block; text-align: center;">
						<input class="button" type="submit" value="Обновить">
			  			<a class="button_cancel">Закрыть</a>
					</div>
				</form>
			<?php } else { echo 'ERR'; ?>
				<div style="width: 100%; display: inline-block; text-align: center;">
	  				<a class="button_cancel">Закрыть</a>
				</div>
			<?php } ?>
		<?php } else if ($key == 'grg') { ?>
		<?php 
			$result = $AccConn->query("SELECT naim FROM grG WHERE kod = $id");
			$naimgrg = iconv("Windows-1251", "UTF-8", $result->fetchColumn());
		?>
			<h3>Описание к группе: <?php echo $id." - ".$naimgrg; ?></h3>
			<?php
					if($result = $AccConn->query("SELECT opisanie FROM grG WHERE kod = $id")) {
						$opisanie = iconv("Windows-1251", "UTF-8", $result->fetchColumn());
				?>
				<form action="sortgroup.php" method="POST">
					<div style="width: 100%; display: inline-block; text-align: center;">

						<input type="text" name="naim_grG" value = <?php echo '"'.$naimgrg.'"'; ?> style = "margin-bottom: 5px; width: 99%;">
						<textarea rows="6" cols="145" name="opisanie_grG" style="resize: none;"><?php echo $opisanie; ?></textarea>
						<input type="hidden" name="id_grG" value = <?php echo '"'.$id.'"'; ?>>

					</div>
					<div style="width: 100%; display: inline-block; text-align: center;">
						<input class="button" type="submit" value="Обновить">
			  			<a class="button_cancel">Закрыть</a>
					</div>
				</form>
			<?php } else { echo 'ERR'; ?>
				<div style="width: 100%; display: inline-block; text-align: center;">
	  				<a class="button_cancel">Закрыть</a>
				</div>
			<?php } ?>
			<?php } else if ($key == 'gr') { ?>
		<?php 
			if($result = $mysqli->query("SELECT BTandBTplus FROM GR_BT_BTplus WHERE kod = $id"))
			{
				$n = $result->fetch_row();
				$naimgr = $n[0];
			} else $naimgr = "ERR SQL";
		?>
			<h3>Описание к группе: <?php echo $id." - ".$naimgr; ?></h3>
			<?php
					if($result = $mysqli->query("SELECT opisanie FROM GR_BT_BTplus WHERE kod = $id")) {
						$n = $result->fetch_row();
						$opisanie = $n[0];
				?>
				<form action="sortgroup.php" method="POST">
					<div style="width: 100%; display: inline-block; text-align: center;">
						
						<textarea rows="6" cols="145" name="opisanie_gr" style="resize: none;"><?php echo $opisanie; ?></textarea>
						<input type="hidden" name="id_gr" value = <?php echo '"'.$id.'"'; ?>>
				
					</div>
					<div style="width: 100%; display: inline-block; text-align: center;">
						<input class="button" type="submit" value="Обновить">
			  			<a class="button_cancel">Закрыть</a>
					</div>
				</form>
			<?php } else { echo 'ERR'; ?>
				<div style="width: 100%; display: inline-block; text-align: center;">
	  				<a class="button_cancel">Закрыть</a>
				</div>
			<?php } ?>
		<?php } else { ?>
			<h3>Ошибка в параметрах, обратитесь к администратору</h3>
			<div style="width: 100%; display: inline-block; text-align: center;">
  				<a class="button_cancel">OK</a>
			</div>
		<?php } ?>
	</div>
</div>

<?php //------------------------------------------------------------------------------------------------------------------------------------------

function FileExist($name, $array)
	{
		$is = false;
		foreach ($array as $value) 
			{
				$string = substr($value, 23);
				if ($name == $string) $is = true;
			}
		return $is;
	}

function GetArraypntsNameUI($IDarray, $AccConn) // получаем массив имен свойств из таблицы PNTS2 по массиву идентификаторов
    {
        $array = explode("_", $IDarray);
        for ($i = 1; $i < count($array); $i++)
        {
            if ($name = GetpntsName($array[$i], $AccConn)) 
           	{ ?>
<li><div class = "sortSV"><input name = <?php echo '"'.$array[$i].'"'; ?> type = "hidden"><span style = "cursor: default; margin-right: 16px;"><?php echo iconv("Windows-1251", "UTF-8", $name); ?></span><a class = "close_li"></a></div></li>
      <?php }
        }
        // кнопка добавить свойство ?>
	<li id = "LI_add_SV">
		<a class = "button" id = "add_SV" style = "cursor: pointer;">+</a>
		<div style = "display:none;">
			<div class = "sortSV">
				<select name = "chose_svs" id = "chose_svs">
					<option value = "0"></option>
					<?php 
						$query = "SELECT * FROM PNTS2";
						$result = $AccConn->query($query);
						while ($row = $result->fetch(PDO::FETCH_LAZY))
						{
							echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
						}
					?>
				</select>
				<div>
					<a class = "button" id = "ok_SV" style = "cursor: pointer;">Add</a>
					<a class = "button" id = "cancel_SV" style = "cursor: pointer;">Cancel</a>
				</div>
			</div>
		</div>
	</li>

   <?php }

function getpnto($AccConn, $pnt)
    {
        $stmt = $AccConn->prepare("SELECT pnto FROM pnt WHERE pnt = $pnt");
        if ($stmt->execute())
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }

function printXarakter($AccConn, $BT)
	{
		$stmt = $AccConn->prepare("SELECT xarakter FROM BT WHERE kod = $BT");
        if ($stmt->execute()) 
        {
			$xarakter = $stmt->fetchColumn();
			$arrSV = explode("_", $xarakter);
	        for ($i=1; $i < count($arrSV); $i++)
			{ 
				$SV = $arrSV[$i]; // код свойства
				$SVal = GetpntsName($SV, $AccConn);
				echo "<div class='sv'><span style = 'cursor: default;'>".iconv("Windows-1251", "UTF-8", $SVal)." </span></div>";
			}
		}
		else echo 'error';
	}


function printPNTONameAndValue($AccConn, $idString)
{
	$arrSV = explode("_", $idString);
	for ($i=1; $i < count($arrSV); $i++)
	{ 
		$SV = (int)substr($arrSV[$i], 0, -5); // код свойства
		$SVname = GetpntsName($SV, $AccConn);
		$SVnameConv = iconv("Windows-1251", "UTF-8", $SVname);

		$SValue = (int)substr($arrSV[$i], -5); // код значения свойства
		$SValueName = GetpntsValue($SValue, $AccConn);
		$SValueNameConv = iconv("Windows-1251", "UTF-8", $SValueName);

		if($SVnameConv !== "Единица измерения" && $SVnameConv !== "Страна")
		{
			echo "<li><div class = 'sortSV'><span style = 'cursor: default; float: left; margin: 3px 5px 0px 0px;'>".$SVnameConv." </span><input style='margin-right: 15px;' type='text' name = '".$SV."' value = '".htmlspecialchars($SValueNameConv, ENT_NOQUOTES)."'><a class='close_ui'></a></div></li>";
		}
	}
// кнопка добавить свойство ?>
	<li id = "LI_add_SV">
		<a class = "button" id = "add_SV" style = "cursor: pointer;">+</a>
		<div style = "display:none;">
			<div class = "sortSV">
				<select name = "chose_svs" id = "chose_svs">
					<option value = "0"></option>
					<?php 
						$query = "SELECT * FROM PNTS2";
						$result = $AccConn->query($query);
						while ($row = $result->fetch(PDO::FETCH_LAZY))
						{
							echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
						}
					?>
				</select>
				<div>
					<a class = "button" id = "ok_SV" style = "cursor: pointer;">Add</a>
					<a class = "button" id = "cancel_SV" style = "cursor: pointer;">Cancel</a>
				</div>
			</div>
		</div>
	</li>
<?php }

function printXarakterToAdd($AccConn, $BT)
	{
		$stmt = $AccConn->prepare("SELECT xarakter FROM BT WHERE kod = $BT");
        if ($stmt->execute()) 
        {
			$xarakter = $stmt->fetchColumn();
			$arrSV = explode("_", $xarakter);
	        for ($i=1; $i < count($arrSV); $i++)
			{ 
				$SV = $arrSV[$i]; // код свойства
				$SVal = GetpntsName($SV, $AccConn);
				$placeholder = iconv("Windows-1251", "UTF-8", GetpntsPlaceholder($SV, $AccConn));
				?>

<li><div class="sortSV"><span style = "cursor: default; float: left; margin: 3px 5px 0px 0px;"><?php echo iconv("Windows-1251", "UTF-8", $SVal); ?> </span><input type='text' name = <?php echo '"'.$SV.'"'; ?> placeholder = <?php echo '"'.$placeholder.'"'; ?> style="margin-right: 15px;"><a class='close_ui'></a></div></li>

				<?php
			}
		}
		else echo 'error';
		// кнопка добавить свойство ?>
	<li id = "LI_add_SV">
		<a class = "button" id = "add_SV" style = "cursor: pointer;">+</a>
		<div style = "display:none;">
			<div class = "sortSV">
				<select name = "chose_svs" id = "chose_svs">
					<option value = "0"></option>
					<?php 
						$query = "SELECT * FROM PNTS2";
						$result = $AccConn->query($query);
						while ($row = $result->fetch(PDO::FETCH_LAZY))
						{
							echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
						}
					?>
				</select>
				<div>
					<a class = "button" id = "ok_SV" style = "cursor: pointer;">Add</a>
					<a class = "button" id = "cancel_SV" style = "cursor: pointer;">Cancel</a>
				</div>
			</div>
		</div>
	</li>
<?php
	}

function printPNTONameAndValueOld($AccConn, $idString)
	{
		$arrSV = explode("_", $idString);
		for ($i=2; $i < count($arrSV); $i++)
		{ 
			$SV = (int)substr($arrSV[$i], 0, -5); // код свойства
			$SVconv = GetpntsNameOld($SV, $AccConn);
			$SValue = (int)substr($arrSV[$i], -5); // код значения свойства
			$SValueConv = GetpntsValueOld($SV, $SValue, $AccConn); ?>

			<li><div class="sortSV"><span style = "cursor: default;"><?php echo $SVconv; ?> </span><input type='text' name = <?php echo '"'.$SV.'"'; ?> value = <?php echo '"'.$SValueConv.'"'; ?> </div></li>
			<?php 
		}
	}
?>