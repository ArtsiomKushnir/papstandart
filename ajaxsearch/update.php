<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

$KeyName = $_POST['KeyName'];
$key = $_POST['key'];
$field = $_POST['field'];
$table = $_POST['table'];
$value = $_POST['value'];

//if ($value == '' || $value == ' '){$value = 'NULL';} // если пустое значение или пробел, присваиваем NULL

if ($field == 'BT2plus')
{
	if ($OldValue = ifBTplusExist($AccConn, $value)) // существует ли такое наименование в таблице BPplus
	{
		$qery = "UPDATE $table SET $field = $OldValue WHERE $KeyName = $key";
	}
	else if ($NewValue = addBTplus($AccConn, $value)) // создаем новое наименование в таблице BPplus
	{
		$qery = "UPDATE $table SET $field = $NewValue WHERE $KeyName = $key";
	}
}

else if ($field == 'ves') // далее если значение $value - вес товара
{
	$qery = "UPDATE $table SET $field = $value WHERE $KeyName = $key";
}
else if ($field == 'brend') // далее если значение $value - вес товара
{
	$qery = "UPDATE $table SET $field = $value WHERE $KeyName = $key";
}
else if ($field == 'barCode') // далее если значение $value - штрих-код товара
{
	$qery = "UPDATE $table SET $field = '$value' WHERE $KeyName = $key";
}

else
{
	$value = iconv("UTF-8", "Windows-1251", $_POST['value']);
	$qery = "UPDATE $table SET $field = '$value' WHERE $KeyName = $key";
}

$result = $AccConn->prepare($qery);
if ($result->execute())
{
	AddLog('Данные обновлены. Таблица: '.$table.', поле: '.$field.', ID строки: '.$key.', Новое значение: '.$_POST['value']);
	// ----------------------------------------------------------------------------------------------------------------------
	if ($field == 'BT2plus') // если обновляемое поле BT2plus, 
	{
		if($naim = GetNAIM($AccConn, $key)) // то обновляем поле NAIM2 в pnt по коду товара $key
		{
			AddLog("Присвоено NAIM2 = ".iconv("Windows-1251", "UTF-8", $naim).", код товара: ".$key);
			UpdatePNT($AccConn, 'NAIM2', $naim, $key);
		}
		else AddLog("Не удалось записать NAIM2, код товара: ".$key);
	}
	// ----------------------------------------------------------------------------------------------------------------------
}
else 
{
	AddLog('Ошибка обновления данных. Таблица: '.$table.', поле: '.$field.', ID строки: '.$key);
}

?>