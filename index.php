<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СТАНДАРТ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="Modal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="title"><strong><i id="nameTitle"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body" id="modalBody">
										
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="save">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="masthead">
		<!-- МЕНЮ ======================================================= -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>

							<li>
								<a>СТАТИСТИЧЕСКАЯ ИНФОРМЦИЯ</a>
							</li>
						</ul>
			    	</div>
				</div>
			</nav>
		</div>

		<!-- КОНТЕНТ ======================================================= -->
		<div class="row">
	  		<div class="col-xs-12">
	  			<h3>ОБЩАЯ СТАТИСТИКА</h3>
	  			<?php
					$query = "SELECT DISTINCT pnt
								FROM scl
								WHERE ost > 0";

					if ($result = $AccConn->query($query)) {
						$arr = $result->fetchAll(PDO::FETCH_ASSOC);
						$countAll = count($arr);
					} else echo "ERR";
				?>
				<!-- <pre>
					<?php //print_r($arr); ?>
				</pre> -->
	  			<table class="table table-condensed table-hover table-bordered">
	  				<tbody>
	  					<tr>
	  						<td>Всего кодов товара в наличии: </td>
	  						<td colspan="2" align="center">
	  							<button type="button" class="btn btn-sm btn-info"><strong><?php echo $countAll; ?></strong></button>
	  						</td>
	  					</tr>
	  					<!-- <tr>
	  						<td rowspan="2">Отсутствую изображения на сайте:</td>
	  						<td align="center">Брест</td>
	  						<td align="center">Минск</td>
	  					</tr>
	  					<tr>
	  						<?php 
		  							/*if($str = file_get_contents('http://dc.paperki.by/brest.php')) {
										$array = explode("~", $str);
									}*/
									
								?>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger"><strong><?php //echo $array[0]; ?></strong></button>
	  						</td>
	  						<?php 
		  							/*if($str = file_get_contents('http://dc.paperki.by/minsk.php')) {
										$array = explode("~", $str);
									}*/
									
								?>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger"><strong><?php //echo $array[0]; ?></strong></button>
	  						</td>
	  					</tr> -->
	  					<tr>
	  						<td>Не присвоены новые стандарты товарам в наличии:</td>
	  						<?php
	  								$query = "SELECT scl.pnt
												FROM scl
												LEFT JOIN pnt ON scl.pnt = pnt.pnt
												WHERE (((scl.ost)>0))
												GROUP BY scl.pnt
												HAVING ((((FIRST(pnt.BT2)) IS NULL) OR ((FIRST(pnt.BT2)) = 0)) OR ((FIRST(pnt.PNTO2)) IS NULL))";

	  								/*$query = "SELECT count(*) as count
	  											FROM scl s
	  											LEFT JOIN pnt p ON p.pnt = s.pnt
	  											WHERE (s.ost > 0) AND ((p.PNTO2 IS NOT NULL) AND ((p.BT2 IS NOT NULL) OR (p.BT2 = 0)))";*/
	  								if ($result = $AccConn->query($query)) {
	  									$arr = $result->fetchAll(PDO::FETCH_ASSOC);
	  									$countNewBT = count($arr);
									} else echo "ERROR";
							?>
							<!-- <pre>
								<?php //print_r($arr); ?>
							</pre> -->
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger" id="getPntList">
	  								<input type="hidden" value="SELECT scl.pnt
	  																, First(pnt.NAIM) AS [First-NAIM]
	  																, First(pnt.BT2) AS [First-BT2]
	  																, First(pnt.ARTIKUL) AS [Articul]
	  																, First(pnt.barCode) AS [First-barCode]
	  																, First(pnt.gabarits) AS [First-gabarits]
	  																, First(pnt.ves) AS [First-ves]
															FROM scl
															LEFT JOIN pnt ON scl.pnt = pnt.pnt
															WHERE (((scl.ost)>0))
															GROUP BY scl.pnt
															HAVING ((((FIRST(pnt.BT2)) IS NULL) OR ((FIRST(pnt.BT2)) = 0)) OR ((FIRST(pnt.PNTO2)) IS NULL))">
	  								<strong><?php echo $countNewBT; ?></strong>
	  							</button>
	  						</td>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-success"><strong><?php echo $countAll - $countNewBT; ?></strong></button>
	  						</td>
	  					</tr>
	  					<tr>
	  						<td>Не заполнены значения Веса и Габаритов у товаров в наличии:</td>
	  						<?php

	  								$query = "SELECT scl.pnt
												FROM scl
												LEFT JOIN pnt ON scl.pnt = pnt.pnt
												WHERE (((scl.ost)>0))
												GROUP BY scl.pnt
												HAVING (((FIRST(pnt.gabarits)) IS NULL) OR ((FIRST(pnt.ves)) IS NULL))";
	  								if ($result = $AccConn->query($query)) {
	  									$arr = $result->fetchAll(PDO::FETCH_ASSOC);
	  									$countGabaritsVes = count($arr);
									} else echo "ERROR";
							?>
							<!-- <pre>
								<?php //print_r($arr); ?>
							</pre> -->
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger" id="getPntList">
	  								<input type="hidden" value="SELECT scl.pnt
	  																, First(pnt.NAIM) AS [First-NAIM]
	  																, First(pnt.BT2) AS [First-BT2]
	  																, First(pnt.ARTIKUL) AS [Articul]
	  																, First(pnt.barCode) AS [First-barCode]
	  																, First(pnt.gabarits) AS [First-gabarits]
	  																, First(pnt.ves) AS [First-ves]
															FROM scl
															LEFT JOIN pnt ON scl.pnt = pnt.pnt
															WHERE (((scl.ost)>0))
															GROUP BY scl.pnt
															HAVING (((FIRST(pnt.gabarits)) IS NULL) OR ((FIRST(pnt.ves)) IS NULL))">
								<strong><?php echo $countGabaritsVes; ?></strong></button>

	  						</td>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-success"><strong><?php echo $countAll - $countGabaritsVes; ?></strong></button>
	  						</td>
	  					</tr>
	  					<tr>
	  						<td>Не заполнено значение Штрих-кода у товаров в наличии:</td>
	  						<?php
	  								$query = "SELECT scl.pnt
												FROM scl
												LEFT JOIN pnt ON scl.pnt = pnt.pnt
												WHERE (((scl.ost)>0))
												GROUP BY scl.pnt
												HAVING ((FIRST(pnt.barCode)) IS NULL)";

	  								if ($result = $AccConn->query($query)) {
	  									$arr = $result->fetchAll(PDO::FETCH_ASSOC);
	  									$countBarcode = count($arr);
							        } else echo "ERROR";
							?>
							<!-- <pre>
								<?php //print_r($arr); ?>
							</pre> -->
							
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger" id="getPntList">
	  							<input type="hidden" value="SELECT scl.pnt
	  																, First(pnt.NAIM) AS [First-NAIM]
	  																, First(pnt.BT2) AS [First-BT2]
	  																, First(pnt.ARTIKUL) AS [Articul]
	  																, First(pnt.barCode) AS [First-barCode]
	  																, First(pnt.gabarits) AS [First-gabarits]
	  																, First(pnt.ves) AS [First-ves]
															FROM scl
															LEFT JOIN pnt ON scl.pnt = pnt.pnt
															WHERE (((scl.ost)>0))
															GROUP BY scl.pnt
															HAVING ((FIRST(pnt.barCode)) IS NULL)">
	  							<strong><?php echo $countBarcode; ?></strong></button>
	  						</td>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-success"><strong><?php echo $countAll - $countBarcode; ?></strong></button>
	  						</td>
	  					</tr>
	  					<tr>
	  						<td>Не заполнено наименование для сайта у товаров в наличии:</td>
	  						<?php

	  								$query = "SELECT scl.pnt
												FROM pnt 
												RIGHT JOIN (scl LEFT JOIN pntlongnaim ON scl.pnt = pntlongnaim.pnt) ON pnt.pnt = scl.pnt
												WHERE (((scl.ost)>0))
												GROUP BY scl.pnt
												HAVING (((First(pntlongnaim.NaimSite)) Is Null))";

	  								if ($result = $AccConn->query($query)) {
	  									$arr = $result->fetchAll(PDO::FETCH_ASSOC);
	  									$countNaimSite = count($arr);
							        } else echo "ERROR";
							?>
							<!-- <pre>
								<?php //print_r($arr); ?>
							</pre> -->
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger" id="getPntList">
	  							<input type="hidden" value="SELECT scl.pnt
	  																, First(pnt.NAIM) AS [First-NAIM]
	  																, First(pnt.BT2) AS [First-BT2]
																	, First(pnt.ARTIKUL) AS [Articul]
	  																, First(pnt.barCode) AS [First-barCode]
	  																, First(pnt.gabarits) AS [First-gabarits]
	  																, First(pnt.ves) AS [First-ves]
															FROM pnt 
															RIGHT JOIN (scl LEFT JOIN pntlongnaim ON scl.pnt = pntlongnaim.pnt) ON pnt.pnt = scl.pnt
															WHERE (((scl.ost)>0))
															GROUP BY scl.pnt
															HAVING (((First(pntlongnaim.NaimSite)) Is Null))">
	  							<strong><?php echo $countNaimSite; ?></strong></button>
	  						</td>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-success"><strong><?php echo $countAll - $countNaimSite; ?></strong></button>
	  						</td>
	  					</tr>
	  					<tr>
	  						<td>Не заполнено описание у товаров в наличии:</td>
	  						<?php
	  								$query = "SELECT scl.pnt
												FROM scl 
												LEFT JOIN pntlongnaim ON scl.pnt = pntlongnaim.pnt
												WHERE (((scl.ost)>0))
												GROUP BY scl.pnt
												HAVING (((FIRST(pntlongnaim.opisanie)) IS NULL))";

	  								if ($result = $AccConn->query($query)) {
	  									$arr = $result->fetchAll(PDO::FETCH_ASSOC);
	  									$countopisanie = count($arr);
							        } else echo "ERROR";
							?>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-danger" id="getPntList">
	  							<input type="hidden" value="SELECT scl.pnt
	  																, First(pnt.NAIM) AS [First-NAIM]
	  																, First(pnt.BT2) AS [First-BT2]
	  																, First(pnt.ARTIKUL) AS [Articul]
	  																, First(pnt.barCode) AS [First-barCode]
	  																, First(pnt.gabarits) AS [First-gabarits]
	  																, First(pnt.ves) AS [First-ves]
															FROM pnt 
															RIGHT JOIN (scl LEFT JOIN pntlongnaim ON scl.pnt = pntlongnaim.pnt) ON pnt.pnt = scl.pnt
															WHERE (((scl.ost)>0))
															GROUP BY scl.pnt
															HAVING (((First(pntlongnaim.opisanie)) Is Null))">
	  							<strong><?php echo $countopisanie; ?></strong></button>
	  						</td>
	  						<td align="center">
	  							<button type="button" class="btn btn-sm btn-success"><strong><?php echo $countAll - $countopisanie; ?></strong></button>
	  						</td>
	  					</tr>
	  				</tbody>
	  			</table>
	  		</div>

	  		<style type="text/css">
	  			div.panel-body {
	  				text-align: center;
	  			}
	  		</style>

	  		<div class="col-xs-12">
		  		<div class="panel panel-default">
		  			<div class="panel-heading">Результат выборки</div>

		  			<div class="panel-body" id="resSearch">

					</div>
				</div>
			</div>

	  	</div>

	</div>

	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- tablesorter  http://tablesorter.com/addons/pager/jquery.tablesorter.pager.js -->
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript">
		$(document).on('click', '#getPntList', function(){
			/*alert($(this).children('input').val());*/
			$('#resSearch').html('<img src="../images/pbar.gif" alt="clock">');
			$.ajax({
				type: "POST",
				url: "MVC/get-tovar-list.php",
				cache: false,
				data: {'query': $(this).children('input').val()},
				success: function(html) {
					$('#resSearch').html(html);
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		});
	</script>
	<script type="text/javascript">
		// показать окно редактирования по старому наименованию
		$(document).on('click', 'a.getOldPnt', function(){
			$.ajax({
				type: "GET",
				url: "MVC/get-pnt-old.php?pnt="+ $(this).attr('id'),
				cache: false,
				success: function(html) {
					$('#modalBody').html(html);
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		});

		$(document).on('click', '#save', function(){
			$.ajax({
				type: "POST",
				url: "MVC/update-pnt-old.php",
				cache: false,
				data: {'desc': $('#pntDesc').val(), 'naimSite': $('#pntShortNaim').val(), 'pnt': $('#updPnt').html()},
				success: function(html) {
					if (html == 'OK') {
						$("#Modal").modal('hide');
						$('#modalBody').html('');
					} else {
						alert('UPDATE ERROR ('+html+')');
					}
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		});
	</script>
	<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function(){
			$('.ajax').html($('.ajax input').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<input id="editbox" value="' + $(this).text() + '" type="text">');
			$('#editbox').focus();
		});

		$(document).on('keydown', 'td.edit', function(){
			arr = $(this).attr('class').split(" ");
			if(event.which == 13) {
				$.ajax({ 
					type: "POST",
					url: "MVC/update.php",
					data: {"value": $('.ajax input').val(), "key": arr[2], "KeyName": arr[3], "field": arr[1], "table": arr[4]},
					cache: false,
					success: function(data){
						$('.ajax').html($('.ajax input').val());
						$('.ajax').removeClass('ajax');
					},
					error: function() {
						alert("ERROR request->response");
					}
				});
				return false;
			}
		});
	</script>
	
</body>
</html>