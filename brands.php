<?php
include_once "application/application.php";
include_once "application/brand_handler.php";
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>РЕДАКТОР БРЕНДОВ</title>
	<?php include_once 'head.php'; ?>
</head>
<body>
	<div align="center">
		<div class="messager" id="messager"></div>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="head">
			<div class="container">
				<div class="time" id="time"></div>
				<?php include_once 'login.php';?>
			</div>
		</section>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="menu">
			<div class="container">
				<?php include_once 'menu.php';?>
			</div>
		</section>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="categories">
			<div class="container">
				<div class="width100 left" style="display: inline-block;">
					<div style="width: 50%; float: left;">
						<div class="addbrend">
							<form action="brands.php" method="post">
								<span>Добавить новый бренд</span><br />
								<input style="width: 300px;" type="text" name="addbrend"><br/>
								<input type="submit" value="Добавить">
							</form>
						</div>
						<div>
							<?php BrendsPrint($AccConn) ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
<script>      
	$(document).ready(function()
	{  
		show();  
		setInterval('show()',10000);
	});  
</script>
<script type="text/javascript">
    	var config = {
      	'.chosen-select'           : {},
      	'.chosen-select-deselect'  : {allow_single_deselect:true},
      	'.chosen-select-no-single' : {disable_search_threshold:10},
     	'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
      	'.chosen-select-width'     : {width:"95%"}
    	}
	    for (var selector in config)
	    {
	    	$(selector).chosen(config[selector]);
	    }
</script>
<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function()
		{
			$('.ajax').html($('.ajax input').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<form action="ajaxsearch/update_gr.php" method="post" name="form" onsubmit="return false;"><input id="editbox" size="'+ $(this).text().length+'" value="' + $(this).text() + '" type="text"></form>');
			$('#editbox').focus();
		});
		
		$(document).on('keydown', 'td.edit', function(event)
		{
			if(event.which == 13)
		   	{
		   		var arr = $(this).attr('class').split(" ");
				var table = $('table').attr('id');
				var value = $('.ajax input').val();
				$.ajax(
				{ 
					type: "POST",
					url: "ajaxsearch/update_gr.php",
					data: {"value": value, "id": arr[2], "field": arr[1], "table": table},
					cache: false,
					success: function(data){
						 $('.ajax').html($('.ajax input').val());
						 $('.ajax').removeClass('ajax');
				 	}
				});
				return false;
		 	}
		});
	</script>
</body>
</html>