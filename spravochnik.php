<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СПРАВОЧНИК ТОВАРОВ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<link rel="stylesheet" href="css/bootstrap.chosen.min.css">

</head>
<body>
	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="Modal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="title"><strong><i id="nameTitle"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body" id="modalBody">
										
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="save" onclick="">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</div>

	<!-- ГЛАВНЫЙ КОНТЕЙНЕР ======================================================= -->
	<div class="container">

		<div class="masthead">
			<!-- МЕНЮ ======================================================= -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>

							<li>
								<a>СПРАВОЧНИК ТОВАРОВ</a>
							</li>
						</ul>
			    	</div>
				</div>
			</nav>
		</div>

		<!-- КОНТЕНТ ======================================================= -->
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">ПОИСК ПО СПРАВОЧНИКУ ТОВАРОВ</div>
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td>
								<div class="input-group">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control" placeholder="Код товара" id="pnt">
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control" placeholder="Штрих-код" id="barCode">
								</div>								
							</td>
						</tr>
						<tr>
							<td>
								<div class="input-group">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control" placeholder="Наименование товара" id="pntnaim">
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon"></span>
									<input type="text" class="form-control" placeholder="Артикул товара" id="pntarticul">
								</div>								
							</td>
						</tr>
					</tbody>
				</table>
			</div>			
		</div>
		<div class="row">
			<div id="resSearch" class="text-center">
				<?php// Сюда загружается результат search ?>
			</div>
		</div>

	</div>

	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- tablesorter  http://tablesorter.com/addons/pager/jquery.tablesorter.pager.js -->
	<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script src="js/chosen.jquery.min.js"></script>
	<script src="js/chosen.order.jquery.min.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
    	// контроль ввода символов
		$('#pnt').keypress(function(key)
		{
			if (key.charCode < 48 || key.charCode > 57 & key.charCode != 13) return false;
		});

		$('#barCode').keypress(function(key)
		{
			if (key.charCode < 48 || key.charCode > 57 & key.charCode != 13) return false;
		});
	</script>
	<script type="text/javascript">

		$("#pnt").keyup(function(event){
			search();
			return false;
		});

		$("#barCode").keyup(function(event){
			if(event.which == 13) {				
				search();
			}
			return false;
		});

		$("#pntnaim").keyup(function(event){
			if(event.which == 13) {				
				search();
			}
			return false;
		});

		$("#pntarticul").keyup(function(event){
			if(event.which == 13) {				
				search();
			}
			return false;
		});

		function search() {
			$("#resSearch").html('<div style = "color: red;"><strong>Выполняется запрос, подождите </strong><img src="ajax_clock_small.gif" width="16" height="16" alt="ajax clock"><div>');
			$.ajax({
				type: "GET",
				url: "MVC/search-tovar.php?pnt="+ $("#pnt").val() + "&barCode=" + $("#barCode").val() + "&naim=" + $("#pntnaim").val() + "&articul=" + $("#pntarticul").val(),
				cache: false,
				success: function(html) {
					$('#resSearch').html(html);
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

	</script>
	<script type="text/javascript">
		// показать окно редактирования по старому наименованию
		$(document).on('click', 'a.getOldPnt', function(){
			$.ajax({
				type: "GET",
				url: "MVC/get-pnt-old.php?pnt="+ $(this).attr('id'),
				cache: false,
				success: function(html) {
					$('#nameTitle').html('Редактирование товара');
					$('#modalBody').html(html);
					$("#Modal").modal('show');
					$('#save').attr('onclick', 'updateOldPNT()');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		});

		function updateOldPNT() {
			$.ajax({
				type: "POST",
				url: "MVC/update-pnt-old.php",
				cache: false,
				data: {'desc': $('#pntDesc').val()
						, 'naimSite': $('#pntShortNaim').val()
						, 'pnt': $('#updPnt').html()
					},
				success: function(html) {
					if (html == 'OK') {
						$("#Modal").modal('hide');
					} else {
						alert('UPDATE ERROR ('+html+')');
					}
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

	</script>
	<script type="text/javascript">
		$(document).on('dblclick', 'td.edit', function(){
			$('.ajax').html($('.ajax input').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<input id="editbox" value="' + $(this).text() + '" type="text">');
			$('#editbox').focus();
		});

		$(document).on('keydown', 'td.edit', function(){
			arr = $(this).attr('class').split(" ");
			if(event.which == 13) {
				$.ajax({ 
					type: "POST",
					url: "MVC/update.php",
					data: {"value": $('.ajax input').val()
							, "key": arr[2]
							, "KeyName": arr[3]
							, "field": arr[1]
							, "table": arr[4]
						},
					cache: false,
					success: function(data){
						$('.ajax').html($('.ajax input').val());
						$('.ajax').removeClass('ajax');
					},
					error: function() {
						alert("ERROR request->response");
					}
				});
				return false;
			}
		});
	</script>
	<script type="text/javascript">
		function showImageForm(id) {
			$.ajax({
				type: "GET",
				url: "MVC/get-image-form.php?id="+id,
				cache: false,
				success: function(html){
					$('#nameTitle').html('Редактирование изображения к коду: '+ id);
					$('#modalBody').html(html);
					$("#Modal").modal('show');
					$('#save').attr('onclick', '');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function Standart(id){
			$.ajax({
				type: "POST",
				url: "MVC/get-form-add-standart.php",
				cache: false,
				data: {'id': id},
				success: function(html){
					$('#nameTitle').html('Добавление стандарта к коду: '+ id);
					$('#modalBody').html(html);
					$('#save').attr('onclick', 'update("'+id+'", "pnt", "BT2", "pnt")');
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function pnt(id){
			$.ajax({
				type: "POST",
				url: "MVC/get-form-edit-pnt.php",
				cache: false,
				data: {'id': id},
				success: function(html){
					$('#nameTitle').html('Свойства по стандарту для товара по коду: '+ id);
					$('#modalBody').html(html);
					$('#save').attr('onclick', 'updatePNT('+id+')');
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function update(key, keyname, field, table) {
			$.ajax({ 
				type: "POST",
				cache: false,
				url: "MVC/update.php",
				data: {"value": $('#Standart').val()
						, "key": key
						, "KeyName": keyname
						, "field": field
						, "table": table
						, "resetPNT": $('#resetPNT').is(':checked') ? '1' : '0'
				},
				success: function(response){
					$("#Modal").modal('hide');
					$('#save').attr('onclick', '');
					search();
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function updatePNT(id) {
			$.ajax({
				type: "POST",
				cache: false,
				url: "MVC/updatePNTO2.php",
				data: {"id": id, "data": $('#PNT2').serializeArray()},
				success: function(response){
					$("#Modal").modal('hide');
					$('#save').attr('onclick', '');
					search();
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
		};
	</script>
</body>
</html>