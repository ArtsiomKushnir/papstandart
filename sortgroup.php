<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';

//------------- ОБРАБОТЧИК ---------------------------------------------------------------------------
if (isset($_POST['addgroup'])) {
	if ($_POST['addgroup'] !== '') {
		$nextNumber = GetNextNumber($AccConn, 'kod' ,'grK');
		$naimgrK = iconv("UTF-8", "Windows-1251", $_POST['addgroup']);
		if($posl = getNextSortnumber($AccConn, 0, 0, 'grK', 'posl')) {
		} else $posl = 99999;
		$values = array($nextNumber, $naimgrK, $posl);
		$result = $AccConn->prepare("INSERT INTO grK (kod, naim, posl) VALUES (?, ?, ?)");
		if ($result->execute($values)) {
			AddLog('Данные в grK добавлены. kod: '.$nextNumber.', naim: '.$_POST['addgroup']);
		} else AddLog("Ошибка добавления данных в grK. kod - ".$nextNumber.', naim: '.$_POST['addgroup']);
	}
	header('Location: sortgroup.php', true, 303);
//----------------------------------------------------------------------------------------------------
} else if (isset($_POST['addsecondgroup']) && isset($_POST['parentgroup'])) {
	if ($_POST['addsecondgroup'] !== '' && $_POST['parentgroup'] !== '') {
		$nextNumber = GetNextNumber($AccConn, 'kod' ,'grG');
		$naim = iconv("UTF-8", "Windows-1251", $_POST['addsecondgroup']);
		$grk = (int)$_POST['parentgroup'];
		if($poslSite = getNextSortnumberGrG($AccConn, $grk)) {
		} else $poslSite = 99999;
		$values = array($nextNumber, $naim, $grk, $poslSite);
		$result = $AccConn->prepare("INSERT INTO grG (kod, naim, grk, poslSite) VALUES (?, ?, ?, ?)");
		if ($result->execute($values)) {
			AddLog('Данные в grG добавлены. kod: '.$nextNumber.', naim: '.$_POST['addsecondgroup']);
		} else AddLog("Ошибка добавления данных в grG. kod - ".$nextNumber.', naim: '.$_POST['addsecondgroup']);
	}
	header('Location: sortgroup.php', true, 303);
//----------------------------------------------------------------------------------------------------
} else if (isset($_POST['naim_grK']) && $_POST['naim_grK'] != '' && isset($_POST['opisanie_grK']) && isset($_POST['id_grK'])) {
	$idgrK = $_POST['id_grK'];
	$naimgrK = iconv("UTF-8", "Windows-1251", $_POST['naim_grK']);
	$opisaniegrK = iconv("UTF-8", "Windows-1251", $_POST['opisanie_grK']);
	$query = "UPDATE grK SET opisanie = '".$opisaniegrK."', naim = '".$naimgrK."' WHERE kod = ".$idgrK;
	$result = $AccConn->prepare($query);
	if ($result->execute())	{
		AddLog("Данный в grK обновлены! kod: ".$idgrK.", naim = ".$_POST['naim_grK'].", opisanie = ".$_POST['opisanie_grK']);
	} else AddLog("Ошибка обновления данных в grK! kod: ".$idgrK);
	header('Location: sortgroup.php', true, 303);
//----------------------------------------------------------------------------------------------------
} else if (isset($_POST['naim_grG']) && $_POST['naim_grG'] != '' && isset($_POST['opisanie_grG']) && isset($_POST['id_grG'])) {
	$idgrG = $_POST['id_grG'];
	$naimgrG = iconv("UTF-8", "Windows-1251", $_POST['naim_grG']);
	$opisaniegrG = iconv("UTF-8", "Windows-1251", $_POST['opisanie_grG']);
	$query = "UPDATE grG SET opisanie = '".$opisaniegrG."', naim = '".$naimgrG."' WHERE kod = ".$idgrG;
	$result = $AccConn->prepare($query);
	if ($result->execute())	{
		AddLog("Данный в grG обновлены! kod: ".$idgrG.", naim = ".$_POST['naim_grG'].", opisanie = ".$_POST['opisanie_grG']);
	} else AddLog("Ошибка обновления данных в grG! kod: ".$idgrG);
	header('Location: sortgroup.php', true, 303);
//----------------------------------------------------------------------------------------------------
} else if (isset($_POST['opisanie_gr']) && isset($_POST['id_gr'])) {
	$idgr = $_POST['id_gr'];
	$opisaniegr = $_POST['opisanie_gr'];
	$query = "UPDATE GR_BT_BTplus SET opisanie = '".$opisaniegr."' WHERE kod = ".$idgr;
	if($result = $mysqli->query($query)) {
		AddLog('Данные в GR_BT_BTplus обновлены. kod: '.$_POST['id_gr'].', opisanie: '.$_POST['opisanie_gr']);
	} else AddLog('Ошибка обновления данных в GR_BT_BTplus. kod: '.$_POST['id_gr'].', opisanie: '.$_POST['opisanie_gr']);
	header('Location: sortgroup.php', true, 303);
//----------------------------------------------------------------------------------------------------
} else {}
//----------------------------------------------------------------------------------------------------

?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СОРТИРОВКА ГРУПП ТОВАРОВ</title>

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/jqueryui.custom.css" type="text/css">
	<link rel="stylesheet" href="css/chosen.min.css">

	<script src="http://code.jquery.com/jquery-1.8.3.js" type="text/javascript"></script>
	<script src="js/scripts.js" type="text/javascript"></script>
	<script src="js/chosen.jquery.min.js" type="text/javascript"></script>
	<script src="js/chosen.order.jquery.min.js" type="text/javascript"></script>

	</head>
</head>
<body>
	<div align="center">
<?php//----------------всплывающие окна-------------------------------------------------------------------------------------------------------------------------------------- ?>
		<div id = "messagebox">
			<div class="b-popup" id="popup">
				<div class="b-popup-content">
					<img src="pbar.gif" alt="clock">
				</div>
			</div>
		</div>
<?php//---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="head">
			<div class="container">
				<div class="time" id="time"></div>
				<?php include_once $_SERVER['DOCUMENT_ROOT'].'/login.php';?>
			</div>
		</section>
<?php//---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section class="menu">
			<div class="container">
				<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menu.php';?>
			</div>
		</section>
<?php//---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
		<section>
		<style type="text/css">
			div.sortSV {cursor: default; position: relative; display: inline-block; margin:3px; padding:3px; border:1px solid #888; border-radius: 3px; background-color: #eee; background-image: linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%); box-shadow: 0 0 2px #fff inset,0 1px 0 rgba(0,0,0,.05);}
			div.pilot{display: inline-block;}
			div.up{position: relative; display: inline-block; cursor: pointer; background-image: url(images/up.png); margin:0px; padding: 0px; height: 9px; width: 16px; float: right;}
			div.upno{position: relative; display: inline-block; cursor: default; margin:0px; padding: 0px; height: 9px; width: 16px; float: right;}
			div.down{position: relative; display: inline-block; cursor: pointer; background-image: url(images/down.png); margin:0px; padding: 0px; height: 9px; width: 16px; float: right;}
			li {margin-left: 30px;}
			ul {margin-bottom: 10px; list-style-type: none;}
			.ui-state-highlight {height: 1.5em; line-height: 1.2em;}
			span.col {}
		</style>
			<div class="container">
				<span style = "text-align: left; color: red; font-weight: 600; display: inline-block; width: 100%;">НОВЫЕ ГРУППЫ ВЫДЕЛЕНЫ КРАСНЫМ</span><br>
				<span style = "text-align: left; display: inline-block; width: 100%;">нажмите на название группы чтобы раскрыть вложенные</span>

				<div class="width100 left" style="display: inline-block; padding: 10px 0 0 10px;">
					<div style="width: 50%; float: left;">
						<div class="addgroup">
							<form action="sortgroup.php" method="post">
								<span>Добавить Генеральную группу товара:</span><br />
								<input style="width: 300px;" type="text" name="addgroup"><br />
								<input type="submit" value="Добавить">
							</form>
						</div>
					</div>
					<div style="width: 50%; float: left;">
						<div class="addgroup">
							<form action="sortgroup.php" method="post">
								<span>Добавить подчиненную группу товара:</span><br />
								<input style="width: 300px;" type="text" name="addsecondgroup"><br />
								<select style="width: 300px;" name = 'parentgroup' data-placeholder="Выберите генеральную группу..." class="chosen-select">
									<option value =""></option>
									<?php
								        $query = "SELECT * FROM grK";
								        $result = $AccConn->query($query);
								        while ($row = $result->fetch(PDO::FETCH_LAZY))
								        {
								        	echo "<option value = ".$row['kod'].">".iconv("Windows-1251", "UTF-8", $row['naim'])."</option>";
								        }
									?>
								</select><br />
								<input type="submit" value="Добавить">
							</form>
						</div>
						<script type="text/javascript">
							var config = {
								'.chosen-select'           : {},
								'.chosen-select-deselect'  : {allow_single_deselect:true},
								'.chosen-select-no-single' : {disable_search_threshold:10},
								'.chosen-select-no-results': {no_results_text:'Ничего не найдено!'},
								'.chosen-select-width'     : {width:"95%"}
							}
							for (var selector in config) {
								$(selector).chosen(config[selector]);
							}
						</script>
					</div>
				</div>
				<div style="text-align: left; margin: 5px; padding: 5px; display: block;">
					<ul type="none">
						<?php 
							BTupdate($AccConn, $mysqli); // обновляем таблицу
							NewTreeCatalogGRK($AccConn, $mysqli); // выводим группы
							$mysqli->close();
						?>
					</ul>
				</div>
			</div>
		</section>
<?php//----------------------------------------------------------------------------------------------------------------------------------------------------------------------- ?>
	</div>
		<script>      
			$(document).ready(function(){  
				show();  
				setInterval('show()',10000);
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				PopUpHide();
			});
			function PopUpShow(){
				$("#popup").show();
			}
			function PopUpHide(){
				$("#popup").hide();
			}
		</script>
		<script>
			$(document).ready(function(){ // схлопнуть дерево категорий
				$('div.closed').parent('li').children('ul').hide();
			});

			$(document).on('click', 'div.open', function(){ // раскрыть категорию
				$(this).parent('li').children('ul').hide();
				$(this).addClass('closed');
				$(this).removeClass('open');
			})

			$(document).on('click', 'div.closed', function(){ // закрыть категорию
				$(this).parent('li').children('ul').show();
				$(this).addClass('open');
				$(this).removeClass('closed');
			})

			// поднимаем вверх
			$(document).on('click', 'div.up', function(){
				var prevli = $(this).parent('div').parent('div').parent('li').prev('li');
				var thisli = $(this).parent('div').parent('div').parent('li');

				var prevnumber = prevli.find('span.sort').first().text();
				var thisnumber = thisli.find('span.sort').first().text();

				var arr1 = thisli.find('span.col').first().text().split("_");
				var arr2 = prevli.find('span.col').first().text().split("_");

				$.ajax({ 
						type: "POST",
						url: "ajaxsearch/gr_sort.php",
						async: false,
						data: {"level": arr1[0], "thisid": arr1[1], "thisnumber": thisnumber, "nextid": arr2[1], "nextnumber": prevnumber},
						cache: false,
						success: function(response)
						{							
							if (response == "OK") {
								// меняем номера местами
								prevli.find('span.sort').first().text(thisnumber);
								thisli.find('span.sort').first().text(prevnumber);

								// меняем позиции местами
								var prevlihtml = prevli.html();
								prevli.html(thisli.html());
								thisli.html(prevlihtml);
							} else alert("ERR");
						}
				});
				return false;
			})

			// опускаем вниз
			$(document).on('click', 'div.down', function(){
				var nextli = $(this).parent('div').parent('div').parent('li').next('li');
				var thisli = $(this).parent('div').parent('div').parent('li');

				var nextnumber = nextli.find('span.sort').first().text();
				var thisnumber = thisli.find('span.sort').first().text();

				var arr1 = thisli.find('span.col').first().text().split("_");
				var arr2 = nextli.find('span.col').first().text().split("_");

				$.ajax({ 
						type: "POST",
						url: "ajaxsearch/gr_sort.php",
						async: false,
						data: {"level": arr1[0], "thisid": arr1[1], "thisnumber": thisnumber, "nextid": arr2[1], "nextnumber": nextnumber},
						cache: false,
						success: function(response)
						{							
							if (response == "OK") {
								// меняем номера местами
								nextli.find('span.sort').first().text(thisnumber);
								thisli.find('span.sort').first().text(nextnumber);

								// меняем позиции местами
								var nextlihtml = nextli.html();
								nextli.html(thisli.html());
								thisli.html(nextlihtml);
							} else alert("ERR");
						}
				});
				return false;
			})
		</script>
		<script> 	
	    	// подгрузить окно редактирования свойств по стандарту
	    	$(document).on('click', 'a.edit', function(){
				var id = $(this).attr('id');
				$(this).parent('div').addClass('editable');
				$('a.edit').addClass('no_edit');
				$('a.edit').removeClass('edit');
				PopUpShow();
				$.ajax({ 
					type: "POST",
					url: "ajaxsearch/get_editable.php",
						data: {"id": id},
						cache: false,
						success: function(response)
						{
							$('#popup').html(response);
						}
					});
					return false;
				});
				// закрыть окно редактирования свойств по стандарту
				$(document).on('click', 'a.button_cancel', function(){
					$("div.addBT").detach(); // удаляем элемент
					$('a.no_edit').addClass('edit');
					$('a.edit').removeClass('no_edit');
					$('.editable').removeAttr('class'); // удаляем класс
					PopUpHide();
					$('#popup').html('<div class="b-popup-content"><img src="pbar.gif" alt="clock"></div>');
				});
		</script>
</body>
</html>


<?php
function NewTreeCatalogGRK ($AccConn, $mysqli)
    { ?>
            <?php
                $result = $AccConn->query("SELECT * FROM grK ORDER BY posl");
                while ($row = $result->fetch(PDO::FETCH_LAZY))
                    { ?>
                    <li type="none">
	                    <div style="cursor: default; float: left;" class="closed">
	                    	<div class="pilot">
								<div class="down"></div>
								<div class="up"></div>
							</div>
							<div class="pilot">
<span style = "display: none;" class="sort"><?php echo $row['posl']; ?> </span><span class="col" style = "display: none;"><?php echo 'grK_'.$row['kod']; ?></span><?php echo iconv("Windows-1251", "UTF-8", $row['naim']);?>
	                    	</div>
	                    </div>
	                    <span style="margin-left: 10px;"><a href="#" class="edit" id =<?php echo '"'.$row['kod'].'_grk'.'"'; ?>>описание</a></span>

                    	<?php echo NewTreeCatalogGRG($row['kod'], $AccConn, $mysqli); ?>

                    </li>
            <?php } ?>
<?php }

function NewTreeCatalogGRG ($grk, $AccConn, $mysqli)
    { ?>
        <ul type="none">
            <?php
                $result = $AccConn->query("SELECT * FROM grG WHERE grk = $grk ORDER BY poslSite");
                while ($row = $result->fetch(PDO::FETCH_LAZY))
                    { ?>
                    <li type="none">                    	
						<div style="cursor: default; float: left;" class="closed">
							<div class="pilot">
								<div class="up"></div>
								<div class="down"></div>
							</div>
								<?php
									if ($row['kod'] > 635) { ?>
										<div class="pilot" style="color: red; font-weight: 600;">
									<?php } else { ?>
										<div class="pilot">
									<?php } ?>
<span style = "display: none;" class="sort"><?php echo $row['poslSite']; ?> </span><span class="col" style = "display: none;"><?php echo 'grG_'.$row['kod']; ?></span><?php echo iconv("Windows-1251", "UTF-8", $row['naim']); ?>
							</div>
						</div>
						<span style="margin-left: 10px;"><a href="#" class="edit" id =<?php echo '"'.$row['kod'].'_grg'.'"'; ?>>описание</a></span>

							<?php echo TovarGroupPrint($row['kod'], $AccConn, $mysqli); ?>

					</li>
            <?php } ?>            
        </ul>
<?php }

function TovarGroupPrint ($grg, $AccConn, $mysqli)
     { ?>
            <ul id="sortable">
            	<?php
				if($result = $mysqli->query("SELECT kod, BTandBTplus, sortnumber FROM GR_BT_BTplus WHERE GrG = $grg ORDER BY sortnumber"))
				{
				while ($row = $result->fetch_array(MYSQLI_BOTH))
                    { ?>
						<li>
							<div class="sortSV">
								<div class="pilot">
									<div class="down"></div>
									<div class="up"></div>
								</div>
								<div class="pilot">
<span style = "display: none;" class="sort"><?php echo $row['sortnumber']; ?> </span><span class="col" style = "display: none;"><?php echo 'gr_'.$row['kod']; ?></span><?php echo $row['BTandBTplus']; ?>
								</div>
								<span style="margin-left: 10px;"><a href="#" class="edit" id =<?php echo '"'.$row['kod'].'_gr'.'"'; ?>>описание</a></span>
							</div>
						</li>
					<?php }
				} else {?>
					<li>
						<div class="sortSV">SQL_ERROR</div>
					</li>
				<?php } ?>
            </ul>
<?php }


function BTupdate($AccConn, $mysqli)
	{

	// -----------------------------------------------------------
		$NewAdded = 0;
		$OldExist = 0;
		$ErrorAdd = 0;
	// -----------------------------------------------------------
		if ($result = $AccConn->query("SELECT DISTINCT
										pnt.BT2Plus AS pntBTplus,
										BT.GrG AS BTGrG, 
										BT.naim AS BTnaim, 
										BTplus.naim AS BTplusnaim
										FROM (pnt INNER JOIN BT ON pnt.BT2 = BT.kod) 
										LEFT JOIN BTplus ON pnt.BT2plus = BTplus.kod
										WHERE (((pnt.BT2) Is Not Null))"))
		{

			while ($row = $result->fetch(PDO::FETCH_LAZY))
			{
				if ($row['pntBTplus'] != '' OR $row['pntBTplus'] != 0)
				{
					$BTandBTplus = iconv("Windows-1251", "UTF-8", $row['BTnaim'].' '.$row['BTplusnaim']);
				}
				else
				{
					$BTandBTplus = iconv("Windows-1251", "UTF-8", $row['BTnaim']);
				}

				if (ifBTandBTplusExist($mysqli, $BTandBTplus, $row['BTGrG']))
				{
					$OldExist ++;
				}
				else if(addBTandBTplus($mysqli, $BTandBTplus, $row['BTGrG']))
				{
					$NewAdded ++;
				}
				else $ErrorAdd ++;
			}
			echo '<span>Сущестующих групп BT + BTplus: '.$OldExist.'</span><br>';
			echo '<span>Новых групп BT + BTplus: '.$NewAdded.'</span><br>';
			if ($ErrorAdd > 0) 
			{
				echo '<span style = "color: red; font-weight: 600;">Ошибок BT + BTplus: '.$ErrorAdd.'</span><br>';
			}
			else echo '<span>Ошибок BT + BTplus: '.$ErrorAdd.'</span><br>';
		}
		else echo '<span>ERROR</span>';
	}

function ifBTandBTplusExist($mysqli, $BTandBTplus, $GrG) // ищем совпадение BTandBTplus
	{
		$query = "SELECT BTandBTplus FROM GR_BT_BTplus WHERE GrG = $GrG";
		if ($result = $mysqli->query($query))
		{
			$dataSQL = false;
			while ($row = $result->fetch_array(MYSQLI_BOTH))
			{
				if ($BTandBTplus == $row['BTandBTplus'])
				{
					$dataSQL = true;
				}
			}
			return $dataSQL;
		}
		else return false;
	}

function addBTandBTplus($mysqli, $BTandBTplus, $grg) // добавляем новую BTandBTplus в конец
	{
		$query = "SELECT COUNT(GrG) FROM GR_BT_BTplus WHERE GrG = $grg";
		if($result = $mysqli->query($query))
		{
			$numb = $result->fetch_row();
			$n = $numb[0];
		}

		if($n > 0)
		{
			if($sortnumber = getNextSortnumberSQL($mysqli, $grg))
			{}
			else $sortnumber = 0;
		}
		else $sortnumber = 1;
	
		$query = "INSERT INTO GR_BT_BTplus (GrG, BTandBTplus, sortnumber) VALUES ('$grg', '$BTandBTplus', '$sortnumber')";
		return $result = $mysqli->query($query);
	}

function getNextSortnumberSQL($mysqli, $grg)
	{
		$query = "SELECT MAX(sortnumber) FROM GR_BT_BTplus WHERE GrG = $grg";
		if($result = $mysqli->query($query))
		{
			$numb = $result->fetch_row();
			return $numb[0] + 1;
		}
		else return false;
	}

function getNextSortnumberGrG($AccConn, $grk)
	{
		$query = "SELECT MAX(poslSite) FROM grG WHERE grk = $grk";
		if($result = $AccConn->query($query))
		{
			$numb = $result->fetchColumn();
			return $numb + 1;
		}
		else return false;
	}

function getNextSortnumber($AccConn, $parent, $parentName, $tablename, $poslfieldname)
	{
		if ($parent == 0 OR $parentName == 0) $query = "SELECT MAX($poslfieldname) FROM $tablename";
		else $query = "SELECT MAX($poslfieldname) FROM $tablename WHERE $parentName = $parent";

		if($result = $AccConn->query($query))
		{
			$numb = $result->fetchColumn();
			return $numb + 1;
		}
		else return false;
	}

?>