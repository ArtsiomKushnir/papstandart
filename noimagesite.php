<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>ОТСУТСТВУЮЩИЕ ИЗОБРАЖЕНИЯ НА САЙТАХ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="Modal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="title"><strong><i id="nameTitle"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body" id="modalBody">
										
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<!-- ГЛАВНЫЙ КОНТЕЙНЕР ======================================================= -->
	<div class="container">

		<div class="masthead">
			<!-- МЕНЮ ======================================================= -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>

							<li>
								<a>ОТСУТСТВУЮЩИЕ ИЗОБРАЖЕНИЯ НА САЙТАХ</a>
							</li>
						</ul>
			    	</div>
				</div>
			</nav>
		</div>

		<!-- КОНТЕНТ ======================================================= -->
		<div class="row">

			<style type="text/css">
				.tabheader
					{
					    font-size: 11px;
						font-weight: 400;
					    color: #777777;
					    background: #f2f2f2;
					    border: solid #e6e6e6;
					    border-width: 1px;
					    padding: 5px;
					    text-align: center;
					}
			</style>

			<div class="col-md-6">
				<div class="table-responsive">
				<?php
					if($str = file_get_contents('http://dc.paperki.by/brest.php'))
					{
						$array = explode("~", $str);
				?>
						<h3>Отсутствуют изображения товара Брест: <?php echo "<span style = 'color: red;'>".$array[0]."</span>";  unset($array[0]); ?></h3>
							<table class="table table-condensed table-bordered" style="text-align: left;">
								<tr class="tabheader">
									<td>Изображение</td>
									<td>Код товара</td>
									<td>Наименование</td>
								</tr>
							<?php foreach ($array as $key => $value)
								{ ?>
								<tr>
									<td><button type="button" class="btn btn-sm btn-info" onclick="showImageForm(<?php echo $value; ?>)"><strong>EDIT</strong></button></td>
									<td><?php echo $value; ?></td>
									<td class="left"><?php if ($Naim = GetOldNaim($AccConn, $value)){echo iconv("Windows-1251", "UTF-8", $Naim);} else echo "Error Naim"?></td>
								</tr>
							<?php } 
									?>		
							</table>
				<?php } else echo "ERROR FUNCTION"; ?>
				</div>
			</div>

			<div class="col-md-6">
				<div class="table-responsive">
				<?php
					if($str = file_get_contents('http://dc.paperki.by/minsk.php'))
					{
						$array = explode("~", $str);
				?>
						<h3>Отсутствуют изображения товара Минск: <?php echo "<span style = 'color: red;'>".$array[0]."</span>";  unset($array[0]); ?></h3>
							<table class="table table-condensed table-bordered" style="text-align: left;">
								<tr class="tabheader">
									<td>Изображение</td>
									<td>Код товара</td>
									<td>Наименование</td>
								</tr>
							<?php foreach ($array as $key => $value)
								{ ?>
								<tr>
									<td><!-- <button type="button" class="btn btn-sm btn-info" onclick="showImageForm(<?php /*echo $value;*/ ?>)"><strong>EDIT</strong></button> --></td>
									<td><?php echo $value; ?></td>
									<td class="left"><?php if ($Naim = GetOldNaim($AccConn, $value)){echo iconv("Windows-1251", "UTF-8", $Naim);} else echo "Error Naim"?></td>
								</tr>
							<?php } 
									?>		
							</table>
				<?php } else echo "ERROR FUNCTION"; ?>
				</div>				
			</div>

		</div>

	</div>
	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript">
		function showImageForm(id) {
			$.ajax({
				type: "GET",
				url: "MVC/get-image-form.php?id="+id,
				cache: false,
				success: function(html){
					$('#nameTitle').html('Редактирование изображения к коду: '+id+'.');
					$('#modalBody').html(html);
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		}
	</script>
</body>
</html>
<?php
function GetOldNaim($AccConn, $pnt)
    {
        $stmt = $AccConn->prepare("SELECT naim FROM pnt WHERE pnt = $pnt");
        if ($stmt->execute()) 
        {
            $name = $stmt->fetchColumn();
            return $name;
        }
        else return false;
    }
?>