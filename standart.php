<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/application/application.php';
?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content=text/html; charset=UTF-8>
	<title>СТАНДАРТ</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<link rel="stylesheet" href="css/bootstrap.chosen.min.css">

</head>
<body>
	<!-- ВСПЛЫВАЮЩЕЕ ОКНО ======================================================= -->
	<div id="Modal" class="modal fade bs-example-modal-lg" aria-labelledby="myModalLabel" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<!-- Заголовок модального окна -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="title"><strong><i id="nameTitle"></i></strong></h4>
				</div>
				<!-- Основное содержимое модального окна -->
				<div class="modal-body" id="modalBody">
										
				</div>
				<!-- Футер модального окна -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					<button type="button" class="btn btn-primary" id="save" onclick="">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="masthead">
		<!-- МЕНЮ ======================================================= -->
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">

						<?php include_once $_SERVER['DOCUMENT_ROOT'].'/menunew.php'; ?>

							<li>
								<a>СТАНДАРТЫ НАИМЕНОВАНИЙ ТОВАРОВ</a>
							</li>
						</ul>
			    	</div>
				</div>
			</nav>
		</div>

		<!-- КОНТЕНТ ======================================================= -->
		<div class="row">
			<div class="col-xs-12">
				<button type="button" class="btn btn-sm btn-success" id="formStandart" onclick="getFormStandart()">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					<strong>ДОБАВИТЬ СТАНДАРТ</strong>
				</button>
				
				<h3>СПРАВОЧНИК СТАНДАРТОВ</h3>
				<style type="text/css">
					a {
						cursor: pointer;
					}

					.tabheader
					{
					    font-size: 11px;
						font-weight: 400;
					    color: #777777;
					    background: #f2f2f2;
					    border: solid #e6e6e6;
					    border-width: 1px;
					    padding: 5px;
					    text-align: center;
					}

					.sv {
					    position: relative;
					    margin: 3px 3px 3px 0;
					    padding: 3px 3px 3px 3px;
					    border: 1px solid #aaa;
					    max-width: 100%;
					    border-radius: 3px;
					    background-color: #eee;
					    background-image: -webkit-gradient(linear,50% 0,50% 100%,color-stop(20%,#f4f4f4),color-stop(50%,#f0f0f0),color-stop(52%,#e8e8e8),color-stop(100%,#eee));
					    background-image: -webkit-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
					    background-image: -moz-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
					    background-image: -o-linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
					    background-image: linear-gradient(#f4f4f4 20%,#f0f0f0 50%,#e8e8e8 52%,#eee 100%);
					    background-size: 100% 19px;
					    background-repeat: repeat-x;
					    background-clip: padding-box;
					    box-shadow: 0 0 2px #fff inset,0 1px 0 rgba(0,0,0,.05);
					    color: #333;
					    line-height: 13px;
					    cursor: default;
					    float: left;
					    list-style: none;
					}
					ul {
					  list-style-type: none;
					}
				</style>
				<?php
					treePrintGrk($AccConn);
				?>
			</div>
		</div>
	</div>

	<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script src="js/chosen.jquery.min.js"></script>
	<script src="js/chosen.order.jquery.min.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){ // схлопнуть дерево категорий
			$('div.closed').parent('li').children('ul').hide();
		});

		$(document).on('click', 'div.open', function(){
			$(this).parent('li').children('ul').hide();
			$(this).addClass('closed');
			$(this).removeClass('open');
		});

		$(document).on('click', 'div.closed', function(){
			$(this).parent('li').children('ul').show();
			$(this).addClass('open');
			$(this).removeClass('closed');
		});

		$(document).on('dblclick', 'span.edit', function(){ // двойной клик
			$('.ajax').html($('#editbox').val());
			$('.ajax').removeClass('ajax');
			$(this).addClass('ajax');
			$(this).html('<input id="editbox" size="'+ $(this).text().length+'" value="' + $(this).text() + '" class = "'+ $(this).attr('id') +'" type="text">');
			$('#editbox').focus();
		});

		$(document).on('keydown', '#editbox', function(){
		   if(event.which == 13)
		   {
				$.ajax({ 
					type: "PUT",
					url: "MVC/update-grk-posl.php?"+ $.param({"order": $(this).val()}) + "&" + $.param({"id": $(this).attr('class')}),
					async: false,
					cache: false,
					success: function(json)
					{
						location.reload();
				 	},
					error: function() {
						alert('Ошибка сервера');
					}
				});
		 	}
		});
	</script>
	<script type="text/javascript">
		function getFormStandart() {
			$.ajax({
				type: "GET",
				url: "MVC/get-form-standart.php",
				async: false,
				cache: false,
				success: function(html) {
					$('#nameTitle').html('Создание нового стандарта свойств для товара');
					$('#modalBody').html(html);
					$('#save').attr('onclick', 'addNewStandart()');
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function getSvs() {
			$.ajax({
				type: "GET",
				url: "MVC/svs.php",
				async: false,
				cache: false,
				success: function(html) {
					$('#getsvs').html(html);
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function addNewStandart() {
			$.ajax({
				type: "POST",
				url: "MVC/add-standart.php",
				async: false,
				cache: false,
				data:  {
							"BT": $('input[name="BT"]').val()
							, "svs": $('#PNTS').getSelectionOrder()
							, "grg": $('#grG').val()
						},
				success: function(response) {
					if (response == 'OK') {location.reload();} else alert(response);
				},
				error: function() {
					alert('Ошибка сервера');
				}
			});
		};

		function editStandart(id) {
			$.ajax({
				type: "GET",
				url: "MVC/get-form-edit-standart.php?id="+id,
				async: false,
				cache: false,
				success: function(html) {
					$('#nameTitle').html('Редактирование стандарта свойств для товара ('+id+')');
					$('#modalBody').html(html);
					$('#save').attr('onclick', 'updateStandart('+id+')');
					$("#Modal").modal('show');
				},
				error: function() {
					alert("ERROR request->response");
				}
			});
			return false;
		};

		function updateStandart(id) {
			$.ajax({
				type: "POST",
				url: "MVC/update-standart.php",
				async: false,
				cache: false,
				data: {"id": id, 
						"BT": $('#BTname').val(),
						"xarakter": $('#xarakter').serializeArray(),
						"gr": $('#gr').val()},
				success: function(response) {
					if (response == 'OK') {
						$("#Modal").modal('hide');
						$('#save').attr('onclick', '');
					} else alert(response);
				},
				error: function() {
					alert("Ошибка Сервера");
				}
			});
			return false;
		};

		function addNewSv(name) {
			var returnval;
			$.ajax({
				type: "POST",
				url: "MVC/add-new-sv.php",
				async: false,
				cache: false,
				data: {"name": name},
				success: function(response) {
					if (response == 'ERROR') {
						alert(response);
						returnval = false;
					} else if (response == 'EMPTY VALUE') {
						alert(response);
						returnval = false;
					} else if ($.isNumeric(response)) {
						returnval = response;
					} else {
						alert(response);
						returnval = false;
					}
				},
				error: function() {
					alert("Ошибка Сервера");
					returnval = false;
				}
			});
			return returnval;
		};

		function addSV2(name) {
			if (SvId2 = addNewSv(name)) {
				alert('Свойство успешно создано');
				getSvs()
				//getFormStandart();
			} else {
				alert('Ошибка в создании нового свойства!');
			}
		};

		function addSvLi() {
			$('#addSvLi').html('<div class="form-horizontal"><div class="input-group" style="width: 60%;"><span class="input-group-addon green" onclick="addSV($('+"'#newsvs'"+').val())"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></span><input class="form-control" placeholder="Введите Название свойства" type="text" id="newsvs"><span class="input-group-addon red" onclick="cancelAddSV()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span></div></div>');
		};

		function addSV(value) {
			if (SvId = addNewSv(value)) {
				alert(SvId);
				$('#addSvLi').html('<span class="label label-primary">'+value+' <input class="form-control" type="hidden" name = "'+SvId+'" value="'+value+'" ><span style="top: 3px;" class="glyphicon glyphicon-remove" aria-hidden="true" onclick="javascript: $(this).parents('+"'li'"+').remove();"></span></span>');
				$('#addSvLi').removeAttr('id');
				$('#sortable').append('<li id="addSvLi"><span class="label label-success" onclick="addSvLi()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Свойство</span></li>');
			} else {
				alert('Ошибка в создании нового свойства!');
			}
		};

		function cancelAddSV() {
			$('#addSvLi').html('<span class="label label-success" onclick="addSvLi()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Свойство</span>');
		};
	</script>
</body>
</html>

<?php
// -------------------------------------------------------------------------------------------------------------------
function treePrintGrk($AccConn) { ?>
	<ul>
	<?php
	foreach (getGrK($AccConn) as $value) { ?>
		<li>
			<div class = "closed">
				<span class="edit" id=<?php echo '"'.$value['kod'].'"'; ?>><?php echo $value['posl']; ?></span>
				<a class="cursor">
					<span><strong> - <?php echo $value['naim']; ?></strong></span>
				</a>
				<!-- <button type="button" class="btn btn-link" id=<?php //echo '"'.$value['kod'].'"'; ?> data-toggle="modal">описание</button>
				<button type="button" class="btn btn-warning" id=<?php //echo '"'.$value['kod'].'"'; ?> data-toggle="modal">Удалить</button> -->
			</div>
			<?php treePrintGrg($value['kod'], $AccConn); ?>
		</li>
<?php } ?>
	</ul>
<?php }

function treePrintGrg($parentId, $AccConn) { ?>
	<ul>
	<?php
	foreach (getGrg($parentId, $AccConn) as $value) { ?>
		<li>
			<div class = "closed">
			<span class="edit" id=<?php echo '"'.$value['kod'].'"'; ?>><?php echo $value['poslSite']; ?></span>
				<a class="cursor">
					<span><strong> - <?php echo $value['naim']; ?></strong></span>
				</a>
			</div>
			<?php standartPrint($value['kod'], $AccConn); ?>
		</li>
	<?php } ?>
	</ul>
<?php }

function standartPrint($group, $AccConn) { ?>
	<ul>
		<li>
			<table class="table table-condensed table-hover table-bordered">
				<tr class='tabheader'>
					<td>kod</td>
					<td>Наименование БТ</td>
					<td>Свойства</td>
				</tr>
				<?php
					foreach (getStandart($group, $AccConn) as $value) { ?>
						<tr>
							<td><?php echo $value['kod']; ?></td>
							<td><a onclick="editStandart(<?php echo "'".$value['kod']."'"; ?>)"><?php echo $value['naim']; ?></a></td>
							<td><?php getXarakterSlow($value['xarakter'], $AccConn); // printXarakter($value['xarakter'], $AccConn); ?></td>
						</tr>
					<?php }
				?>				
			</table>
		</li>
	</ul>
<?php }

function printXarakter($xarakter, $AccConn) { 
	if ($ar = getXarakter($xarakter, $AccConn)) {
		foreach ($ar as $value) { ?>
			<div class='sv'>
				<?php echo iconv("Windows-1251", "UTF-8", $value['naim']); ?>
			</div>
		<?php }
	} else echo 'ERROR';
}

function getXarakterSlow($xarakter, $AccConn) {
	$array = explode("_", $xarakter);
	if($result = $AccConn->prepare("SELECT naim FROM PNTS2 WHERE kod = ?")) {
		for ($i = 1; $i < count($array); $i++) {
			if ($result->execute(array((int)$array[$i]))) {
				?>
					<div class='sv'>
						<?php echo iconv("Windows-1251", "UTF-8", $result->fetchColumn()); ?>
					</div>
				<?php 
			} else echo 'ERROR';
		}
	} else echo 'ERROR';
}

function getXarakter($xarakter, $AccConn) {
	$in = printArrayInSqlQuery($xarakter);
	$query = "SELECT naim FROM PNTS2 WHERE kod IN (".$in.") ORDER BY ".OrderByString($xarakter)."";
	AddLog($query);
	if($result = $AccConn->prepare($query)){
		if ($result->execute()) {
			return $row = $result->fetchAll(PDO::FETCH_ASSOC);
		} else return false;
	} else return false;
}

function OrderByString($xarakter) {
	$array = explode("_", $xarakter);
	$str = "CASE kod";
	for ($i = 1; $i < count($array); $i++) {
		$str .= " WHEN ".(int)$array[$i]." THEN ".$i;
	}
	return $str." END ASC";
}

function printArrayInSqlQuery($xarakter) {
	$array = explode("_", $xarakter);
	$str = "";
	for ($i = 1; $i < count($array); $i++) {
		$str .= (int)$array[$i].", ";
	}
	return substr($str, 0, -2);
}

function getStandart($group, $AccConn) {
	$objArr = new ArrayObject();
	if($result = $AccConn->query("SELECT * FROM BT WHERE GrG = $group")) {
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$objArr->append(array('kod' => $row['kod'], 'naim' => iconv("Windows-1251", "UTF-8", $row['naim']), 'xarakter' => $row['xarakter']));
		}
	}
	return $objArr;
}

function getGrg($parentId, $AccConn) {
	$objArr = new ArrayObject();
	if($result = $AccConn->query("SELECT kod, naim, poslSite FROM grG WHERE grk = $parentId ORDER BY poslSite")) {
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$objArr->append(array('kod' => $row['kod'], 'naim' => iconv("Windows-1251", "UTF-8", $row['naim']), 'poslSite' => $row['poslSite']));
		}
	} 
	return $objArr;
}

function getGrK($AccConn) {
	$objArr = new ArrayObject();
	if($result = $AccConn->query("SELECT kod, naim, posl FROM grK ORDER BY posl")) {
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$objArr->append(array('kod' => $row['kod'], 'naim' => iconv("Windows-1251", "UTF-8", $row['naim']), 'posl' => $row['posl']));
		}
	}
	return $objArr; 
}

?>