-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.31 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных standart
DROP DATABASE IF EXISTS `standart`;
CREATE DATABASE IF NOT EXISTS `standart` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `standart`;


-- Дамп структуры для таблица standart.GR_BT_BTplus
DROP TABLE IF EXISTS `GR_BT_BTplus`;
CREATE TABLE IF NOT EXISTS `GR_BT_BTplus` (
  `GrG` int(11) DEFAULT NULL,
  `BTandBTplus` varchar(150) DEFAULT NULL,
  `sortnumber` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица динамичных данных (Базовый товар + BTplus) для группировки 3 уровня категории';

-- Дамп данных таблицы standart.GR_BT_BTplus: ~33 rows (приблизительно)
DELETE FROM `GR_BT_BTplus`;
/*!40000 ALTER TABLE `GR_BT_BTplus` DISABLE KEYS */;
INSERT INTO `GR_BT_BTplus` (`GrG`, `BTandBTplus`, `sortnumber`) VALUES
	(115, 'Зажимы для бумаг Berlingo', 1),
	(115, 'Зажимы для бумаг OfficeSpace', 2),
	(94, 'Подложка настольная OfficeSpace', 1),
	(603, 'Оснастка автоматическая для штампа Trodat Professional', 1),
	(5, 'Бумага офисная Xerox Colotech + Gloss Coated (глянцевая)', 1),
	(5, 'Бумага офисная Xerox Performer', 2),
	(5, 'Бумага офисная Xerox Office', 3),
	(5, 'Бумага офисная Xerox Colotech +', 4),
	(5, 'Бумага офисная Элита C', 5),
	(5, 'Бумага офисная Элита B', 6),
	(5, 'Бумага офисная Xerox Business', 7),
	(5, 'Бумага офисная Xerox Марафон Бизнес', 8),
	(5, 'Бумага офисная Navigator Universal', 9),
	(5, 'Бумага офисная Xerox Марафон Стандарт', 10),
	(5, 'Бумага офисная Xerox Марафон Премьер', 11),
	(5, 'Бумага офисная Ранiца', 12),
	(5, 'Бумага офисная цветная Lomond', 13),
	(5, 'Бумага офисная цветная Clairefontaine Trophee', 14),
	(5, 'Бумага офисная цветная "Радуга"', 15),
	(5, 'Бумага офисная цветная IQ Color', 16),
	(5, 'Бумага офисная цветная Xerox Symphony', 17),
	(5, 'Бумага офисная цветная Creative', 18),
	(5, 'Бумага офисная цветная', 19),
	(87, 'Бумага газетная для записей', 1),
	(87, 'Бумага писчая офсетная Студенческая', 2),
	(87, 'Бумага писчая "Ветразь"', 3),
	(14, 'Ватман', 1),
	(14, 'Калька универсальная Zanders', 2),
	(14, 'Калька универсальная Xerox Tracing Paper', 3),
	(14, 'Калька универсальная', 4),
	(14, 'Альбом для рисования', 5),
	(14, 'Блок для черчения', 6),
	(14, 'Блок для рисования', 7);
/*!40000 ALTER TABLE `GR_BT_BTplus` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
