<?php

//-------------------------------------------------------------------------------------------
	$LogDir = $_SERVER['DOCUMENT_ROOT'].'/custom/tovars.csv';
	$locaimagelFolder = 'D:\SALON\FotoTovar/';
	$imageDir = $_SERVER['DOCUMENT_ROOT']."/custom/img/";
	$ExcelFileDir = $_SERVER['DOCUMENT_ROOT']."/custom/tovar.xls";

	$query = "SELECT pnt.pnt, pnt.MAIM, pnt.ARTIKUL, pnt.STRANA 
				FROM pnt 
				WHERE pnt = ?";
//-------------------------------------------------------------------------------------------

// Подключаем класс для работы с excel
require_once('PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
require_once('PHPExcel/Writer/Excel5.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Artsiom Kushnir")
							 ->setLastModifiedBy("Artsiom Kushnir")
							 ->setTitle("Office 2007 XLSX Document")
							 ->setSubject("Office 2007 XLSX Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("result file");

$objPHPExcel->setActiveSheetIndex(0)
				            ->setCellValue('A1', 'Код товара')
				            ->setCellValue('B1', 'Наименование')
				            ->setCellValue('C1', 'Артикул')
				            ->setCellValue('D1', 'Страна ввоза');

$fp = fopen($LogDir, 'w');

/*fputcsv($fp, array(	iconv("UTF-8", "Windows-1251", 'Код товара'), 
					iconv("UTF-8", "Windows-1251", 'Наименование'), 
					iconv("UTF-8", "Windows-1251", 'Артикул'), 
					iconv("UTF-8", "Windows-1251", 'Страна ввоза')), "~");*/

$AccConn = DBAccessConnect();
$arr = file('products.txt');

$place_holders = implode(',', array_fill(0, count($arr), '?'));

$query = "SELECT pnt.pnt, pnt.MAIM, pnt.ARTIKUL, pnt.STRANA FROM pnt WHERE pnt IN ($place_holders)";

if ($result = $AccConn->prepare($query)) {
	if ($result->execute($arr))
	{
		while ($row = $result->fetch(PDO::FETCH_LAZY))
		{
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$count, iconv("Windows-1251", "UTF-8", $row['pnt']))
						->setCellValue('B'.$count, iconv("Windows-1251", "UTF-8", $row['NAIM']))
						->setCellValue('C'.$count, iconv("Windows-1251", "UTF-8", $row['ARTIKUL']))
						->setCellValue('D'.$count, iconv("Windows-1251", "UTF-8", $row['STRANA']));

			//fwrite($fp, $row['pnt']."~".$row['NAIM']."~".$row['ARTICUL'].PHP_EOL);
			/*fputcsv($fp, array($row['pnt'], $row['NAIM'], $row['ARTIKUL'], $row['STRANA']), "~");*/

			echo $count;
		}
	} else echo "ERROR execute ";
} else echo "ERROR prepare ";

foreach ($arr as $pnt) {
	$imagefile = GetImageFileName(substr($pnt, 0, -2));
	if(file_exists($locaimagelFolder.$imagefile)) {
		if (!copy($locaimagelFolder.$imagefile, $imageDir.$imagefile)) {}
	} else echo "ERROR file exist ".$imagefile.", ";
}

fclose($fp);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($ExcelFileDir);
exit;

//-------------------------------------------------------------------------------------------
// подключение к БД Access
function DBAccessConnect() {
	$dbAcess = 'D:\SALON\setd.mdb';
	try { 
		$conn = new PDO("odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=".$dbAcess.";Uid=; Pwd=;"); 
		return $conn; 
	} catch (Exception $e) {
		return false;
	}
}

function GetImageFileName($product_sku) {
	$icode = '00000000';
	$len = strlen($product_sku);
	if ($len == '1') {$icode = '0000000'.$product_sku;}
	else if ($len == '2') {$icode = '000000'.$product_sku;}
	else if ($len == '3') {$icode = '00000'.$product_sku;}
	else if ($len == '4') {$icode = '0000'.$product_sku;}
	else if ($len == '5') {$icode = '000'.$product_sku;}
	else if ($len == '6') {$icode = '00'.$product_sku;}
	else if ($len == '7') {$icode = '0'.$product_sku;}
	else if ($len == '8') {$icode =  $product_sku;}

	return $icode.'.jpg';
}

?>